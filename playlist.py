#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import random

try:
    from gi.repository import GLib, Gio, TotemPlParser
except ImportError, e:
    print e


def exclude_elems(seq, patterns=[".zip", ".bz2", ".mp3", ".rar", ".xz"]):
    copy_seq = list(seq)

    for item in seq:
        for pattern in patterns:
            if item.endswith(pattern):
                copy_seq.remove(item)

    return copy_seq


def get_list_dirs(path):
    dirs = os.listdir(path)

    return exclude_elems(dirs)


def get_uri_files(path):
    """Return list of files, absolute path is converted to uri."""

    all_files = []
    audio_ext = [".mp3", ".ogg", ".flac"]

    for root, dirs, files in os.walk(path):
        for f in files:
            all_files.append(os.path.join(root, f))

    # Get all no audio files
    no_files = exclude_elems(all_files, patterns=audio_ext)

    return [GLib.filename_to_uri(i, None) for i in all_files if i not in no_files]


def create_playlist(files, filename, playlist_type):
    """Created playlist file."""

    pl = TotemPlParser.Parser.new()
    playlist = TotemPlParser.Playlist.new()

    if isinstance(files, list):
        for f in files:
            iter = playlist.append()
            playlist.set_value(iter, TotemPlParser.PARSER_FIELD_URI, f)
        
        pl.save(playlist, filename, "Playlist", playlist_type)


def main(args):
    dirname = "".join(args.dir)
    playlist_type = args.type
    ext = None
    pl_type = None

    # Normalize path
    if dirname.startswith("~"):
        top_dir = os.path.expanduser("~")
    else:
        top_dir = os.path.abspath(dirname)

    # Extension
    if playlist_type == 0:
        ext = ".pls"
        pl_type = TotemPlParser.ParserType.PLS
    elif playlist_type == 1 or playlist_type == 2:
        ext = ".m3u"
        if playlist_type == 1:
            pl_type = TotemPlParser.ParserType.M3U
        else:
            pl_type = TotemPlParser.ParserType.M3U_DOS
    elif playlist_type == 3:
        ext = ".xspf"
        pl_type = TotemPlParser.ParserType.XSPF

    # Build playlist filename
    home_dir = GLib.get_home_dir()
    playlist_name = Gio.file_new_for_path(os.path.join(home_dir, \
                                                       "playlist" + ext))

    dirs = get_list_dirs(top_dir)

    # Choose random directory
    dir_name = random.choice(dirs)
    random_name = os.path.join(top_dir, dir_name)

    # Create playlist
    files = get_uri_files(random_name)
    create_playlist(files, playlist_name, pl_type)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create easily playlist")
    parser.add_argument("dir", nargs=1, help="directory path")
    parser.add_argument("-t", metavar="INT", type=int, \
                        choices=[0, 1, 2, 3], \
                        default=1, \
                        dest="type", \
                        help="type of playlist format 0, 1, 2 or 3 (default 1)")
    main(parser.parse_args())
