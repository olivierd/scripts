/**
 * $CC -Wall `pkconf --cflags --libs glib-2.0 gdk-pixbuf-2.0 \
 * -o pixbuf-formats pixbuf-formats.c
 *
 *  Get image formats supported by GdkPixbuf.
 */

#include <stdio.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <gmodule.h> /* GSlist */
#include <gdk-pixbuf/gdk-pixbuf.h>


int
main (int argc, char *argv[])
{
    GSList *formats, *elem;
    guint i;

    formats = gdk_pixbuf_get_formats ();
    for (i = 0; (elem = g_slist_nth (formats, i)); i++) {
        GdkPixbufFormat *fmt = elem->data;
        /*gchar *name;

        name = gdk_pixbuf_format_get_name (fmt);
        g_fprintf (stdout, "%s\n", name);
        g_free (name);*/
        gchar **mimes_list;
        gchar *mimes;

        mimes_list = gdk_pixbuf_format_get_mime_types (fmt);
        mimes = g_strjoinv (", ", mimes_list);
        g_strfreev (mimes_list);

        g_fprintf (stdout, "%s\n", mimes);
        g_free (mimes);
    }

    g_slist_free (formats);

    return 0;
}
