/**
* Display the standard host name.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
    char *hostname = NULL;
    size_t name_max = sysconf (_SC_HOST_NAME_MAX);

    hostname = (char *)malloc (name_max * sizeof (char));

    if (gethostname (hostname, name_max) == 0) {
        fprintf (stdout, "%s\n", hostname);
    }

    free (hostname);

    return 0;
}
