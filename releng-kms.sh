#!/bin/sh

# Check which kms driver to use in -RELENG.
#

RELENG_VER=$(sysctl -n kern.osrelease | awk -F - '{print $1}')
PCIID=$(pciconf -l | grep vga | cut -d " " -f 3 | cut -c 6-11)
PORTSDIR="/usr/ports"

if [ ! -f drm_pciids.h_BASE ]; then
	fetch -o drm_pciids.h_BASE \
		https://svn.freebsd.org/base/releng/${RELENG_VER}/sys/dev/drm2/drm_pciids.h
fi

base_result=$(grep -i ${PCIID} drm_pciids.h_BASE | cut -f 2)
echo "Support in the base driver: ${base_result:-No match}"

if [ -d ${PORTSDIR} ]; then
	_FILE="${PORTSDIR}/graphics/drm-fbsd12.0-kmod/Makefile";

	PORT_VER=$(grep PORTVERSION ${_FILE} | cut -f 2)
	COMMIT=$(grep GH_TAGNAME ${_FILE} | cut -f 2)

	fetch https://codeload.github.com/FreeBSDDesktop/kms-drm/tar.gz/${COMMIT}?dummy=/FreeBSDDesktop-kms-drm-${PORT_VER}-${COMMIT}_GH0.tar.gz

	tar -xzf FreeBSDDesktop-kms-drm-${PORT_VER}-${COMMIT}_GH0.tar.gz ;
	rm FreeBSDDesktop-kms-drm-${PORT_VER}-${COMMIT}_GH0.tar.gz ;

	for f in $(find kms-drm-${COMMIT} -type f -name '*_pciids.h'); do
		drm_result=$(grep -i ${PCIID} ${f} | cut -f 2)
		if [ ! -z "${drm_result}" ]; then
			comp=$(echo ${f} | awk -F _ '{print $1}')
			if [ $(basename ${comp}) = "i915" ]; then
				echo "Support in drm-kmod driver: Intel"
			elif [ $(basename ${comp}) = "drm" ]; then
				echo "Support in drm-kmod driver: AMD GPU"
			fi
		fi
	done

	rm -Rf kms-drm-${COMMIT}/
fi
