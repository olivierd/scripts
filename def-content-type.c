#include <stdio.h>
#include <stdlib.h> /* free() */

#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>

int
main(int argc, char *argv[])
{
	GFileInfo *info;
	gchar *content_type;
	GIcon *icon;
	gchar *name = NULL;

	info = g_file_query_info ();
	
	content_type = g_content_type_get_description ("application/x-bat");
	icon = g_content_type_get_icon (content_type);

	g_free (content_type);

	name = g_icon_to_string (icon);
	if (name != NULL)
		g_printf ("%s\n", name);

	g_object_unref (icon);

	return 0;
}
