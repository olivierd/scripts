#!/usr/bin/env python

'''Monitor file using kqueue/kevent through the 'select' standard
module.'''

import os
import time
import select

def main():
    filename = 'test.txt'

    try:
        fd = os.open(filename, os.O_RDONLY)
        # os.path.getmtime(filename) is similar
        last_mtime = os.fstat(fd).st_mtime
        print time.strftime('%Y-%m-%d %H:%M:%S',
                            time.localtime(last_mtime))

        kq = select.kqueue()
        ke = select.kevent(fd, filter=select.KQ_FILTER_VNODE,
                           flags=select.KQ_EV_ADD | select.KQ_EV_CLEAR,
                           fflags=select.KQ_NOTE_WRITE)

        kq.control(None, 0, None)
        while True:
            events = kq.control([ke], 1)
            if events:
                mtime = os.fstat(fd).st_mtime
                if mtime != last_mtime:
                    #print 'Something happened'
                    print time.strftime('%Y-%m-%d %H:%M:%S',
                                        time.localtime(mtime))

        kq.close()
        os.close(fd)
    except IOError:
        pass

if __name__ == '__main__':
    main()
