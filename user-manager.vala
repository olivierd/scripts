/**
* valac --pkg=glib-2.0 --pkg=gio-2.0 user-manager.vala
*
* Works only with ConsoleKit2.
**/

[DBus (name = "org.freedesktop.ConsoleKit.Manager")]
interface SystemManager : GLib.Object {
	public abstract GLib.ObjectPath[] get_sessions () throws GLib.Error;
}

[DBus (name = "org.freedesktop.ConsoleKit.Session")]
interface SystemSession : GLib.Object {
	public abstract string get_session_class () throws GLib.Error;
	public abstract string get_session_state () throws GLib.Error;
}


static void
user_session_state (string path) {
	SystemSession session_iface = null;
	string session_state;

	try {
		session_iface = GLib.Bus.get_proxy_sync (GLib.BusType.SYSTEM,
                                                 "org.freedesktop.ConsoleKit",
                                                 path);
		if (session_iface != null) {
			if (session_iface.get_session_class () == "user") {
				session_state = session_iface.get_session_state ();
				stdout.printf ("%s\n", session_state);
			}
			else {
				stderr.printf ("Not a normal user session\n");
			}
		}
	} catch (GLib.Error err) {
		stderr.printf ("Error: %s\n", err.message);
	}
}

static int
main (string[] argv) {
	SystemManager manager_iface = null;
	GLib.ObjectPath[] sessions;

	try {
		manager_iface = GLib.Bus.get_proxy_sync (GLib.BusType.SYSTEM,
                                                 "org.freedesktop.ConsoleKit",
                                                 "/org/freedesktop/ConsoleKit/Manager");
		if (manager_iface != null) {
			sessions = manager_iface.get_sessions ();
			foreach (GLib.ObjectPath session in sessions) {
				//stdout.printf ("%s\n", session.to_string ());
				user_session_state (session.to_string ());
			}
		}
		else {
			stderr.printf ("Error\n");
		}
	} catch (GLib.Error err) {
		stderr.printf ("Error: %s\n", err.message);
	}

	return 0;
}
