/**
 * gcc -Wall `pkgconf --cflags --libs glib-2.0 gmodule-2.0 libarchive` \
 *     -o test-libarchive test-libarchive.c
 *
 *  Simple script using libarchive.
 **/

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>
#include <gmodule.h>
#include <archive.h>


static const char *filename = NULL;

static const GOptionEntry options[] = {
	{ "archive", 'a', 0, G_OPTION_ARG_FILENAME, &filename,
      "Tarball", "file" },
    { NULL }
};

int
main (int argc, char *argv[])
{
	GOptionContext *ctx;
	GError         *error = NULL;
	struct archive *a;
	/*gint            r;*/

	ctx = g_option_context_new ("- libarchive example");
	g_option_context_set_help_enabled (ctx, TRUE);
	g_option_context_set_ignore_unknown_options (ctx, TRUE);
	g_option_context_add_main_entries (ctx, options, NULL);

	if (!g_option_context_parse (ctx, &argc, &argv, &error)) {
		g_print ("Error: %s\n", error->message);

		g_error_free (error);
	}
	else {
		if (filename != NULL) {
			/*
			a = archive_read_new ();
			if (a != NULL) {
				archive_read_support_filter_all (a);
				archive_read_support_format_all (a);

				r = archive_read_open_filename (a, filename, 10240);
				g_print ("%s\n", filename);
			}
			else {
				perror ("archive_read_new");
			}

			archive_read_free (a);*/

			if (g_utf8_validate (filename, -1, NULL)) {
				g_print ("Ok\n");
			}
			else {
				g_print ("Need convert to UTF-8\n");
			}
		}
	}

	g_option_context_free (ctx);

	return 0;
}
