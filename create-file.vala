/**
* valac --pkg=glib-2.0 --pkg=gobject-2.0 --pkg=gio-2.0 create-file.vala
*
*/

static bool
file_exists (string filename) {
    GLib.File file;
    bool result = false;

    file = GLib.File.new_for_path (filename);
    if (file.query_exists ()) {
        result = true;
    }

    return result;
}

static int
main (string[] args) {
    unowned string config_dir;
    string file;

    config_dir = GLib.Environment.get_user_config_dir ();
    file = GLib.Path.build_filename (config_dir,
                                     "plank-initial-setup-done");

    if (file_exists (file)) {
        string content;

        try {
            GLib.FileUtils.get_contents (file, out content);
            stdout.printf ("%s\n", content);
        } catch (GLib.FileError err) {
            stderr.printf ("Error\n");
        }
    }
    else {
        try {
            GLib.FileUtils.set_contents (file, "yes", -1);
        } catch (GLib.FileError err) {
            stderr.printf ("Error\n");
        }
    }

    return 0;
}
