#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import inspect
import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, GObject, Gio

try:
    gi.require_version('libxfce4util', '1.0')
    from gi.repository import libxfce4util

    if inspect.isclass(libxfce4util.Rc):
        have_libxfce4util = True
    else:
        have_libxfce4util = False
except (ValueError, AttributeError):
    have_libxfce4util = False

try:
    gi.require_version('Tracker', '2.0')
    from gi.repository import Tracker

    have_tracker = True
except ValueError:
    have_tracker = False

try:
    gi.require_version('Zeitgeist', '2.0')
    from gi.repository import Zeitgeist

    have_zeitgeist = True
except ValueError:
    have_zeitgeist = False


default_settings = {'show-hidden-files': False,
                    'show-sidebar': False,
                    'window-width': 650,
                    'window-height': 470,
                    'window-x': -1,
                    'window-y': -1}
# locate(1) stuff
if sys.platform.startswith('linux'):
    __locate_db_path__ = '/var/lib/mlocate/mlocate.db'
    __update_db__ = 'updatedb'
elif 'bsd' in sys.platform or sys.platform.startswith('dragonfly'):
    __locate_db_path__ = '/var/db/locate.database'
	# Outside of PATH
    __update_db__ = '/usr/libexec/locate.updatedb'


def get_locate_db_path():
    return __locate_db_path__

def get_locate_utilities():
    '''Return a tuple.'''

    up = GLib.find_program_in_path(__update_db__)
    if up is not None:
        return (GLib.find_program_in_path('locate'), up)
    else:
        return ('locate', __update_db__)


class CatfishSettings(object):

    def __init__(self, settings_file='catfish/catfish.rc'):

        self._settings = {}

        if have_libxfce4util:
            p = libxfce4util.resource_save_location(libxfce4util.ResourceType.CONFIG,
                                                    settings_file, True)
            self.settings_path = pathlib.Path(p)
        else:
            settings_dir = GLib.get_user_config_dir()
            p = pathlib.Path(settings_dir)
            self.settings_path = p / settings_file
            if not self.settings_path.parent.exists():
                self.settings_path.parent.mkdir(parents=True)

        if not self.settings_path.exists():
            self.settings_write()
        else:
            self.settings_read()
            #if not 'locate_db' in self._settings:
            #    self.settings_write()

    def get_current_desktop(self):
        desktop = GLib.environ_getenv(GLib.get_environ(),
                                      'XDG_CURRENT_DESKTOP')

        return desktop.lower()

    if have_tracker:
        def tracker_idle(self):
            '''Check if org.freedesktop.Tracker1 service exists and
            daemon is running.'''
            result = False

            conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
            proxy = Gio.DBusProxy.new_sync(conn, Gio.DBusProxyFlags.NONE,
                                           None,
                                           'org.freedesktop.Tracker1',
                                           '/org/freedesktop/Tracker1/Status',
                                           'org.freedesktop.Tracker1.Status',
                                           None)
            if proxy is not None:
                # Idle or Progress
                variant = proxy.call_sync('GetStatus', None,
                                          Gio.DBusCallFlags.NONE,
                                          -1, None)
                res = variant.get_child_value(0).get_string()
                if res == 'Idle':
                    result = True

            return result

    if have_zeitgeist:
        def zeitgeist_acquired_proxy(self):
            '''Check org.gnome.zeitgeist.Engine service is
            connected to Gio.DBusProxy.'''
            result = False

            log = Zeitgeist.Log.new()
            if log.get_is_connected() and log.get_proxy_created():
                result = True

            return result

    if have_libxfce4util:
        def _write(self, dict_settings, filename):
            # We open 'filename' in read/write mode
            rc = libxfce4util.rc_simple_open(filename, False)
            if isinstance(rc, libxfce4util.Rc):
                rc.set_group('Configuration')

                for k in iter(dict_settings):
                    value = dict_settings.get(k)
                    if isinstance(value, bool):
                        rc.write_bool_entry(k, value)
                    elif isinstance(value, int):
                        rc.write_int_entry(k, value)
                    elif isinstance(value, str):
                        rc.write_entry(k, value)

                # Check if everything has been written
                rc.flush()

            rc.close()

        def _read(self, filename):
            res = True

            # We open 'filename' in read mode
            rc = libxfce4util.rc_simple_open(filename, True)
            if isinstance(rc, libxfce4util.Rc):
                gp = rc.get_group()
                if gp == 'Configuration':
                    res = False

                    for i in rc.get_entries(gp):
                        if 'show-' in i:
                            self._settings[i] = rc.read_bool_entry(i,
                                                                   False)
                        elif i == 'tracker' or i == 'zeitgeist':
                            self._settings[i] = rc.read_bool_entry(i,
                                                                   False)
                        elif 'window-' in i:
                            self._settings[i] = rc.read_int_entry(i, 0)
                        else:
                            self._settings[i] = rc.read_entry(i, '')

            rc.close()

            return res
    else:
        def _write(self, dict_settings, filename):
            conf_parser = configparser.ConfigParser()
            conf_parser['Configuration'] = dict_settings

            with open(filename, 'w') as f:
                conf_parser.write(f)

        def _read(self, filename):
            res = True
            conf_parser = configparser.ConfigParser()

            try:
                conf_parser.read(filename)
                if conf_parser.has_section('Configuration'):
                    res = False

                    for i in conf_parser.options('Configuration'):
                        if 'show-' in i:
                            self._settings[i] = conf_parser.getboolean('Configuration',
                                                                       i)
                        elif i == 'tracker' or i == 'zeitgeist':
                            self._settings[i] = conf_parser.getboolean('Configuration',
                                                                       i)
                        elif 'window-' in i:
                            self._settings[i] = conf_parser.getint('Configuration',
                                                                   i)
                        else:
                            self._settings[i] = conf_parser.get('Configuration',
                                                                i)
            except configparser.MissingSectionHeaderError:
                pass

            return res

    def settings_write(self):
        self._settings = default_settings.copy()

        self._settings.update({'desktop': self.get_current_desktop()})

        # locate(1)
        locate_utilities = get_locate_utilities()
        self._settings.update(locate=locate_utilities[0],
                              update_db=locate_utilities[1],
                              locate_db=get_locate_db_path())

        if have_tracker:
            if self.tracker_idle():
                self._settings.update({'tracker': True})

        if have_zeitgeist:
            if self.zeitgeist_acquired_proxy():
                self._settings.update({'zeitgeist': True})

        filename = '{0}'.format(self.settings_path)
        self._write(self._settings, filename)

    def settings_read(self):
        filename = '{0}'.format(self.settings_path)
        res = self._read(filename)
        # Config file has changed
        if res:
            # Remove previous config file
            self.settings_path.unlink()

            self.settings_write()

    @property
    def settings(self):
        return self._settings

def main():
    cat = CatfishSettings()

    if cat.settings:
        print(cat.settings)


if __name__ == '__main__':
    main()
