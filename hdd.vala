/**
* valac --pkg=glib-2.0 --pkg=gio-unix-2.0 --pkg=gio-2.0 hdd.vala
**/


public static uint64
get_size_from_filesystem (string path) {
	File file;
	FileInfo info;
	uint64 size = 0;
	
	file = File.new_for_path (path);
	try {
		info = file.query_filesystem_info (GLib.FileAttribute.FILESYSTEM_SIZE,
										   null);
		size = info.get_attribute_uint64 (GLib.FileAttribute.FILESYSTEM_SIZE);
	} catch (GLib.Error e) {
		stderr.printf ("Error: %s\n", e.message);
	}
	
	return size;
}


public static int
main (string[] argv) {
	List<UnixMountEntry> ume;
	string path;
	uint64 nbytes_total = 0;
	List<uint64?> list;
	
	list = new List<uint64?> ();
	
	ume = UnixMountEntry.get ();
	foreach (unowned UnixMountEntry mount_point in ume) {
		path = mount_point.get_mount_path ();
		
		if (path != null) {
			list.append (get_size_from_filesystem (path));
		}
	}
	
	for (int i = 0; i < list.length (); i++) {
		nbytes_total = nbytes_total + list.nth_data (i);
	}
	
	stdout.printf ("%s\n", GLib.format_size (nbytes_total));
	
	return 0;
}
