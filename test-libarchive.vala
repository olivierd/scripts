/*
* valac --pkg=glib-2.0 --pkg=gio-2.0 --pkg=libarchive \
*       test-libarchive.vala
*
* Simple example using libarchive.
*
*/

public class TestLibArchive : Object {

	private static string? filename = null;

	private const GLib.OptionEntry[] options = {
		{ "archive", 'a', 0, GLib.OptionArg.FILENAME, ref filename,
          "Tarball", null },
        { null }
	};

	static int main (string[] args)
	{
		GLib.OptionContext ctx;
		Archive.Read a;
		Archive.Result r;
		unowned Archive.Entry e;
		size_t size = 10240; /* taken from libarchive example */

		try {
			ctx = new GLib.OptionContext ();
			ctx.set_help_enabled (true);
			ctx.add_main_entries (options, null);
			ctx.parse (ref args);
		} catch (GLib.OptionError e) {
			stderr.printf ("Error: %s\n", e.message);
			stderr.printf ("Run '%s --help'\n", args[0]);

			return -1;
		}

		/* Check if archive exists */
		if ((filename != null) && (GLib.FileUtils.test (filename, GLib.FileTest.EXISTS))) {
			a = new Archive.Read ();
			a.support_filter_all ();
			a.support_format_all ();

			r = a.open_filename (filename, size);
			if (r != Archive.Result.OK) {
				stderr.printf ("Error\n");
				a.close ();
			}
			else {
				//stdout.printf ("Ok\n");
				while (a.next_header (out e) == Archive.Result.OK) {
					stdout.printf ("%s\n", e.pathname ());
					a.read_data_skip ();
				}
				a.close ();
			}
		}

		return 0;
	}
}
