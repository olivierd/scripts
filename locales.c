/*
 * Connection between locales and country name.
 */

#include <stdio.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>

gchar *build_filename_iso_code (const gchar *domain);
GList *locales_available (void);
gchar *get_country_code (gchar *locale);
xmlChar *get_country_name (gchar *filename, gchar *code);


gchar *
build_filename_iso_code (const gchar *domain)
{
		gchar *share_dir, *filename;

		share_dir = g_build_path (G_DIR_SEPARATOR_S,
															ISO_CODES_PREFIX,
															"share", NULL);
		filename = g_build_filename (share_dir,
																 "xml",
																 "iso-codes",
																 domain, NULL);
		g_free (share_dir);

		return filename;
}

GList *
locales_available (void)
{
		const gchar *command_line = "locale -a";

		gchar *locales = NULL;
		gchar **tokens;
		gboolean res;
		GError *err = NULL;
		gint i;
		GList *list = NULL;

		res = g_spawn_command_line_sync (command_line,
																		 &locales,
																		 NULL, NULL,
																		 &err);

		if (res) {
				tokens = g_strsplit_set (g_strstrip (locales), "\n\r", -1);
				g_free (locales);
		}
		else {
				fprintf (stderr, "Failed to run command: %s\n", err->message);
				g_error_free (err);
		}

		if (tokens) {
				for (i = 0; tokens[i]; i++) {
						list = g_list_append (list, tokens[i]);
				}
		}
		return list;
}

gchar *
get_country_code (gchar *locale)
{
		gchar **lang, **iso_abbr;
		gchar *encoding, *code, *country_code = NULL;

		lang = g_strsplit_set (locale, ".@", -1);
		if (g_strv_length (lang) == 2) {
				encoding = g_strdup (lang[1]);
				iso_abbr = g_strsplit (g_strdup (lang[0]), "_", -1);

				if (g_strv_length (iso_abbr) == 2) {
						code = g_strdup (iso_abbr[1]);
				}

				g_strfreev (iso_abbr);

				if (g_strcmp0 (encoding, "UTF-8") == 0
						|| g_strcmp0 (encoding, "utf8") == 0) {
						/*country_code = g_utf8_strdown (code, sizeof (code));*/
						country_code = code;
				}
				else {
						/*country_code = g_utf8_strdown (g_locale_to_utf8 (code, -1,
																														 NULL,
																														 NULL,
																														 NULL),
																														 sizeof (code));*/
						country_code = g_locale_to_utf8 (code, -1, NULL, NULL, NULL);
				}

				g_free (encoding);
		}
		g_strfreev (lang);

		return country_code;
}

xmlChar *
get_country_name (gchar *filename, gchar *code)
{
		const gchar *attrib = "name";
		gchar *string;
		int size;

		xmlChar *xpath, *res = NULL;
		xmlDocPtr doc;
		xmlXPathContextPtr ctx;
		xmlXPathObjectPtr obj;

		/* Build dynamic XPath expression */
		string = g_strdup_printf ("//iso_3166_entry[@alpha_2_code='%s']",
															code);
		xpath = xmlCharStrdup (string);
		g_free (string);

		/* Parse XML file */
		doc = xmlParseFile (filename);
		ctx = xmlXPathNewContext (doc);

		if (!ctx)
				xmlFreeDoc (doc);

		/* Evaluate XPath expression */
		obj = xmlXPathEvalExpression (xpath, ctx);
		if (!obj) {
				printf ("Failed to evaluate XPath expression\n");
				xmlXPathFreeContext (ctx);
				xmlFreeDoc (doc);
		}

		size = obj->nodesetval->nodeNr;
		if (size == 1)
				res = xmlGetProp (obj->nodesetval->nodeTab[0],
													xmlCharStrdup (attrib));

		xmlXPathFreeObject (obj);
		xmlXPathFreeContext (ctx);
		xmlFreeDoc (doc);

		return res;
}

void
display (gpointer data, gpointer user_data)
{
		gchar *iso_code;

		iso_code = get_country_code (data);
		printf (user_data, data, iso_code);
}

int
main (int argc, char *argv[])
{
		const gchar *iso_file = "iso_3166.xml";

		gchar *src, *iso_code, *elem;
		guint size, i;
		GFile *file;
		GList *locales;
		xmlChar *country;

		src = build_filename_iso_code (iso_file);
		file = g_file_new_for_path (src);
		if (g_file_query_exists (file, NULL)) {
				g_object_unref (file);

				locales = locales_available ();

				/* g_list_foreach (locales, (GFunc) display, "%s %s\n"); */
				size = g_list_length (locales);
				for (i = 0; i < size; i++) {
						elem = (gchar *) g_list_nth (locales, i)->data;
						if (g_strcmp0 (elem, "C") == 0 || g_strcmp0 (elem, "POSIX") == 0)
								continue;

						iso_code = get_country_code (elem);
						country = get_country_name (src, iso_code);
						if (!country)
								printf ("%s\n", elem);
						else
								printf ("%s %s\n", elem, (gchar *) country);
				}

				g_list_free (locales);
		}

		return 0;
}
