/**
 * Display each category from pkg(8).
 *
 * cc -Wall `pkgconf --cflags --libs pkg` -L/usr/include -lc \
 * -o get-categories categories.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <pkg.h>


/*
void
pkgdb_repo_categories(char *reponam)
{
	char 
	sqlite3_stmt *stmt;
	int ret;
	const char *sql = "SELECT name FROM categories ORDER BY name;";

	if (sqlite3_prepare_v2(db->sqlite, sql, -1, &stmt, NULL) != SQLITE_OK) {
		perror("sqlite3_prepare_v2");
	}

	while ((ret = sqlite3_step(stmt)) == SQLITE_ROW) {
		printf("%s\n", sqlite3_column_text(stmt, 0));
	}

	sqlite3_finalize(stmt);
}
*/

void
build_db_path(const char *reponame)
{
	const char *dbdir;
	char dbpath[MAXPATHLEN];
	char *buf;

	dbdir = pkg_object_string(pkg_config_get("PKG_DBDIR"));
	snprintf(dbpath, sizeof(dbpath), "%s/repo-%s.sqlite", dbdir,
			 reponame);

	printf("%s\n", dbpath);
	if ((buf = malloc(strlen(dbpath))) != NULL) {
		buf = &dbpath[0];

		printf("%s\n", buf);
		free(buf);
	}
}

/**
 * Return main repository, with higher priority.
 */
const char *
get_main_repo(struct pkg_repo *r)
{
	unsigned int priority = 0;
	const char *name = NULL;
	
	while (pkg_repos(&r) == EPKG_OK) {
		if (pkg_repo_enabled(r)) {
			if (pkg_repo_priority(r) > priority) {
				priority = pkg_repo_priority(r);
				name = pkg_repo_name(r);
			}
		}
	}

	return name;
}

int
main(int argc, char *argv[])
{
	int err;
	struct pkg_repo *repo = NULL;
	const char *reponame;

	if (!pkg_initialized()) {
		err = pkg_init(NULL, NULL);
		if (err != EPKG_OK)
			printf("Can't initialized\n");
		else {
			err = pkgdb_access(PKGDB_MODE_READ, PKGDB_DB_REPO);
			if (err == EPKG_ENOACCESS) {
				printf("Insufficient privileges\n");
			}
			else if (err == EPKG_OK) {
				reponame = get_main_repo(repo);
				if (reponame != NULL) {
					build_db_path(reponame);
				}
			}		
		}
	}

	pkg_shutdown();

	return 0;
}
