/**
 * Display enabled repository name from pkg(8).
 *
 * cc -Wall `pkgconf --cflags --libs pkg` -L/usr/include -lc \
 * -o get-repos repos.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <pkg.h>

int
main(int argc, char *argv[])
{
	int err;
	struct pkg_repo *repo;

	if (!pkg_initialized()) {
		err = pkg_init(NULL, NULL);

		if (err != EPKG_OK)
			printf("Nooooo\n");
		else {
			//printf("%d\n", pkg_repos_activated_count());

			while (pkg_repos(&repo) == EPKG_OK) {
				if (pkg_repo_enabled(repo))
					printf("%s\n", pkg_repo_name(repo));
			}
		}
	}

	pkg_shutdown();
	
	return 0;
}
