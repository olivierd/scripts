/**
 * Display default settings.
 *
 * cc -Wall `pkgconf --cflags --libs pkg` -L/usr/include -lc \
 * -o pkg-settings settings.c
 */

#include <stdlib.h>
#include <stdio.h>

#include <pkg.h>


int
main(int argc, char *argv[]) {
	const char *dbdir;
	int err;

	if (!pkg_initialized()) {
		err = pkg_init(NULL, NULL);
		if (err != EPKG_OK) {
			perror("pkg_init");
		}
		else {
			dbdir = pkg_object_string(pkg_config_get("PKG_DBDIR"));
			// dbdir -> /var/db/pkg
			printf("%s\n", dbdir);
		}
	}

	pkg_shutdown();

	return 0;
}
