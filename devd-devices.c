/**
 * Simple C script in order to use kqueue/kevent and libgeom.
 *
 * clang -Wall -lc devd-devices.c -o devd-devices
 */

#include <stdio.h>
#include <string.h> /* strlcpy */
#include <sys/socket.h>
#include <sys/un.h> /* struct sockaddr_un */
#include <sys/event.h>


#define DEVD_SOCKET "/var/run/devd.pipe"


int
main (int argc, char *argv[])
{
	int fd, kq, res;
	struct sockaddr_un addr;
	struct kevent event;
	ssize_t ret;
	char buf[1024];

	fd = socket (PF_UNIX, SOCK_STREAM, 0);
	if (fd < 0) {
		perror ("socket");
	}

	addr.sun_family = AF_UNIX;
	strlcpy (addr.sun_path, DEVD_SOCKET, sizeof (addr.sun_path));

	if (connect (fd, (struct sockaddr *) &addr, sizeof (struct sockaddr_un)) < 0) {
		perror ("connect");
	}

	/* Create kqueue */
	kq = kqueue ();
	if (kq < 0) {
		perror ("kqueue");
	}
	else {
		/* Initialize kevent structure */
		EV_SET (&event, fd, EVFILT_READ, EV_ADD | EV_ENABLE, 0, 0, NULL);
		/* Attach event to the kqueue */
		res = kevent (kq, &event, 1, NULL, 0, NULL);
		if (res < 0) {
			perror ("kevent");
		}

		/* Infinite loop */
		for (;;) {
			ret = read (fd, buf, event.data);
			printf ("%s\n", buf);
		}
	}
	
	if (fd > 0)
		close (fd);
	
	return 0;
}
