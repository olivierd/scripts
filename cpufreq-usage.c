/**
 * Example to display cpufreq information.
 *
 * cc -Wall -I/usr/include -lc -o cpufreq-usage cpufreq-usage.c
 */

#include <stdio.h>
#include <stdlib.h> /* free() */
#include <string.h> /* strsep() */
#include <sys/types.h>
#include <sys/sysctl.h>


char *
split(char *string, const char *delim)
{
	char *tokens = NULL;

	tokens = strsep(&string, delim);
	if (tokens != NULL || tokens != '\0')
	{
		/* We need only the first fragment */
		return tokens;
	}
	else
		return NULL;
}

void
current_freq(void)
{
	unsigned long freq;
	size_t len = sizeof(freq);

	if (sysctlbyname("dev.cpu.0.freq", &freq, &len, NULL, 0) < 0)
		perror("sysctlbyname");

	printf("%lu\n", freq);
}

void
cpufreq_update(void)
{
	char buf[1024];
	char *buf_cp, *tokens;
	unsigned long min = 0 , max = 0;
	size_t len = sizeof(buf);

	if (sysctlbyname("dev.cpu.0.freq_levels", buf, &len, NULL, 0) < 0)
		perror("sysctlbyname");

	buf_cp = strndup(buf, len);
	if (buf_cp == NULL)
		free(buf_cp);


	while ((tokens = strsep(&buf_cp, " ")) != NULL)
	{
		char *tmp;
		unsigned long freq;

		tmp = split(tokens, "/");
		if (tmp != NULL)
		{
			freq = strtol(tmp, &tmp, 10);

			if (freq > max)
			{
				max = freq;
			}
			else {
				if ((min == 0) || (freq < min))
					min = freq;
			}
		}
		//printf("%s\n", split(tokens, "/"));
	}

	printf("Max: %lu - Min: %lu\n", max, min);

	free(buf_cp);
}

int
n_cpu(void)
{
	static int mib[] = {CTL_HW, HW_NCPU};
	int buf;
	size_t len = sizeof(int);

	if (sysctl(mib, 2, &buf, &len, NULL, 0) < 0)
		return 0;
	else
		return buf;
}

int
main(int argc, char *argv[])
{
	int ncpu;

	ncpu = n_cpu();
	if (ncpu > 0)
	{
		printf("%d\n", ncpu);

		current_freq();

		cpufreq_update();
	}

	return 0;
}
