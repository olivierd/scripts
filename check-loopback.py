#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Unlicense
#

import ipaddress
import socket

def main():
  # IPv4
  ip_loopback = ipaddress.ip_network('127.0.0.1/32')
  # IPv6
  #ip_loopback = ipaddress.ip_network('::1')
  
  if ip_loopback.num_addresses == 1:
    hostname, alias_list, ipaddr_list = socket.gethostbyaddr('{0}'.format(ip_loopback.hosts()[0]))
    print(socket.getfqdn(hostname))
    
    if alias_list:
      print(alias_list)
    if ipaddr_list:
      print(ipaddr_list)


if __name__ == '__main__':
  main()
