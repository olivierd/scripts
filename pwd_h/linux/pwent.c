/**
 * Get record from the password database.
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <shadow.h>

#define PATH_SHADOW "/etc/shadow"


void
shadow_entries (void)
{
    FILE *fp;
    int ret = 0;
    struct spwd spw;
    struct spwd *spwp;
    char buf[1024];

    fp = fopen (PATH_SHADOW, "r");
    if (fp == NULL) {
        fprintf (stderr, "Unable to open password database\n");
    }
    else {
        do {
            ret = fgetspent_r (fp, &spw, buf, sizeof (buf), &spwp);
            if (ret == 0) {
                fprintf (stdout, "%s → %s\n", spwp->sp_namp,
                         spwp->sp_pwdp);
            }
        } while (spwp != NULL);
    }

    fclose (fp);
}

/**
 * don't use the supplied steam
 */
void
shadow_entries_v2 (void)
{
    int ret = 0;
    struct spwd spw;
    struct spwd *spwp;
    char buf[1024];

    setspent ();

    do {
        ret = getspent_r (&spw, buf, sizeof (buf), &spwp);
        if (ret == 0) {
            fprintf (stdout, "%s → %s\n", spwp->sp_namp,
                     spwp->sp_pwdp);
            }
    } while (spwp != NULL);

    endspent ();
}


void
password_entries (void)
{
    struct passwd pw;
    struct passwd *pwp;
    int ret = 0;
    char buf[1024];

    /* rewind to the beginning of the password database */
    setpwent ();

    do {
        ret = getpwent_r (&pw, buf, sizeof (buf), &pwp);
        if (ret == 0) {
            fprintf (stdout, "%s → %s\n", pwp->pw_name,
                     pwp->pw_passwd);
        }
    } while (pwp != NULL);

    /* close the password database */
    endpwent ();
}


int
main (int argc, char *argv[])
{
    uid_t u_id = getuid ();
    uid_t eu_id = geteuid ();

    if (u_id == 0 || u_id != eu_id) {
        /*shadow_entries ();*/
        shadow_entries_v2 ();
    }
    else {
        password_entries ();
    }

    return 0;
}
