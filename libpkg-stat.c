/**
 *
 * Simple script, which displays stats (using libpkg).
 *
 * cc -Wall -lc -lpkg -I/usr/include -I/usr/local/include
 */

#include <stdio.h>
#include <inttypes.h> /* PRId64 macro */

#include <pkg.h>


int
main (int argc, char *argv[])
{
	struct pkgdb *db = NULL;
	int64_t stat;

	if (pkgdb_open (&db, PKGDB_REMOTE) != EPKG_OK) {
		perror ("pkgdb_open");
	}
	else {
		if (pkgdb_obtain_lock (db, PKGDB_LOCK_READONLY) != EPKG_OK) {
			perror ("pkgdb_obtain_lock");
		}
		else {
			stat = pkgdb_stats (db, PKG_STATS_REMOTE_REPOS);
			printf ("%" PRId64 "\n", stat);
		}
	}

	pkgdb_close (db);

	return 0;
}
