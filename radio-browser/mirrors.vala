/**
* Get a list of available servers for the API from radio-browser.info.
*
* valac --pkg=glib-2.0 --pkg=gio-2.0 mirrors.vala
*
*/


static void
dns_lookup (GLib.Resolver resolver, GLib.InetAddress addr) {
    string hostname = null;

    try {
        hostname = resolver.lookup_by_address (addr, null);
        if (hostname != null) {
            stdout.printf ("%s\n", hostname);
        }
    } catch (GLib.Error err) {
        stderr.printf ("Error: %s\n", err.message);
    }
}

static int
main (string[] args) {
    GLib.Resolver resolver;
    GLib.List<GLib.InetAddress> addresses;

    resolver = GLib.Resolver.get_default ();
    try {
        addresses = resolver.lookup_by_name ("all.api.radio-browser.info",
                                             null);

        foreach (GLib.InetAddress addr in addresses) {
            // We are interesting to IPv4 only!
            if (addr.family == GLib.SocketFamily.IPV4) {
                //stdout.printf ("%s\n", addr.to_string ());
                dns_lookup (resolver, addr);
            }
        }
    } catch (GLib.Error err) {
        stderr.printf ("Error: %s\n", err.message);
    }

    return 0;
}
