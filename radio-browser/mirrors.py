#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Get a list of available servers.
'''

import socket


def main():
    servers = []
    addr = socket.getaddrinfo('all.api.radio-browser.info', 80,
                              proto=socket.IPPROTO_TCP)
    for elem in addr:
        # Check only IPv4!
        if elem[0] == socket.AF_INET:
            ip_addr = elem[-1][0]
            host_info = socket.gethostbyaddr(ip_addr)

            if host_info[0] not in servers:
                servers.append(host_info[0])

    if servers:
        print(servers)

if __name__ == '__main__':
    main()
