#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Delete some part in filename.
'''

import argparse
import os
import pathlib
import sys


def crop(files, sep):
    for f in files:
        if isinstance(f, pathlib.Path):
            # It is a pathlib.Path object
            dirname = f.parent
            # It is a string
            basename = f.name

            s = basename.split(sep)
            # We want only the last part!
            p = dirname.joinpath('{0}{1}'.format(sep, s[-1]))
            f.rename(p)
        else:
            print('Internal error')
            sys.exit(0)

def get_files(path):
    files = []

    with os.scandir(path) as elements:
        for elem in elements:
            if elem.is_file():
                files.append(pathlib.Path(elem.path))
    return files

def absolute_path(pathname):
    if pathname.startswith('~'):
        p = pathlib.Path(pathname).expanduser()
    else:
        p = pathlib.Path(pathname).resolve()

    if p.exists():
        return p
    else:
        print('Error: no such file or directory')
        sys.exit(0)

def main(args):
    root_dir = absolute_path(args.directory)
    if root_dir is not None:
        files = get_files(root_dir)
        if files:
            crop(files, args.separator)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', required=True,
                        help='directory which contains files to rename')
    parser.add_argument('-s', '--separator', required=True,
                        help='element uses to rename files')
    main(parser.parse_args())
