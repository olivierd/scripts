#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import pathlib
import sys

def add_nzeros(nb, limit):
    '''Add n zeros as prefix.'''
    if limit > 100 and limit < 1000:
        return '{0:03d}'.format(int(nb))

def rename_files(files):
    size = len(files)

    for i in files:
        if isinstance(i, pathlib.Path):
            # It is pathlib.Path object!
            dirname = i.parent
            # It is string!
            basename = i.name

            tokens = basename.split('.')
            if len(tokens) == 2:
                s = '.'.join([add_nzeros(tokens[0], size), tokens[-1]])

                p = dirname / '{0}'.format(s)
                i.rename(p)
        else:
            print('Internal error')
            sys.exit(0)

def get_files(path):
    '''Return sorted list of files.'''
    list_files = []

    with os.scandir(path) as files:
        for i in files:
            if i.is_file():
                list_files.append(pathlib.Path(i.path))

    # Case-insensitive string comparaison
    if list_files:
        return sorted(list_files,
                      key=lambda str: '{0}'.format(str).lower())
    else:
        return None

def check_directory(path):
    res = False

    if isinstance(path, pathlib.Path):
        if path.is_dir() and path.exists():
            res = True

    return res

def main(args):
    # We want pathlib.Path object
    if args.directory.startswith('~'):
        root_dir = pathlib.Path(args.directory).expanduser()
    else:
        root_dir = pathlib.Path(args.directory).resolve()
    if check_directory(root_dir):
        files = get_files(root_dir)
        if files:
            rename_files(files)
        else:
            print('Noooo')
    else:
        print('No such file or directory')
        sys.exit(-1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', required=True,
                        help='directory which contains files to rename')
    main(parser.parse_args())
