/**
 * Display network interfaces, like ifconfig -a.
 *
 * cc -Wall -I/usr/include -lc -o ifaces ifaces.c
 */

#include <stdio.h>
#include <stdbool.h> /* true, false */
#include <net/if.h> /* MUST be included before <ifaddrs.h> */
#include <net/if_mib.h> /* struct ifmibdata, NETLINK_GENERIC ... */
#include <net/if_types.h> /* IFT_... */
#include <ifaddrs.h> /* getifaddrs() */
#include <sys/socket.h> /* PF_LINK */
#include <sys/types.h>
#include <sys/sysctl.h>


bool
has_sysctl_property(const char *name)
{
	bool res = false;
	size_t len = sizeof(int);

	if (sysctlbyname(name, NULL, &len, NULL, 0) == 0){
		res = true;
	}

	return res;
}


void
get_ifmbdata()
{
	int if_count;
	size_t len;
	int name[] = { CTL_NET,
				   PF_LINK,
				   NETLINK_GENERIC,
				   IFMIB_SYSTEM,
				   IFMIB_IFCOUNT };
	struct ifmibdata ifmd;

	len = sizeof(int);
	if (sysctl(name, 5, &if_count, &len, NULL, 0) < 0) {
		perror("sysctl");
	}

	for (int i = 1; i <= if_count; i++) {
		len = sizeof(ifmd);

		int name[] = { CTL_NET,
					   PF_LINK,
					   NETLINK_GENERIC,
					   IFMIB_IFDATA,
					   i,
					   IFDATA_GENERAL };

		if (sysctl(name, 6, &ifmd, &len, NULL, 0) < 0) {
			perror("sysctl");
		}
		
		if ((ifmd.ifmd_data.ifi_type == IFT_ETHER) ||
			(ifmd.ifmd_data.ifi_type == IFT_IEEE80211)) {
			if ((ifmd.ifmd_data.ifi_link_state == LINK_STATE_UP) &&
				(ifmd.ifmd_data.ifi_ipackets > 0)) {
				printf("%s - packets received: %lu\n", ifmd.ifmd_name,
					   ifmd.ifmd_data.ifi_ipackets);
			}
		}
	}
}


int
main(int argc, char *argv[])
{
	//struct ifaddrs *ifap = NULL;

	if (has_sysctl_property("net.link.generic.system.ifcount")) {
		/*
		if (getifaddrs(&ifap) < 0) {
			perror("getifaddrs");
		}

		while (ifap->ifa_next != NULL) {
			printf("%s\n", ifap->ifa_name);

			ifap = ifap->ifa_next;
		}

		freeifaddrs(ifap);
		*/
		get_ifmbdata();
	}
	
	return 0;
}
