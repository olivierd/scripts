#!/usr/bin/env python

import argparse
import os
import sys


def get_list_directories(dirname):
    """Return list of directories."""

    list = []
    
    if os.path.exists(dirname):
        list = os.listdir(dirname)

    return list

def guess_prefix(nb):
    if int(nb) < 10:
        return '0%s' % nb
    else:
        return nb

def prepare_rename(path):
    base = os.path.basename(path)
    root = os.path.dirname(path)

    tokens = base.split(' ')
    size = len(tokens)
    new_base = None
    
    if size == 2:
        new_base = '-'.join([tokens[0], guess_prefix(tokens[1])])

    if new_base is not None:
        os.rename(path, os.path.join(root, new_base))
    else:
        print path

def main (opt):
    # Normalize path
    if opt.startswith('~'):
        root = os.path.expanduser(opt)
    else:
        root = os.path.abspath(opt)

    dirs = get_list_directories(root)
    if dirs:
        for d in dirs:
            prepare_rename(os.path.join(root, d))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', nargs=1, help='Root directory')

    args = parser.parse_args()
    main(''.join(args.directory))
