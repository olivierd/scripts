#!/bin/sh -eu
#
# Copyright (c) 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
# All rights reserverd.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#

# Local repository
LOCAL_REPO="/home/olivierd/tmp"
# Version supported
BRANCH="0.12"
# Plasma5 stuff (from FreeBSD's KDE repository)
KDE_BRANCH="kde5-import"

#### Don't change anything below

PRGFILE="`realpath "$0"`"
PRGNAME="${0##*/}"

CWD=`pwd`

err()
{
		echo "$@" >&2
		exit 1
}

usage()
{
		cat <<EOF >&2
usage: ${PRGNAME} options

options:
	-f	fetch required files
	-r	revert changes
EOF
		exit 0
}

check_svn()
{
		if [ -x $(which svn 2>/dev/null) ]; then
				echo $(which svn);
		elif [ -x $(which svnlite 2>/dev/null) ]; then
				echo $(which svnlite);
		else
				err "ERROR: neither svn(1) nor svnlite(1) found";
		fi
}

fetch()
{
		svn_cmd=$(check_svn)

		if [ ! -d "${LOCAL_REPO}/.svn" ]; then
				err "ERROR: you must clone repository first"
		fi
		
		FREEBSD_REPO="https://github.com/freebsd/freebsd-ports-kde/"
		if [ "x${KDE_BRANCH}" != "x" ]; then
				GITHUB_URL="${FREEBSD_REPO}/branches/${KDE_BRANCH}"

				cd ${LOCAL_REPO};

				mkdir plasma5 ; cd plasma5 ;
				# Fetch Uses/kde.mk
				${svn_cmd} co ${GITHUB_URL}/Mk/Uses ;
				# Fetch x11/plasma5-libkscreen
				${svn_cmd} co ${GITHUB_URL}/x11/plasma5-libkscreen ;
				rm -Rf plasma5-libkscreen/.svn/ ;

				if [ ! -d "${LOCAL_REPO}/Mk/Uses" ]; then
						mkdir -p ${LOCAL_REPO}/Mk/Uses ;
				fi
				cp Uses/kde.mk ${LOCAL_REPO}/Mk/Uses ;
				
				if [ ! -d "${LOCAL_REPO}/x11" ]; then
						mkdir ${LOCAL_REPO}/x11 ;
				fi
				cp -R plasma5-libkscreen/ ${LOCAL_REPO}/x11 ;

				cd .. ; rm -Rf plasma5 ;

				cd ${CWD}
		fi

		exit 0
}

opts=$(getopt hfr ${*})
if [ ${?} -ne 0 ]; then
		usage;
fi
set -- ${opts}

if [ ${#} -eq 1 ]; then
		usage;
else
		while true; do
				case "${1}" in
						-h)
								usage;
								shift
								;;
						-f)
								fetch;
								shift
								;;
						-r)
								usage;
								shift
								;;
						--)
								shift ; break
								;;
				esac
		done
fi
