/**
 * File monitoring
 *
 * valac --pkg=glib-2.0 --pkg=gio-2.0
 **/

public class FileMonitoring : Object {
	private GLib.File file;
	private GLib.FileMonitor monitor;

	/* Constructor method */
	public FileMonitoring (string filename) {
		string path;
		string resource;

		path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S,
									 GLib.Environment.get_home_dir ());
		resource = GLib.Path.build_filename (path, filename);

		/* Create GLib.File object */
		file = GLib.File.new_for_path (resource);
	}

	public void monitor_connect () {
		monitor = file.monitor_file (GLib.FileMonitorFlags.NONE,
									 null);

		monitor.changed.connect ((src, dest, event) => {
				if (dest != null) {
					stdout.printf ("%s: %s, %s\n", event.to_string (),
								   src.get_path (),
								   dest.get_path ());
				}
				else {
					stdout.printf ("%s: %s\n", event.to_string (),
								   src.get_path ());
				}
			});
	}

	public static int main (string[] args) {
		FileMonitoring file_mo;
		GLib.MainLoop loop;

		string filename = "test.txt";

		loop = new GLib.MainLoop ();

		file_mo = new FileMonitoring (filename);
		file_mo.monitor_connect ();

		loop.run ();

		return 0;
	}
}
