#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2019, Olivier Duchateau <duchateau.olivier@gmail.com>
#
# Permission to use, copy, modify, and/or distribute this software
# for any purpose with or without fee is hereby granted, provided
# that the above copyright notice and this permission notice appear
# in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
# FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
# ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
# THIS SOFTWARE.
#

'''Download a video from a PeerTube instance. By default it is
peertube.video, but it is easy to use another host.

If a video is not available with the default resolution (720),
you can also change this parameter.

Use 'debug' option in order to see different hosts and resolutions.

Complete list of instances, https://instances.joinpeertube.org/instances
'''

import argparse
import io
import json
import pathlib
import sys

try:
    import certifi
    import urllib3
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


class PeerTube(object):
    '''Meta-class which uses PeerTube API.'''

    def __init__(self, host=None, resolution=None):
        self.result = {}

        if host is None:
            host = 'peertube.video'

        if resolution is None:
            self.resolution = 720
        else:
            self.resolution = resolution

        self.conn = urllib3.HTTPSConnectionPool(host,
                                                cert_reqs='CERT_REQUIRED',
                                                ca_certs=certifi.where())

    def peertube_api_request(self, path, fields=None):
        content = None
        headers = {'content-type': 'application/json'}

        res = self.conn.request('GET', path, headers=headers,
                                fields=fields)
        if res.status == 200:
            content = res.data.decode('utf-8')

        return content

    def get_video_uuid(self, iterable, local):
        '''Return identifiant of video.'''
        res = None

        if isinstance(iterable, dict):
            if 'uuid' in iterable:
                res = iterable.get('uuid')

            if 'isLocal' in iterable and iterable.get('isLocal') == local:
                return res
            else:
                return None


    def get_video_host(self, iterable):
        '''Return hoster of video.'''
        res = None

        if isinstance(iterable, dict):
            if 'channel' in iterable:
                res = iterable.get('channel').get('host')

        return res

    def select_video_from_resolution(self, iterable):
        res = None
        flag = False

        if isinstance(iterable, dict):
            if 'resolution' in iterable:
                if self.resolution == iterable.get('resolution').get('id'):
                    flag = True

            if flag and 'fileDownloadUrl' in iterable:
                res = iterable.get('fileDownloadUrl')

        return res

    def peertube_get_videos(self, content, is_local=True):
        video_id = None
        res = []

        if 'data' in content:
            for i in content.get('data'):
                video_id = self.get_video_uuid(i, is_local)
                if video_id is not None:
                    res.append(video_id)
                    video_id = None

        return res

    def peertube_select_video(self, content):
        data = json.loads(content)
        res = None

        if 'files' in data:
            for i in data.get('files'):
                res = self.select_video_from_resolution(i)
                if res is not None:
                    break

        return res

    def peertube_search(self, query, n_items):
        fields = {'search': '{0}'.format(query),
                  'count': n_items}

        content = self.peertube_api_request('/api/v1/search/videos', fields)
        if content is not None:
            data = json.loads(content)
            videos = self.peertube_get_videos(data)
            if videos:
                for i in videos:
                    if isinstance(i, tuple):
                        path = '/api/v1/videos/{0}'.format(i[0])
                    else:
                        path = '/api/v1/videos/{0}'.format(i)
                    data = self.peertube_api_request(path)
                    if data is not None:
                        if isinstance(i, tuple):
                            self.result[i[1]] = self.peertube_select_video(data)
                        else:
                            res = self.peertube_select_video(data)
                            if res is not None:
                                self.result[i] = res
                                break
            else:
                videos = self.peertube_get_videos(data, False)
                if videos:
                    for i in videos:
                        if isinstance(i, tuple):
                            path = '/api/v1/videos/{0}'.format(i[0])
                        else:
                            path = '/api/v1/videos/{0}'.format(i)
                        data = self.peertube_api_request(path)
                        if data is not None:
                            if isinstance(i, tuple):
                                self.result[i[1]] = self.peertube_select_video(data)
                            else:
                                res = self.peertube_select_video(data)
                                if res is not None:
                                    self.result[i] = res
                                    break
                else:
                    print('No video available')
        else:
            print('No matching query')

        self.conn.close() 


class Debug(PeerTube):
    '''Debug class, displays resolution available in each hosts.'''

    def __init__(self, host):
        super().__init__(host)

    def get_video_uuid(self, iterable):
        res = None

        if isinstance(iterable, dict):
            if 'uuid' in iterable:
                res = iterable.get('uuid')

        return res

    def peertube_select_video(self, content):
        data = json.loads(content)
        res = []

        if 'files' in data:
            for i in data.get('files'):
                if 'resolution' in i:
                    res.append(i.get('resolution').get('id'))

        return res

    def peertube_get_videos(self, content, is_local=True):
        res = []

        if 'data' in content:
            for i in content.get('data'):
                video_id = self.get_video_uuid(i)
                video_host = super().get_video_host(i)

                if video_id is not None and video_host is not None:
                    res.append((video_id, video_host))

        return res

    def debug_pretty_print(self, query, max_items):
        data = []
        super().peertube_search(query, max_items)

        if isinstance(self.result, dict):
            for k in self.result.keys():
                data.append({'host': k, 'quality': self.result.get(k)})
            print(json.dumps(data, indent=2))


class Search(PeerTube):
    '''Search video from its title name.'''

    def __init__(self, host, resolution):
        super().__init__(host, resolution)

    def search_video(self, query, max_items):
        super().peertube_search(query, max_items)

class Download(object):
    '''Fetch a stream from URL.'''

    def __init__(self, url):
        fragment = urllib3.util.url.parse_url(url)

        self.path = fragment.path
        if self.path is not None:
            self.conn = urllib3.HTTPSConnectionPool(fragment.host,
                                                    cert_reqs='CERT_REQUIRED',
                                                    ca_certs=certifi.where())
        else:
            self.conn = None

    def _get_stream(self, filename):
        if isinstance(filename, pathlib.PosixPath):
            tmp_filename = filename.with_suffix('.tmp')

            if tmp_filename.exists():
                mode = 'ab'
                size = tmp_filename.stat().st_size
                headers = {'Range': 'bytes={0}-'.format(size)}
            else:
                mode = 'wb'
                headers = None

            res = self.conn.request('GET', self.path, headers=headers,
                                    preload_content=False)
            if res.status == 200 or res.status == 206:
                with io.BufferedWriter(io.FileIO(tmp_filename, mode)) as f:
                    for chunk in res.stream():
                        f.write(chunk)

                res.release_conn()

                tmp_filename.rename(filename)
            else:
                print('Wrong response from server')
                sys.exit(-1)

    def _set_file(self):
        res = None
        if self.path is not None:
            dir_name = pathlib.Path.home()
            base_name = pathlib.Path(self.path).name

            res = dir_name / base_name

        return res

    def dl_get_stream(self):
        filename = self._set_file()
        if isinstance(filename, pathlib.PosixPath):
            if filename.exists():
                print('Video already exists')
                sys.exit(0)
            else:
                self._get_stream(filename)
        else:
            print('Bad class instance')
            sys.exit(-1)


def main(args):
    if args.debug:
        debug = Debug(args.host)
        debug.debug_pretty_print(args.query, args.max)
    else:
        peertube = Search(args.host, args.resolution)
        peertube.search_video(args.query, args.max)

        Download(peertube.result.popitem()[-1]).dl_get_stream()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--max', type=int, default=10,
                        metavar='int',
                        help='maximum number of items (10 by default)')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='enable debug')
    parser.add_argument('-s', '--server', default='peertube.video',
                        metavar='host', dest='host',
                        help='define host')
    parser.add_argument('-r', '--resolution', type=int, default=720,
                        metavar='int',
                        help='''resolution of video (default 720p)
                                see debug option for others values''')
    parser.add_argument('-q', '--query', required=True,
                        metavar='title',
                        help='title of video')
    main(parser.parse_args())
