#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2019, Olivier Duchateau <duchateau.olivier@gmail.com>
#
# Permission to use, copy, modify, and/or distribute this software
# for any purpose with or without fee is hereby granted, provided
# that the above copyright notice and this permission notice appear
# in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
# FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
# ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
# THIS SOFTWARE.
#

'''Get list of available PeerTube instances.

By default it checks healthy instances, you can turn off
this parameter, with -a (or --all) option.

Output format is JSON:

host → PeerTube instance
videos → total of videos follows by this instance
local → real videos hosted on this instance
'''

import argparse
import json
import sys

try:
    import certifi
    import urllib3
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


class PeerTubeInstances(object):
    '''Check PeerTube instances.'''

    def __init__(self, health=True):
        self._result = []
        host = 'instances.joinpeertube.org'
        # Compare between boolean and string value
        if health:
            self.health = 'true'
        else:
            self.health = 'false'

        self.conn = urllib3.HTTPSConnectionPool(host,
                                                cert_reqs='CERT_REQUIRED',
                                                ca_certs=certifi.where())

    def get_n_instances(self, content):
        '''Return total number of PeerTube instances.'''

        data = json.loads(content)

        if 'total' in data:
            return data.get('total')

    def get_infos_per_host(self, iterable):
        host = None
        n_videos = None
        local_nvideos = None

        if isinstance(iterable, dict):
            if 'host' in iterable:
                host = iterable.get('host')

            if 'totalVideos' in iterable:
                n_videos = iterable.get('totalVideos')

            if 'totalLocalVideos' in iterable:
                local_nvideos = iterable.get('totalLocalVideos')

        return {'host': host, 'videos': n_videos, 'local': local_nvideos}

    def get_infos(self, content):
        data = json.loads(content)

        if 'data' in data:
            for i in data.get('data'):
                self._result.append(self.get_infos_per_host(i))

    def api_request(self, path, params):
        '''Return bytes object, otherwise None.'''
        content = None
        headers = {'content-type': 'application/json'}

        res = self.conn.request('GET', path, headers=headers,
                                fields=params)
        if res.status == 200:
            content = res.data

        return content

    def get_instances(self):
        fields = {'start': 0,
                  'count': 0}

        data = self.api_request('/api/v1/instances', fields)
        if data is not None:
            total = self.get_n_instances(data)
            # We update parameters
            fields.update(count=total, healthy=self.health)

            data = self.api_request('/api/v1/instances', fields)
            if data is not None:
                self.get_infos(data)

    @property
    def result(self):
        return self._result


def pretty_print(iterable):
    '''Return JSON output.'''
    if isinstance(iterable, list):
        print(json.dumps(iterable, indent=2))

def main(args):
    peertube = PeerTubeInstances(health=args.health)
    peertube.get_instances()

    pretty_print(peertube.result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--all', action='store_false',
                        default=True, dest='health',
                        help='check all instances')
    main(parser.parse_args())
