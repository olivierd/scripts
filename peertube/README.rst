=========================
PeerTube video downloader
=========================

``peertube.py`` is lightweight Python 3 script in order to download
video from a PeerTube instance (see full list_). It uses its `REST API`_.

By default the program looks for local video, if not available it searches
in another instance.

Default values:

- peertube.video → default host
- 720p           → default resolution
- 10             → number items to search

Requirements
------------

On Linux systems:

- python3-certify
- python3-urllib3

For FreeBSD users:

- py3X-certify
- py3X-urllib3

Where *X* is 6 or higher.

How to use it
-------------

::

   usage: peertube.py [-h] [-m int] [-d] [-s host] [-r int] -q title
   
   optional arguments:
      -h, --help            show this help message and exit
      -m int, --max int     maximum number of items (10 by default)
      -d, --debug           enable debug
      -s host, --server host
                              define host
      -r int, --resolution int
                              resolution of video (default 720p)
                              see debug option for others values
      -q title, --query title
                              title of video

Required options
~~~~~~~~~~~~~~~~

**-q** (or **--query**) specifies title of video

Example:

::

   ./peertube.py -q Sintel

If title contains several words:

::

   ./peertube.py -q "Big Buck Bunny"

Optional options
~~~~~~~~~~~~~~~~

**-d** (or **--debug**) enable debug, it returns JSON output which displays
each video resolutions for hosts. It is helper when script can't find video.

::

   ./peertube.py -d -q Sintel
   [
      {
         "host": "video.tedomum.net",
         "quality": [
            436
         ]
      },
      {
         "host": "video.atlanti.se",
         "quality": [
            436,
            360,
            240
         ]
      },
      ...

**-s** (or **--server**) changes default host.

::

   ./peertube.py -q Sintel -s peertube.social

**-r** (or **--resolution**) changes default resolution of video.

::

   ./peertube -q Sintel -r 480

Of course we can mix options:

::

   ./peertube.py -r 480 -s peertube.social -q Sintel

.. _list: https://instances.joinpeertube.org/instances
.. _REST API: https://docs.joinpeertube.org/api.html
