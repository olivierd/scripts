/**
 * Example how to get data from the org.xfce.Xfconf interface.
 *
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 gio-2.0` -o properties \
 *      properties.c
 */

#include <stdio.h>

#include <glib.h>
#include <gio/gio.h>


static gchar *
get_human_monitor_name (gchar *prop)
{
	GRegex     *regex;
	GMatchInfo *info;
	gchar      *monitor_name;

	/*regex = g_regex_new ("^/.+/last-image$", 0, 0, NULL);*/
	regex = g_regex_new ("/(?P<monit>monitor\\D+-\\d+)/", 0, 0, NULL);
	if (regex != NULL) {
		if (g_regex_match (regex, prop, 0, &info)) {
			monitor_name = g_match_info_fetch_named (info, "monit");
			/*fprintf (stdout, "%s\n", monitor_name);
			g_free (monitor_name);*/
		}
		g_match_info_free (info);
		g_regex_unref (regex);
	}

	return monitor_name;
}

gchar *
retrieve_monitor_name (gint monitor_id)
{
	GDBusProxy   *proxy;
	GError       *err = NULL;
	GVariant     *variant;
	GVariantIter *iter;
	gchar        *key, *monitor_name, *res;

	proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                           G_DBUS_PROXY_FLAGS_NONE,
                                           NULL,
                                           "org.xfce.Xfconf",
                                           "/org/xfce/Xfconf",
                                           "org.xfce.Xfconf",
                                           NULL, &err);
	if (proxy == NULL) {
		fprintf (stderr, "Error: %s\n", err->message);
		g_error_free (err);
	}
	else {
		variant = g_dbus_proxy_call_sync (proxy,
                                          "GetAllProperties",
                                          g_variant_new ("(ss)",
                                                         "xfce4-desktop",
                                                         "/backdrop"),
                                          G_DBUS_CALL_FLAGS_NONE,
                                          -1, NULL, &err);
		if (variant == NULL) {
			fprintf (stderr, "Error: %s\n", err->message);
			g_error_free (err);
		}
		else {
			g_variant_get (variant, "(a{sv})", &iter);
			/* Only keys are interesting */
			while (g_variant_iter_loop (iter, "{&sv}", &key)) {
				monitor_name = get_human_monitor_name (key);
				if (monitor_name) {
					break;
				}
			}
			g_variant_iter_free (iter);
			g_variant_unref (variant);
		}
		g_object_unref (proxy);
	}

	if (monitor_name) {
		res = g_strdup_printf ("%s", monitor_name);
		g_free (monitor_name);
	}
	else {
		res = g_strdup_printf ("monitor%d", monitor_id);
	}

	return res;
}

int main (int args, char *argv[])
{
	gint   monitor_id = 0;
	gchar *result;

	result = retrieve_monitor_name (monitor_id);
	if (result) {
		fprintf (stdout, "%s\n", result);
		g_free (result);
	}

	return 0;
}
