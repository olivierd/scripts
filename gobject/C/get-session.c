/**
 * Get result of 'GetSessionByPID' method from the
 * org.freedesktop.login1.Manager interface (it requires systemd).
 *
 * We use GDBusProxy class.
 *
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 gio-2.0` -o get-session
 *      get-session.c
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>
#include <gio/gio.h>


#define FD_DBUS_NAME "org.freedesktop.login1"
#define FD_DBUS_OBJ_PATH "/org/freedesktop/login1"
#define FD_DBUS_INTERFACE "org.freedesktop.login1.Manager"

int main (int args, char *argv[])
{
	pid_t pid;
	GDBusProxy *proxy;
	GError     *error = NULL;
	GVariant   *variant;
	gchar      *object_path; 

	pid = getpid ();

	proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SYSTEM,
                                           G_DBUS_PROXY_FLAGS_NONE,
                                           NULL,
                                           FD_DBUS_NAME,
                                           FD_DBUS_OBJ_PATH,
                                           FD_DBUS_INTERFACE,
                                           NULL, &error);
	if (proxy == NULL) {
		fprintf (stderr, "Error: %s\n", error->message);
		g_error_free (error);
	}
	else {
		variant = g_dbus_proxy_call_sync (proxy,
                                          "GetSessionByPID",
                                          g_variant_new ("(u)", pid),
                                          G_DBUS_CALL_FLAGS_NONE,
                                          -1, NULL,
                                          &error);
		if (variant == NULL) {
			fprintf (stderr, "Error: %s\n", error->message);
			g_error_free (error);
		}
		else {
			g_variant_get (variant, "(o)", &object_path);

			g_variant_unref (variant);
			fprintf (stdout, "%s\n", object_path);
		}
		g_object_unref (proxy);
	}

	return 0;
}
