/**
 * Get result of 'GetSessionByPID' method from the
 * org.freedesktop.login1.Manager interface (it requires systemd).
 *
 * We use GDBusMessage class.
 *
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 gio-2.0` -o get-session-03
 *      get-session-03.c
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>
#include <gio/gio.h>


#define FD_DBUS_NAME "org.freedesktop.login1"
#define FD_DBUS_OBJ_PATH "/org/freedesktop/login1"
#define FD_DBUS_INTERFACE "org.freedesktop.login1.Manager"

int main (int args, char *argv[])
{
	pid_t            pid;
	GDBusMessage    *msg, *reply;
	GDBusConnection *conn;
	GError          *error = NULL;
	GVariant        *variant;
	gchar           *object_path;

	pid = getpid ();

	msg = g_dbus_message_new_method_call (FD_DBUS_NAME,
                                          FD_DBUS_OBJ_PATH,
                                          FD_DBUS_INTERFACE,
                                          "GetSessionByPID");
	if (msg == NULL) {
		fprintf (stderr, "Could not allocate the DBus message\n");
	}
	else {
		g_dbus_message_set_body (msg, g_variant_new  ("(u)", pid));

		conn = g_bus_get_sync (G_BUS_TYPE_SYSTEM,
                               NULL, &error);
		if (conn == NULL) {
			fprintf (stderr, "Error: %s\n", error->message);
			g_error_free (error);

			g_object_unref (msg);
		}
		else {
			reply = g_dbus_connection_send_message_with_reply_sync (conn,
                                                                    msg,
                                                                    G_DBUS_SEND_MESSAGE_FLAGS_NONE,
                                                                    -1,
                                                                    NULL,
                                                                    NULL,
                                                                    &error);
			if (reply == NULL) {
				fprintf (stderr, "Error: %s\n", error->message);
				g_error_free (error);
			}
			else {
				g_dbus_connection_flush_sync (conn, NULL, &error);
				if (error != NULL) {
					fprintf (stderr,
                             "Unable to flush message queue: %s\n",
                             error->message);
					g_error_free (error);
				}

				g_object_unref (conn);
				g_object_unref (msg);

				variant = g_dbus_message_get_body (reply);

				g_variant_get (variant, "(o)", &object_path);
				g_object_unref (reply);

				fprintf (stdout, "%s\n", object_path);
			}
		}
	}

	return 0;
}
