/**
 * Get result of 'GetSessionByPID' method from the
 * org.freedesktop.login1.Manager interface (it requires systemd).
 *
 * We use GDBusConnection class.
 *
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 gio-2.0` -o get-session-02
 *      get-session-02.c
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>
#include <gio/gio.h>


#define FD_DBUS_NAME "org.freedesktop.login1"
#define FD_DBUS_OBJ_PATH "/org/freedesktop/login1"
#define FD_DBUS_INTERFACE "org.freedesktop.login1.Manager"

int main (int args, char *argv[])
{
	pid_t            pid;
	GDBusConnection *conn;
	GError          *error = NULL;
	GVariant        *variant;
	const gchar     *object_path;

	pid = getpid ();

	conn = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);
	if (conn == NULL) {
		fprintf (stderr, "Error: %s\n", error->message);
		g_error_free (error);
	}
	else {
		variant = g_dbus_connection_call_sync (conn,
                                               FD_DBUS_NAME,
                                               FD_DBUS_OBJ_PATH,
                                               FD_DBUS_INTERFACE,
                                               "GetSessionByPID",
                                               g_variant_new ("(u)", pid),
                                               G_VARIANT_TYPE ("(o)"),
                                               G_DBUS_CALL_FLAGS_NONE,
                                               -1, NULL, &error);
		if (variant == NULL) {
			fprintf (stderr, "Error: %s\n", error->message);
			g_error_free (error);
		}
		else {
			object_path = g_variant_get_string (g_variant_get_child_value (variant,
                                                                           0),
                                                NULL);
			fprintf (stdout, "%s\n", object_path);
			g_variant_unref (variant);
		}
		g_object_unref (conn);
	}

	return 0;
}
