#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Get result of "GetSessionByPID" method from
the org.freedesktop.login1.Manager interface. It requires systemd.

Gio.DBusConnection class is used.
'''

import os

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio

def main():
    pid = os.getpid()

    bus = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
    # Variant is a GLib.Variant object
    variant = bus.call_sync('org.freedesktop.login1',
                            '/org/freedesktop/login1',
                            'org.freedesktop.login1.Manager',
                            'GetSessionByPID',
                            GLib.Variant('(u)', (pid,)),
                            GLib.VariantType.new('(o)'),
                            Gio.DBusCallFlags.NONE,
                            -1, None)

    #(path,) = variant.unpack()
    path = variant.get_child_value(0).get_string()
    print(path)


if __name__ == '__main__':
    main()
