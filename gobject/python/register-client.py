#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Polkit', '1.0')
from gi.repository import GLib, GObject, Gio, Polkit


def get_application_name():
    p = pathlib.Path(__file__)
    return p.name

def get_client_state(dbus_conn, client_path):
    variant = dbus_conn.call_sync('org.xfce.SessionManager',
                                  client_path,
                                  'org.xfce.Session.Client',
                                  'GetState',
                                  None,
                                  GLib.VariantType.new('(u)'),
                                  Gio.DBusCallFlags.NONE,
                                  -1, None)
    print(variant)

def get_clients(dbus_conn):
    variant = dbus_conn.call_sync('org.xfce.SessionManager',
                                  '/org/xfce/SessionManager',
                                  'org.xfce.Session.Manager',
                                  'ListClients',
                                  None,
                                  GLib.VariantType.new('(ao)'),
                                  Gio.DBusCallFlags.NONE,
                                  -1, None)
    if variant is not None:
        (clients,) = variant.unpack()
    return clients

def remove_client(dbus_conn, obj_path):
    dbus_conn.call_sync('org.xfce.SessionManager',
                        '/org/xfce/SessionManager',
                        'org.xfce.Session.Manager',
                        'UnregisterClient',
                        GLib.Variant('(o)', (obj_path,)), 
                        None,
                        Gio.DBusCallFlags.NONE,
                        -1, None)

def register_client(dbus_conn, app_id, subject):
    start_time = '{0}'.format(subject.get_start_time())

    variant = dbus_conn.call_sync('org.xfce.SessionManager',
                                  '/org/xfce/SessionManager',
                                  'org.xfce.Session.Manager',
                                  'RegisterClient',
                                  GLib.Variant('(ss)', (app_id,
                                                        start_time,)),
                                  GLib.VariantType.new('(o)'),
                                  Gio.DBusCallFlags.NONE,
                                  -1, None)
    if variant is not None:
        (path,) = variant.unpack()

        print(get_clients(dbus_conn))

        remove_client(dbus_conn, path)

def main():
    subject = Polkit.UnixProcess.new(os.getpid())

    try:
        conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    except GLib.Error as err:
        print('Error')
        sys.exit(-1)

    #clients = get_clients(conn)
    #if clients:
    #    get_client_state(conn, clients[0])
    register_client(conn, get_application_name(), subject)
    print(get_clients(conn))


if __name__ == '__main__':
    main()
