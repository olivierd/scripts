#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Get number of frames of GIF image.
'''

import argparse
import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GLib, Gio, GdkPixbuf


def absolute_path(path):
    '''Returns an absolute pathlib.Path object.'''
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            return path
        else:
            return path.resolve()
    else:
        if path.startswith('~'):
            return pathlib.Path(path).expanduser()
        else:
            return pathlib.Path(path).resolve()

def cb_loader_area_prepared(loader):
    print('cb_loader_area_prepared')

    animation = loader.get_animation()
    anim_iter = animation.get_iter(None)

    timeout = anim_iter.get_delay_time()
    #if timeout < 0:
    #    pixbuf = anim_iter.get_pixbuf()
    #    print('Size of image: {0}x{1}'.format(pixbuf.get_width(),
    #                                          pixbuf.get_height()))
    print(timeout)

def cb_loader_size_prepared(loader, width, height):
    print('cb_loader_size_prepared')

    # We reduce size of image
    #loader.set_size(width * 0.75, height * 0.75)

def cb_loader_closed(loader):
    print('cb_loader_closed')
    
def cb_async_ready(src_obj, res, user_data):
    main_loop = user_data[0]
    loader = user_data[1]

    # g_bytes is a GLib.Bytes object
    g_bytes = src_obj.read_bytes_finish(res)

    if g_bytes is None:
        print('An error occured')
        main_loop.quit()
    elif g_bytes.get_size() == 0:
        # We have read file completely
        src_obj.close(None)

        if loader.close():
            #pixbuf = loader.get_pixbuf()
            #print('Size of image: {0}x{1}'.format(pixbuf.get_width(),
            #                                      pixbuf.get_height()))
            print('Closed in cb_async_ready')

        main_loop.quit()
    else:
        #loader.write(g_bytes.get_data())
        loader.write_bytes(g_bytes)

        src_obj.read_bytes_async(10240, 0, None,
                                 cb_async_ready,
                                 (main_loop, loader))

def cb_input_stream_ready(src_obj, res, user_data):
    main_loop = user_data[0]
    loader = user_data[1]

    input_stream = src_obj.read_finish(res)

    if input_stream is None:
        print('An error occured')
        main_loop.quit()
    else:
        input_stream.read_bytes_async(10240, 0, None, cb_async_ready,
                                      (main_loop, loader))

def main(args):
    if args.file.exists():
        loop = GLib.MainLoop.new(None, True)

        # Cast is needed because args.file is pathlib.Path object
        g_file = Gio.File.new_for_path('{0}'.format(args.file))

        # We get Gio.FileInfo object
        info = g_file.query_info(Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                 Gio.FileQueryInfoFlags.NONE,
                                 None)
        content_type = info.get_content_type()
        # We are interested only in GIF format
        if 'gif' in content_type:
            loader = GdkPixbuf.PixbufLoader.new_with_mime_type(content_type)

            loader.connect('area_prepared', cb_loader_area_prepared)
            loader.connect('size_prepared', cb_loader_size_prepared)
            loader.connect('closed', cb_loader_closed)

            g_file.read_async(0, None, cb_input_stream_ready,
                              (loop, loader))

            loop.run()
        else:
            print('MIME type not supported')
            sys.exit(0)
    else:
        print('Error: no such file or directory')
        sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        type=absolute_path,
                        help='SVG filename')
    try:
        main(parser.parse_args())
    except KeyboardInterrupt:
        print('Keyboard has been caught.')
