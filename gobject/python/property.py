#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, GObject, Gio


def check_service(service_name):
    res = False

    conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    if conn is not None:
        reply = conn.call_sync('org.freedesktop.DBus',
                               '/org/freedesktop/DBus',
                               'org.freedesktop.DBus',
                               'GetNameOwner',
                               GLib.Variant('(s)', (service_name,)),
                               None,
                               Gio.DBusCallFlags.NO_AUTO_START,
                               -1, None)
        if reply is not None:
            res = True

    return res


class MyProperty(GObject.Object):
    # Property
    prop_zeitgeist = GObject.Property(type=GObject.TYPE_BOOLEAN,
                                      flags=GObject.ParamFlags.READABLE,
                                      default=check_service('org.gnome.zeitgeist.Engine'))

    def __init__(self):
        GObject.Object.__init__(self)


def main():
    p = MyProperty()

    print(p.get_property('prop-zeitgeist'))

    try:
        p.set_property('prop-zeitgeist', False)
        print(p.get_property('prop-zeitgeist'))
    except TypeError:
        pass


if __name__ == '__main__':
    main()
