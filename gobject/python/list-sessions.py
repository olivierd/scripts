#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Get result of "ListSessions" method from the
org.freedesktop.login1.Manager interface (available with systemd).

We use the Python's way.
'''

import gi

gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio


def main():
    proxy = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SYSTEM,
                                           Gio.DBusProxyFlags.NONE,
                                           None,
                                           'org.freedesktop.login1',
                                           '/org/freedesktop/login1',
                                           'org.freedesktop.login1.Manager',
                                           None)

    sessions = proxy.ListSessions()
    print(sessions)


if __name__ == '__main__':
    main()
