#!/usr/bin/env python3

import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, GObject, Gio


def get_vendor(proxy):
    variant = proxy.call_sync('GetInfo', None,
                              Gio.DBusCallFlags.NONE,
                              -1, None)
    if variant.is_of_type(GLib.VariantType('(sss)')):
        name, version, vendor = variant
        return '{0}'.format(vendor)

def get_clients(proxy):
    list_clients = []

    variant = proxy.call_sync('ListClients', None,
                              Gio.DBusCallFlags.NONE,
                              -1, None)
    if variant.is_of_type(GLib.VariantType('(ao)')):
        print(variant.unpack())

def main():
    dbus_proxy = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SESSION,
                                                Gio.DBusProxyFlags.NONE,
                                                None,
                                                'org.xfce.SessionManager',
                                                '/org/xfce/SessionManager',
                                                'org.xfce.Session.Manager',
                                                None)
    if dbus_proxy is not None:
        print(get_vendor(dbus_proxy))

        get_clients(dbus_proxy)


if __name__ == '__main__':
    main()
