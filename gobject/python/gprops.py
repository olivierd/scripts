#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Xfconf', '0')
from gi.repository import GLib, Gio, GObject, Xfconf


class Settings(GObject.Object):

    # 'prop-width' property
    prop_width = GObject.Property(type=GObject.TYPE_INT,
                                  default=350,
                                  flags=GObject.ParamFlags.READWRITE)
    # 'prop-height' property
    prop_height = GObject.Property(type=GObject.TYPE_INT,
                                   default=450,
                                   flags=GObject.ParamFlags.READWRITE)
    # 'prop-toolbar' property
    prop_toolbar = GObject.Property(type=GObject.TYPE_BOOLEAN,
                                    default=True,
                                    flags=GObject.ParamFlags.READWRITE)

    def __init__(self, channel):
        GObject.Object.__init__(self)

        # Facilitate the conversion from GObject property name to
        # Xfconf property name (and vice versa)
        self.properties_dict = {'prop-width': '/application/width',
                                'prop-height': '/application/height',
                                'prop-toolbar': '/show-toolbar'}

        # Initialize Xfconf properties
        # NOTE: We can't use GObject.install_properties() and
        #       others related methods, here.
        # We get gobject.GParamSpec object instead of
        # GObject.ParamSpec.
        # https://gitlab.gnome.org/GNOME/pygobject/issues/227
        if channel is not None:
            self.settings_init(channel)

            # Use the 'notify::' signal in order to bind GObject property
            # to Xfconf property
            self.connect('notify::', self.notify_signal_emitted_cb,
                         channel)

    def settings_init(self, channel):
        '''Initialize Xfconf properties of given channel.'''
        for pspec in self.list_properties():
            prop_name = self.properties_dict.get(pspec.name)
            if not channel.has_property(prop_name):
                val = None

                if pspec.value_type == GObject.TYPE_BOOLEAN:
                    val = GObject.Value(GObject.TYPE_BOOLEAN)
                    val.set_boolean(pspec.default_value)
                elif pspec.value_type == GObject.TYPE_INT:
                    val = GObject.Value(GObject.TYPE_INT)
                    val.set_int(pspec.default_value)

                if val is not None:
                    channel.set_property(prop_name, val)

    def lookup_property_name(self, prop, reverse=False):
        if reverse:
            d = dict(map(reversed, self.properties_dict.items()))
        else:
            d = self.properties_dict

        return d.get(prop)

    def notify_signal_emitted_cb(self, obj, pspec, user_data):
        '''Replace the Xfconf.property_bind() function.'''
        channel = user_data

        if pspec.value_type != GObject.TYPE_INVALID or pspec.value_type != GObject.TYPE_NONE:
            # New value
            value = obj.get_property(pspec.name)
            # Xfconf property
            xfconf_prop = self.lookup_property_name(pspec.name)

            channel.set_property(xfconf_prop, value)


def main():
    res = Xfconf.init()
    if res:
        channel = Xfconf.Channel.new('xfce4-dummy')
    else:
        channel = None

    settings = Settings(channel)

    #print(settings.get_property('prop-width'))
    #print(settings.get_property('prop_width'))
    print(settings.props.prop_toolbar)

    #for pspec in settings.list_properties():
    #    if pspec.value_type == GObject.TYPE_INT:
    #        print(pspec.name)

    settings.props.prop_toolbar = False
    settings.props.prop_width = 480
    print(settings.get_property('prop-toolbar'))


if __name__ == '__main__':
    main()

    #Xfconf.shutdown()
