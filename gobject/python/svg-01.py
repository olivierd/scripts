#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Get GdkPixbuf.Pixbuf from an SVG image (using Rsvg.Handle object).
'''

import argparse
import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Rsvg', '2.0')
from gi.repository import GLib, Gio, Rsvg


def absolute_path(path):
    '''Returns an absolute pathlib.Path object.'''
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            return path
        else:
            return path.resolve()
    else:
        if path.startswith('~'):
            return pathlib.Path(path).expanduser()
        else:
            return pathlib.Path(path).resolve()

def main(args):
    if args.file.exists():
        # Cast is needed because args.file is pathlib.Path object
        g_file = Gio.File.new_for_path('{0}'.format(args.file))

        handle = Rsvg.Handle.new_from_gfile_sync(g_file,
                                                 Rsvg.HandleFlags.FLAGS_NONE,
                                                 None)
        handle.set_dpi_x_y(96.0, 96.0)
        handle.close()

        pixbuf = handle.get_pixbuf()
        if pixbuf is not None:
            print('Size of image: {0}x{1}'.format(pixbuf.get_width(),
                                                  pixbuf.get_height()))
    else:
        print('Error: no such file or directory')
        sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        type=absolute_path,
                        help='SVG filename')

    main(parser.parse_args())
