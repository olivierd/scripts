#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Get result of "GetSessionByPID" method from
the org.freedesktop.login1.Manager interface. It requires systemd.

Gio.DBusProxy class is used.
'''

import os

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio

def main():
    pid = os.getpid()

    proxy = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SYSTEM,
                                           Gio.DBusProxyFlags.NONE,
                                           None,
                                           'org.freedesktop.login1',
                                           '/org/freedesktop/login1',
                                           'org.freedesktop.login1.Manager',
                                           None)
    if proxy is not None:
        # variant is a GLib.Variant object
        variant = proxy.call_sync('GetSessionByPID',
                                  GLib.Variant('(u)', (pid,)),
                                  Gio.DBusCallFlags.NONE,
                                  -1, None)

        #(path,) = variant.unpack()
        path = variant.get_child_value(0).get_string()
        print(path)


if __name__ == '__main__':
    main()
