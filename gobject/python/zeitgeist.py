#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Zeitgeist', '2.0')
from gi.repository import GLib, Gio, Zeitgeist


def zg_async_ready_cb(src, result, user_data):
    main_loop = user_data

    res = src.find_events_finish(result)

    for i in range(res.estimated_matches()):
        if res.has_next():
            event = res.next_value()
            if event.num_subjects() == 1:
                subject = event.get_subject(0)
                print(subject.get_property('uri'))

    main_loop.quit()

def zg_show(main_loop, parent_folder):
    client = Zeitgeist.Log.get_default()

    tmpl = Zeitgeist.Event.new()
    subject = Zeitgeist.Subject.new()
    subject.set_interpretation(Zeitgeist.DOCUMENT)
    subject.set_origin(parent_folder)
    tmpl.add_subject(subject)

    client.find_events(Zeitgeist.TimeRange.to_now(),
                       [tmpl],
                       Zeitgeist.StorageState.ANY,
                       10,
                       Zeitgeist.ResultType.MOST_RECENT_SUBJECTS,
                       None,
                       zg_async_ready_cb, main_loop)

def service_is_running(connection, service_name):
    res = False

    # We don't care about reply type
    reply = connection.call_sync('org.freedesktop.DBus',
                                 '/org/freedesktop/DBus',
                                 'org.freedesktop.DBus',
                                 'GetNameOwner',
                                 GLib.Variant('(s)', (service_name,)),
                                 None,
                                 Gio.DBusCallFlags.NO_AUTO_START,
                                 -1, None)
    if reply is not None:
        res = True

    return res

def folder_2_uri(path):
    gio_file = Gio.File.new_for_path(path)

    return gio_file.get_uri()

def main(args):
    service_name = 'org.gnome.zeitgeist.Engine'

    conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    if conn is None:
        print('Failed to get session bus')
        sys.exit(0)

    if service_is_running(conn, service_name):
        loop = GLib.MainLoop.new(None, True)

        zg_show(loop, args.directory)

        loop.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory',
                        default=GLib.get_home_dir(),
                        type=folder_2_uri,
                        help='folder where applied query')
    try:
        main(parser.parse_args())
    except KeyboardInterrupt:
        print('Keyboard has been caught')
