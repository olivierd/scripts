#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Unlicense

import argparse
import pathlib
import sys

import cairo

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Poppler', '0.18')
from gi.repository import GLib, Gio, Poppler


class PDFSplit(object):
    '''Extract page from PDF file.'''
    def __init__(self, src, nb):
        self.src = src
        self.nb = nb

        self.dst = self.new_filename()

    def new_filename(self):
        if isinstance(self.src, pathlib.Path):
            basename = self.src.name

            token = basename.split('.pdf')
            # The last item is empty string!
            if len(token) == 2:
                name = '{0}-p{1}.pdf'.format(token[0], str(self.nb))
                return '{0}'.format(self.src.with_name(name))

    def extract_page(self, document):
        if isinstance(document, Poppler.Document):
            page = document.get_page(self.nb)

            width, height = page.get_size()

            if self.dst is not None:
                with cairo.PDFSurface(self.dst, width, height) as surface:
                    ctx = cairo.Context(surface)

                    page.render(ctx)
                    ctx.show_page()


def abs_path(path):
    '''Returns an absolute pathlib object.'''
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            return path
        else:
            return path.resolve()
    else:
        if path.startswith('~'):
            return pathlib.Path(path).expanduser()
        else:
            return pathlib.Path(path).resolve()

def is_pdf(file_obj):
    '''Check if Gio.File object is a PDF format.'''
    res = False

    if isinstance(file_obj, Gio.File):
        info = file_obj.query_info(Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                   Gio.FileQueryInfoFlags.NONE,
                                   None)
        content = info.get_content_type()
        if 'pdf' in content:
            res = True

    return res

def main(args):
    if not cairo.HAS_PDF_SURFACE:
        print('cairo was not compiled with PDF support')
        sys.exit(1)

    if args.file.exists():
        # Cast is needed, because 'args.file' is pathlib.Path object
        g_file = Gio.File.new_for_path('{0}'.format(args.file))
        if is_pdf(g_file):
            try:
                doc = Poppler.Document.new_from_gfile(g_file, None,
                                                      None)

                # Total number of pages
                max_pages = doc.get_n_pages()

                if args.last:
                    page_nb = max_pages - 1
                else:
                    page_number = args.page

                if page_number > max_pages:
                    print('Out of bound')
                    sys.exit(1)
                else:
                    p = PDFSplit(args.file, page_number)
                    p.extract_page(doc)
            except GLib.Error as err:
                print(err.message)
    else:
        print('No such file or directory')
        sys.exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        type=abs_path,
                        help='PDF file')
    parser.add_argument('-p', '--page',
                        type=int,
                        default=0,
                        help='page number to extract (first by default)')
    parser.add_argument('-l', '--last',
                        action='store_true',
                        help='the latest page to extract')
    main(parser.parse_args())
