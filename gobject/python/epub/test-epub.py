#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Generate GdkPixbuf.Pixbuf object from cover of EPUB file.'''

import argparse
import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gepub', '0.6')
from gi.repository import GLib, GdkPixbuf, Gepub


def absolute_path(path):
    '''Return an absolute pathlib.Path object.'''
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            return path
        else:
            return path.resolve()
    else:
        if path.startswith('~'):
            return pathlib.Path(path).expanduser()
        else:
            return pathlib.Path(path).resolve()

def generate_pixbuf(mime_type, gbytes):
    try:
        loader = GdkPixbuf.PixbufLoader.new_with_mime_type(mime_type)
    except GLib.Error:
        loader = GdkPixbuf.PixbufLoader.new()

    #
    res = loader.write_bytes(gbytes)

    loader.close()
    if res:
        return loader.get_pixbuf()
    else:
        return None

def supported_mime_type(mime_type):
    '''From minimal list of mime types, find it is available.'''
    res = False

    if mime_type in ['image/png', 'image/jpeg', 'image/gif']:
        res = True
    return res

def main(args):
    if args.file.exists():
        try:
            # Cast is needed, because args.file is pathlib.Path object
            doc = Gepub.Doc.new('{0}'.format(args.file))

            cover = doc.get_cover()
            mime_type = doc.get_resource_mime_by_id(cover)
            if supported_mime_type(mime_type):
                g_bytes = doc.get_resource_by_id(cover)
                pixbuf = generate_pixbuf(mime_type, g_bytes)
                if pixbuf is not None:
                    # Scale
                    new_pixbuf = pixbuf.scale_simple(128, 128,
                                                     GdkPixbuf.InterpType.BILINEAR)
                    print(new_pixbuf)
                else:
                    print('No pixbuf defined')
            else:
                print('Mime type {0} not supported'.format(mime_type))
        except GLib.Error as err:
            print('{0} {1}'.format(err.code, err.message))
            sys.exit(0)
    else:
        print('No such file or directory')
        sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        type=absolute_path,
                        help='.epub file')
    main(parser.parse_args())
