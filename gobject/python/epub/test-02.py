#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gepub', '0.6')
from gi.repository import GLib, Gio, Gepub


def absolute_path(path):
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            return path
        else:
            return path.resolve()
    else:
        if path.startswith('~'):
            return pathlib.Path(path).expanduser()
        else:
            return pathlib.Path(path).resolve()

def main(args):
    if args.file.exists():
        g_file = Gio.File.new_for_path('{0}'.format(args.file))

        try:
            doc = Gepub.Doc.new(g_file.get_uri())
            
            cover = doc.get_cover()
            if cover is not None:
                print('Ok')
        except GLib.Error as err:
            # URI object not supported!
            print('{0}'.format(err.code, err.message))
            sys.exit(0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        type=absolute_path,
                        help='.epub file')
    main(parser.parse_args())
