#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Similar to:
  gdbus call --session --dest org.freedesktop.DBus \
    --object-path /org/freedesktop/DBus \
    --method org.freedesktop.DBus.ListNames
'''

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio

def main():
    conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    variant = conn.call_sync('org.freedesktop.DBus',
                             '/org/freedesktop/DBus',
                             'org.freedesktop.DBus',
                             'ListNames',
                             None,
                             GLib.VariantType('(as)'),
                             Gio.DBusCallFlags.NONE, -1, None)

    (services, ) = variant.unpack()

    for i in services:
        if 'Mutter' in i:
            print(i)


if __name__ == '__main__':
    main()

