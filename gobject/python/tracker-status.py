#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Check the GNOME Tracker status. Because it can be installed, but
daemon not working.
'''

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio


def main():
    conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(conn, Gio.DBusProxyFlags.NONE,
                                   None, 'org.freedesktop.Tracker1',
                                   '/org/freedesktop/Tracker1/Status',
                                   'org.freedesktop.Tracker1.Status',
                                   None)
    if proxy is not None:
        # Idle or Progress
        #variant = proxy.GetProgress()
        variant = proxy.call_sync('GetStatus', None,
                                  Gio.DBusCallFlags.NONE,
                                  -1, None)
        # It is GLib.Variant (in fact, it is a tuple)
        res = variant.get_child_value(0).get_string()
        if res == 'Idle':
            print('Daemon is running')
        else:
            print('Daemon is not running')


if __name__ == '__main__':
    main()
