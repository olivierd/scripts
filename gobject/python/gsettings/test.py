#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Example how to Gio.Settings with PyGObject and GLib schema.

Before to run this script, you need to run glib-compile-schemas

- dconf dump /org/framagit/olivierd/ → show key
- dconf reset -f /org/framagit/olivierd/ → remove this schema
'''

import argparse
import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio

sss = Gio.SettingsSchemaSource


class MySettings(object):

    def __init__(self, schema_id, root_dir):
        # Check if this schema is available for everybody?
        system = sss.get_default()
        if system.lookup(schema_id, False) is None:
            o = self._load_local_schema(schema_id, root_dir)
            if o is not None:
                self.gsettings = Gio.Settings.new_full(o, None, None)
        else:
            self.gsettings = Gio.Settings.new(schema_id)

    def _load_local_schema(self, schema_id, root_dir):
        res = None

        resource = sss.new_from_directory(root_dir,
                                          sss.get_default(),
                                          False)
        # We get Gio.SettingsSchema object
        res = resource.lookup(schema_id, False)

        return res

def set_double(settings, name, new_value):
    '''Change value when there is range.'''
    # It is Gio.SettingsSchema object
    ss = settings.props.settings_schema
    if ss.has_key(name):
        # It is Gio.SettingsSchemaKey object
        schema_key = ss.get_key(name)

        #variant = schema_key.get_range()
        # We get a tuple (XML node, (val1, valN,))
        #print(variant.unpack()[-1])

        # 'new_value' must be GLib.Variant object!
        if schema_key.range_check(new_value):
            settings.set_value(name, new_value)

        print(settings.get_double(name))

def absolute_path(path):
    if path.startswith('~'):
        p = pathlib.Path(path).expanduser()
    else:
        p = pathlib.Path(path).resolve()

    return p

def main(args):
    schema_id = 'org.framagit.olivierd.test'

    if isinstance(args.directory, pathlib.Path) and args.directory.is_dir():
        settings = MySettings(schema_id,
                              '{0}'.format(args.directory)).gsettings
        #print(settings.keys())
        if settings.get_string('last-folder') != '':
            print(settings.get_string('last-folder'))
        else:
            #settings.set_string('last-folder',
            #                     '/usr/share/backgrounds')
            settings.set_value('last-folder',
                               GLib.Variant.new_string('/usr/share/backgrounds'))
            print(settings.get_string('last-folder'))

        print(settings.get_double('duration'))
        set_double(settings, 'duration', GLib.Variant.new_double(30.0))
    else:
        print('Error: no such file or directory')
        sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', required=True,
                        help='Directory schema sources',
                        type=absolute_path)
    main(parser.parse_args())
