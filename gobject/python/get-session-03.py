#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Get result of "GetSessionByPID" method from
the org.freedesktop.login1.Manager interface. It requires systemd.

Gio.DBusMessage class is used.
'''

import os

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio

def main():
    pid = os.getpid()

    bus = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)

    msg = Gio.DBusMessage.new_method_call('org.freedesktop.login1',
                                          '/org/freedesktop/login1',
                                          'org.freedesktop.login1.Manager',
                                          'GetSessionByPID')
    msg.set_body(GLib.Variant('(u)', (pid,)))

    reply = bus.send_message_with_reply_sync(msg,
                                             Gio.DBusSendMessageFlags.NONE,
                                             -1, None)
    variant = reply[0].get_body()
    print(variant.get_child_value(0).get_string())


if __name__ == '__main__':
    main()
