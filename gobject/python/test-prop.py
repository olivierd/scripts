#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import GLib, GObject


class TestSettings(GObject.Object):

    # Properties
    prop_width = GObject.Property(type=GObject.TYPE_INT,
                                  default=350,
                                  flags=GObject.ParamFlags.READWRITE)
    prop_height = GObject.Property(type=GObject.TYPE_INT,
                                   default=500,
                                   flags=GObject.ParamFlags.READWRITE)

    def __init__(self):
        GObject.Object.__init__(self)

        self.name = self.find_property('prop-width').name

def main():
    t = TestSettings()

    # Retrieve default value of each property
    print(t.props.prop_width)
    print(t.get_property('prop-height'))

    # Using GObject.ParamSpec subclass (here GObject.ParamSpecInt)
    pspec = t.find_property('prop-width')
    print(pspec.name)
    # Work too → prop-height
    #print(t.find_property('prop_height').name)

    # We have access to only a few fields
    print(pspec.default_value)

    # Change value of height property
    height = t.get_property('prop-height')
    t.set_property('prop-height', height + 20)
    # → 520
    print(t.get_property('prop-height'))

    print(t.name)


if __name__ == '__main__':
    main()
