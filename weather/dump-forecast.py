#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Unlicense
# LicenseComments: See https://unlicense.org/UNLICENSE for more details.
#

'''
Dump 'output' of locationforecast from met.no service (provides by the
Norwegian Meteorological Institue).

It is mainly for debug purpose.

This script requires PyGObject (a Python bindings for GObject based
libraries such GLib, GObject and so on).
'''

import argparse
import pathlib

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Soup', '2.4')
from gi.repository import GLib, Soup

# Reuse some methods
from coordinates import ostreetmap, geonames, general_request

def get_current_working_directory():
    cur_dir = pathlib.Path(__file__).resolve().parent

    return cur_dir

def requested_uri(data, api_version, use_json):
    '''Build dynamically the requested URL.'''
    host = 'api.met.no'
    path = 'weatherapi/locationforecast/{0}'.format(api_version)
    # Query
    if api_version == '1.9':
        query = '?lat={0}&lon={1}&msl={2}'.format(data.get('lat'),
                                                  data.get('lon'),
                                                  data.get('msl'))
    elif api_version == '2.0':
        if use_json:
            query = '.json?lat={0}&lon={1}&altitude={2}'.format(data.get('lat'),
                                                                data.get('lon'),
                                                                data.get('msl'))
        else:
            query = '.xml?lat={0}&lon={1}&altitude={2}'.format(data.get('lat'),
                                                               data.get('lon'),
                                                               data.get('msl'))

    url = 'https://{0}/{1}/{2}'.format(host, path, query)

    return url

def dump_forecast(location, data, api_version, use_json):
    '''Snapshot of 'locationforecast' product.'''
    # It is pathlib.Path object
    pwd = get_current_working_directory()

    # Construct 'filename' object (it is still pathlib.Path object)
    version = api_version.replace('.', '')
    n = pwd.joinpath('{0}-forecast-{1}'.format(location, version))
    if use_json and api_version == '2.0':
        filename = n.with_suffix('.json')
    else:
        filename = n.with_suffix('.xml')

    # Requested URL
    url = requested_uri(data, api_version, use_json)
    session = Soup.Session.new()

    content = general_request(session, url)
    if content is not None:
        filename.write_text(content, encoding='UTF-8')
    else:
        print(url)

def main(args):
    location = args.location.title()

    coordinates = ostreetmap(location)
    if coordinates is not None:
        geonames(coordinates)

        dump_forecast(location, coordinates,
                      args.api_version, args.json)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--api', choices=['1.9', '2.0'],
                        default='1.9',
                        dest='api_version',
                        help='which API to use')
    parser.add_argument('--json', action='store_true',
                        help='dump JSON format')
    parser.add_argument('--location', required=True,
                        help='name to search')

    main(parser.parse_args())
