#!/usr/bin/env python3
# -* coding: utf-8 -*-

# SPDX-License-Identifier: Unlicense
# LicenseComments: See https://unlicense.org/UNLICENSE for more details.
#

'''
Get latitude, longitude and altitude coordinates.

It requires PyGObject (a Python bindings for GObject based libraries
such GLib, Gio and so on).
'''

import argparse
import xml.etree.ElementTree as ET

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Soup', '2.4')
from gi.repository import GLib, Soup

def general_request(session, url):
    content = None

    msg = Soup.Message.new('GET', url)
    # Synchronous method (it is blocking!)
    session.send_message(msg)
    if msg.status_code == 200:
        body = msg.response_body
        if body.length > 0:
            content = body.data
    else:
        print('{0}: {1}'.format(msg.status_code, msg.reason_phrase))

    return content

def get_information(source):
    store = False
    data = {}
    tree = ET.XML(source)

    for node in tree.iter(tag='place'):
        #print(node.attrib)
        if 'type' in node.attrib and node.attrib.get('type') == 'city':
            store = True
        if store and 'lat' in node.attrib:
            data['lat'] = node.attrib.get('lat')
        if store and 'lon' in node.attrib:
            data['lon'] = node.attrib.get('lon')
        if store and 'class' in node.attrib:
            store = False
            break

    return data

def ostreetmap(location):
    host = 'nominatim.openstreetmap.org'
    query = 'search?q={0}&format=xml'.format(location)

    user_agent = 'Forecast client - 0.0.1'
    result = None

    url = 'https://{0}/{1}'.format(host, query)
    session = Soup.Session.new()
    # We must define the User-Agent (otherwise 403 status code)
    session.props.user_agent = user_agent

    content = general_request(session, url)
    if content is not None:
        result = get_information(content)

    return result

def geonames(data):
    '''Return value of meters above sea level (msl).'''
    latitude = data.get('lat')
    longitude = data.get('lon')

    host = 'secure.geonames.org'
    query = 'srtm3?lat={0}&lng={1}'.format(latitude, longitude)
    query_full = '{0}&username=xfce4weatherplugin'.format(query)

    result = None

    url = 'https://{0}/{1}'.format(host, query_full)
    session = Soup.Session.new()

    content = general_request(session, url)
    if content is not None:
        # It is plain text content-type
        data.update(msl=content.replace('\r\n', ''))

    return data

def main(args):
    info = ostreetmap(args.location.title())
    if info is not None:
        print(geonames(info))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--location', required=True,
                        help='name to search GPS coordinates')

    main(parser.parse_args())
