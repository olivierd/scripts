/**
 * Parse the new output weather API (2.0).
 *
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 libxml-2.0` -o weatherapi \
 *      weatherapi.c
 */

#define _XOPEN_SOURCE
#include <stdio.h>
#include <time.h>

#include <glib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>


static gboolean node_is_type (xmlNode *node, gchar *val)
{
	gint res;

	res = xmlStrEqual (node->name, (const xmlChar *) val);

	if (res == 1)
		return TRUE;
	else
		return FALSE;
}

static gchar *get_attrib (xmlNode *node, gchar *attr)
{
	gchar *res;

	res = (gchar *) xmlGetProp (node, (const xmlChar *) attr);

	return res;
}

static gchar *remove_timezone_offset (gchar *date)
{
	GRegex *re = NULL;
	const gchar *pattern = "[+-][0-9]{2}:[0-9]{2}";
	gchar *res;

	re = g_regex_new (pattern, 0, 0, NULL);
	if (re != NULL && g_regex_match (re, date, 0, NULL)) {
		res = g_regex_replace(re, date, -1, 0, "", 0, NULL);
	}
	else {
		res = date;
	}

	return res;
}

gchar *format_date (time_t time, gchar *format)
{
	struct tm *tm;
	gchar buf[40];
	size_t size;

	tm = gmtime (&time);
	if (G_UNLIKELY (tm == NULL) || tm->tm_year <= 70) {
		return g_strdup ("-");
	}

	size = strftime (buf, 40, format, tm);
	return (size ? g_strdup (buf) : g_strdup ("-"));
}

static gboolean is_night_time (time_t date_t)
{
	time_t now_t;
	gboolean res = FALSE;;

	time (&now_t);

	if (difftime (date_t, now_t) > 0) {
		res = TRUE;
	}

	return res;
}

time_t parse_timestring (gchar *date, gchar *format)
{
	struct tm tm;
	time_t t;

	if (G_UNLIKELY (date == NULL)) {
		memset (&t, 0, sizeof (time_t));
		return t;
	}

	memset (&tm, 0, sizeof (struct tm));
	/* use timezone */
	tm.tm_isdst = -1;

	if (strptime (date, format, &tm) != NULL) {
		t = mktime (&tm);
		if (t < 0) {
			memset (&t, 0, sizeof (time_t));
			return t;
		}
		else {
			return t;
		}
	}
	else {
		memset (&t, 0, sizeof (time_t));
		return t;
	}
}

void parse_astro_time (xmlNode *cur_node)
{
	gchar *date, *sunrise, *sunset;
	xmlNode *iter;
	time_t sunrise_t, sunset_t;
	gboolean sun_rises = FALSE, sun_sets = FALSE;

	date = get_attrib (cur_node, "date");
	fprintf (stdout, "%s\n", date);
	xmlFree (date);

	for (iter = cur_node->children; iter; iter = iter->next) {
		if (iter->type == XML_ELEMENT_NODE) {
			if (node_is_type (iter, "sunrise")) {
				sunrise = remove_timezone_offset (get_attrib (iter,
                                                              "time"));
				sunrise_t = parse_timestring (sunrise,
                                              "%Y-%m-%dT%H:%M:%S");
				xmlFree (sunrise);
				sun_rises = TRUE;
			}

			if (node_is_type (iter, "sunset")) {
				sunset = remove_timezone_offset (get_attrib (iter,
                                                             "time"));
				sunset_t = parse_timestring (sunset,
                                             "%Y-%m-%dT%H:%M:%S");
				xmlFree (sunset);
				sun_sets = TRUE;
			}
		}
	}

	if (sun_rises) {
		if (!is_night_time (sunrise_t)) {
			fprintf (stdout, "%s\n",
                     format_date (sunrise_t, "%H:%M:%S"));
		}
	}

	if (sun_sets) {
		fprintf (stdout, "Sun sets: %s\n",
                 format_date (sunset_t, "%H:%M:%S"));
	}
}

gboolean parse_astrodata (xmlNode *cur_node)
{
	xmlNode *iter;

	if (!node_is_type (cur_node, "location")) {
		return FALSE;
	}

	for (iter = cur_node->children; iter; iter = iter->next) {
		if (iter->type == XML_ELEMENT_NODE) {
			if (node_is_type (iter, "time")) {
				parse_astro_time (iter);
			}
		}
	}

	return TRUE;
}

int main (int args, char *argv[])
{
	const gchar *filename = "sunrise-2.0.xml";
	xmlDoc *doc = NULL;
	xmlNode *root_node = NULL;
	xmlNode *iter;

	doc = xmlReadFile (filename, NULL, 0);
	if (doc != NULL) {
		root_node = xmlDocGetRootElement (doc);
		if (root_node != NULL) {
			/*fprintf (stdout, "%s\n", root_node->name);*/
			for (iter = root_node->children; iter; iter = iter->next) {
				if (iter->type == XML_ELEMENT_NODE) {
					/*fprintf (stdout, "%s\n", iter->name);*/
					if (parse_astrodata (iter))
						fprintf (stdout, "Success\n");
				}
			}
		}
	}

	xmlFreeDoc (doc);

	xmlCleanupParser();

	return 0;
}
