/**
* Use the new weather API from met.no
*
* valac --pkg glib-2.0 --pkg libsoup-2.4 --pkg libxml-2.0 weather.vala
*/

public class Location : GLib.Object {

	private Soup.Session session;
	private Soup.Message msg;

	/* Properties */
	public float lat { get; private set; }
	public float lon { get; private set; }

	/* Constructor */
	public Location () {
		session = new Soup.Session ();
		session.user_agent = "Mozilla/5.0 (X11; Fedora; Linux x86_64) Firefox/65.0";
	}

	private Soup.MessageBody? osm_request (string city) {
		Soup.URI uri;
		Soup.MessageBody res = null;

		uri = new Soup.URI ("https://nominatim.openstreetmap.org");
		uri.set_path ("/search");
		uri.set_query_from_fields ("q", "%s".printf (city),
                                   "format", "xml",
                                   null);

		msg = new Soup.Message.from_uri ("GET", uri);
		session.send_message (msg);
		if (msg.status_code == 200) {
			res = msg.response_body;
		}

		return res;
	}

	void parser_node (Xml.Node* node) {
		string type;

		type = node->get_prop ("type");
		if (type == "administrative") {
			lat = float.parse (node->get_prop ("lat"));
			lon = float.parse (node->get_prop ("lon"));
		}
	}

	private void osm_parser (Xml.Doc* tree) {
		Xml.Node* root;

		root = tree->get_root_element ();
		if (root != null) {
			for (Xml.Node* iter = root->children; iter != null; iter = iter->next) {
				if (iter->type == Xml.ElementType.ELEMENT_NODE) {
					if (iter->name == "place") {
						parser_node (iter);
					}
					else {
						stderr.printf ("Unexpected element\n");
					}
				}
			}
		}

		delete tree;
	}


	public static int main (string[] args) {
		Location loc;
		Soup.MessageBody res;
		string content;
		Xml.Doc* doc;

		loc = new Location ();
		res = loc.osm_request ("Monteux");
		if (res != null) {
			content = (string) res.flatten ().data;
			/* Force UTF8 */
			if (content.validate (-1, null)) {
				doc = Xml.Parser.read_memory (content, content.length);
				loc.osm_parser (doc);

				stdout.printf ("lat: %f - long: %f\n", loc.lat, loc.lon);
			}
		}

		return 0;
	}
}
