/*
* valac --pkg=glib-2.0 --pkg=libsoup-2.4 forecast.vala
*
*/

public class Forecast : GLib.Object
{
    private string url;
    private Soup.Session session;
    private Soup.Message msg;

    public Forecast (string resource) {
        url = resource;

        session = new Soup.Session ();
    }

    private void dump_request () {
        msg = new Soup.Message ("GET", url);

        // Signal
        msg.got_headers.connect (() => {
            msg.response_headers.foreach ((name, val) => {
                stdout.printf ("%s = %s\n", name, val);
            });
        });

        session.send_message (msg);
    }

    public static int main (string[] args) {
        // Values of Paris
        string latitude = "48.85661";
        string longitude = "2.35149";
        string altitude = "39";
        string api_version = "1.9";
        string url = null;
        Forecast forecast;

        if (api_version == "1.9") {
            url = "https://api.met.no/weatherapi/locationforecast/%s/?lat=%s&lon=%s&msl=%s".printf (api_version, latitude, longitude, altitude);
        }
        else if (api_version == "2.0") {
            url = "https://api.met.no/weatherapi/locationforecast/%s/.xml?lat=%s&lon=%s&altitude=%s".printf (api_version, latitude, longitude, altitude);
        }

        forecast = new Forecast (url);
        forecast.dump_request ();

        return 0;
    }
}
