/**
* Use the new weather API from met.no/
*
* valac --pkg glib-2.0 --pkg libsoup-2.4 --pkg libxml-2.0 --pkg posix \
*         api-weather.vala
*/

public class MetDotNo : GLib.Object {

	private Soup.Session session;
	private Soup.Message msg;

	private string latitude = "44.034928";
	private string longitude = "4.996873";
	private string day;
	private string offset;

	public MetDotNo (string date, string timezone_offset) {
		session = new Soup.Session ();

		day = "%s".printf (date);
		offset = "%s".printf (timezone_offset);
	}


	public Soup.MessageBody? met_request () {
		Soup.MessageBody res = null;
		string url = null;

		/* Build URL */
		url = "https://api.met.no/weatherapi/sunrise/2.0/?lat=%s&lon=%s&date=%s&offset=%s&days=5".printf (latitude, longitude, day, offset);

		msg = new Soup.Message ("GET", url);
		session.send_message (msg);
		if (msg.status_code == 200) {
			res = msg.response_body;
		}

		return res;
	}

	private void met_parse_time (Xml.Node* cur_node) {
		Xml.Node* iter;

		for (iter = cur_node->children; iter != null; iter = iter->next) {
			if (iter->type == Xml.ElementType.ELEMENT_NODE) {
				//stdout.printf ("%s\n", iter->name);
				if (Posix.strcmp (iter->name, "moonrise") == 0) {
					stdout.printf ("%s\n", iter->get_prop ("time"));
				}
			}
		}
	}

	public void met_parse_astro (Xml.Node* node) {
		Xml.Node* iter;

		for (iter = node->children; iter != null; iter = iter->next) {
			if (iter->type == Xml.ElementType.ELEMENT_NODE) {
				if (iter->name == "time") {
					met_parse_time (iter);
				}
			}
		}
	}

	public static int main (string[] args) {
		GLib.DateTime dt;
		Soup.MessageBody res;
		MetDotNo m;
		string content;
		Xml.Doc* doc;
		Xml.Node* root;
		Xml.Node* iter;

		dt = new GLib.DateTime.now_local ();

		m = new MetDotNo (dt.format ("%Y-%m-%d"),
                          dt.format ("%:z"));
		res = m.met_request ();
		if (res != null) {
			content = (string) res.flatten ().data;
			/* Force UTF8 */
			if (content.validate (-1, null)) {
				doc = Xml.Parser.read_memory (content, content.length);
				root = doc->get_root_element ();
				if (root != null) {
					for (iter = root->children; iter != null; iter = iter->next) {
						if (iter->type == Xml.ElementType.ELEMENT_NODE) {
							if (iter->name == "location")
								m.met_parse_astro (iter);
						}
					}
				}
				else {
					stderr.printf ("Can't find root node\n");
				}
				delete doc;
			}
			else {
				stderr.printf ("Content is not valid UTF-8\n");
			}
		}

		return 0;
	}
}
