/**
* Example of GLib.Object and GLib.ObjectClass classes.
*
* valac --pkg glib-2.0 --pkg gobject-2.0 config.vala
*/

public class ExConfig : GLib.Object {

	/* Properties */
	[Description (nick="/window/width", blurb="")]
	public int width { get; set; default = 600; }
	[Description (nick="/window/height", blurb="")]
	public int height { get; set; default = 480; }
	[Description (nick="/create-subdir", blurb="")]
	public bool subdir { get; set; default = true; }

	private GLib.Type type;
	private GLib.ObjectClass klass;

	public ExConfig () {
		type = typeof (ExConfig);
		klass = (ObjectClass) type.class_ref ();

		/* Signal */
		this.notify.connect (ex_conf_prop_changed_cb);
	}

	public void ex_conf_get_int_property (string property_name) {
		GLib.Value val;

		val = GLib.Value (typeof (int));
		this.get_property (property_name, ref val);

		stdout.printf ("%d\n", val.get_int ());
	}

	public void ex_conf_get_nick (string property_name) {
		GLib.ParamSpec pspec = null;

		pspec = klass.find_property (property_name);
		if (pspec != null) {
			stdout.printf ("%s\n", pspec.get_nick ());
		}
	}

	public void ex_conf_set_int_property (string property_name, int new_val) {
		GLib.ParamSpec pspec = null;
		GLib.Value val;

		val = GLib.Value (GLib.Type.INT);

		pspec = klass.find_property (property_name);
		if (pspec != null) {
			if (pspec.get_default_value ().get_int() != new_val) {
				val.set_int (new_val);
				this.set_property (property_name, val);
			}
		}
	}

	private void ex_conf_prop_changed_cb (GLib.ParamSpec pspec) {
		unowned string nick;

		nick = pspec.get_nick ();
		stdout.printf ("%s\n", nick);
	}

	public static int main (string[] args) {
		ExConfig conf = new ExConfig ();

		stdout.printf ("%d\n", conf.width);
		conf.ex_conf_get_int_property ("height");
		conf.ex_conf_get_nick ("subdir");

		conf.ex_conf_set_int_property ("width", 500);
		stdout.printf ("%d\n", conf.width);

		return 0;
	}
}
