/**
 * Display time and date of file.
 *
 * valac --pkg=glib-2.0 --pkg=gio-2.0
 *
 **/


public class ModificationTime : Object {
	private GLib.File file;
	private GLib.FileInfo info;
	
	/* Constructor */
	public ModificationTime (string filename) {
		string abs_path;
		string resource;

		abs_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S,
										 GLib.Environment.get_home_dir ());
		resource = GLib.Path.build_filename (abs_path, filename);

		file = GLib.File.new_for_path (resource);
	}

	public void display () {
		GLib.TimeVal res;
		
		try {
			info = file.query_info (GLib.FileAttribute.TIME_MODIFIED,
									0);
			res = info.get_modification_time ();
			stdout.printf ("%s\n", res.to_iso8601 ());
		} catch (GLib.Error e) {
			stderr.printf ("Error: %s\n", e.message);
		}	
	}

	public static int main (string[] args) {
		ModificationTime mod;
		
		string filename = "test.txt";

		mod = new ModificationTime (filename);
		mod.display ();
		
		return 0;
	}
}
