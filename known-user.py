#!/usr/bin/env python3

"""Determine owner of archive."""

import argparse
import os
import pwd
import stat

try:
    from gi.repository import GLib
except ImportError:
    pass


def main(arg):
    # Normalize top directory
    filename = os.path.basename(arg)
    path = os.path.dirname(arg)

    if path.startswith("~"):
        top_dir = os.path.expanduser(path)
    else:
        top_dir = os.path.abspath(path)

    archive = os.path.join(top_dir, filename)

    # Get name of current user
    current_user = GLib.get_user_name()
    
    if os.path.exists(archive):
        fp = open(archive, "rb")
        # File descriptor (required by os.fstat function)
        fd = fp.fileno()
        
        status = os.fstat(fd)
        user_name = pwd.getpwuid(status[stat.ST_UID])
        fp.close()

        if current_user == user_name[0]:
            print("Ok")
        else:
            print("Error, owner of archive is not %s" % current_user)
    else:
        print("Error, archive not found")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("archive", nargs=1, help="Path to archive")
    args = parser.parse_args()
    # One option, join() is useful here
    main("".join(args.archive))
