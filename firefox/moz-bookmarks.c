/**
 * Firefox's bookmarks.
 *
 * cc -Wall -lc -o moz-bookmarks moz-bookmarks.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h> /* opendir() */
#include <sys/queue.h>


struct profile
{
	int id;
	char *name;
	TAILQ_ENTRY(profile) next;
};

TAILQ_HEAD(pf_list_head, profile) pf_list; 

/*
 * Need to be freed
 */
char *
moz_settings_path(void)
{
	char *root = getenv("HOME");
	char *path;
	size_t len;

	len = snprintf(NULL, 0, "%s/tmp", root);
	path = malloc(len + 1);
	if (path == NULL)
	{
		perror("malloc");
		free(path);

		return NULL;
	}

	sprintf(path, "%s/tmp", root);

	return path;
}

void
moz_first_profiles(struct profile *pf)
{
	pf = TAILQ_FIRST(&pf_list);

	printf("%d %s\n", pf->id, pf->name);
}

void
moz_lookup_profiles(char *path)
{
	DIR *stream;
	struct dirent *entry = NULL;
	struct profile *pf;
	int i = 1;

	stream = opendir(path);
	if (stream == NULL)
		perror("opendir");

	while ((entry = readdir(stream)) != NULL)
	{
		pf = malloc(sizeof(struct profile));
		if (pf == NULL)
		{
			perror("malloc");
			free(pf);

			break;
		}
		
		if (strcmp(entry->d_name, ".") == 0)
			continue;
		else if (strcmp(entry->d_name, "..") == 0)
			continue;

		if (entry->d_type == DT_DIR)
		{	
			pf->id = i;
			pf->name = entry->d_name;

			TAILQ_INSERT_TAIL(&pf_list, pf, next);

			i++;
		}

		free(pf);
	}

	closedir(stream);

	/*
	TAILQ_FOREACH(pf, &pf_list, next)
	{
		printf("%d %s\n", pf->id, pf->name);
	}
	*/
}

/*
struct profile *
moz_profiles_init(void)
{
	struct profile *pf;

	pf = malloc(sizeof(struct profile));
	if (pf == NULL)
	{
		perror("malloc");
		free(pf);

		return NULL;
	}

	TAILQ_INIT(&pf_list);

	return pf;
}
*/

int
main(int argc, char *argv[])
{
	char *path = NULL;
	struct profile *pf;

	TAILQ_INIT(&pf_list);
	
	path = moz_settings_path();
	if (path != NULL)
	{
		pf = malloc(sizeof(struct profile));
		if (pf == NULL)
		{
			perror("malloc");
		}
		
		//printf("%s\n", path);
		moz_lookup_profiles(path);

		pf = TAILQ_FIRST(&pf_list);
		printf("%d %s\n", pf->id, pf->name);

		free(pf);
	}
	
	free(path);
	
	return 0;
}
