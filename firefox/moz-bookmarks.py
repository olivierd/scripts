#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os
import sys

try:
    import sqlite3
except ImportError:
    print('Missing sqlite3 module')
    sys.exit(0)


def bookmarks(path):
    db = 'places.sqlite'

    sql = '''SELECT moz_places.url FROM moz_bookmarks 
    INNER JOIN moz_places ON moz_bookmarks.fk = moz_places.id
    WHERE moz_bookmarks.type = 1 AND
    moz_bookmarks.title IS NOT NULL
    ORDER BY moz_bookmarks.dateAdded'''
    
    if os.path.exists(os.path.join(path, db)):
        profile = os.path.basename(path)
        f = open(os.path.join(os.path.expanduser('~'), profile), 'w')
        
        conn = sqlite3.connect(os.path.join(path, db))
        cur = conn.cursor()

        
        for row in cur.execute(sql):
            f.write(row[0])
            f.write(os.linesep)
            #print(row[0])
        
        conn.close()
        f.close()
    
    
def lookup_moz_profiles(path):
    profiles = []
    dirs = os.listdir(path)

    # Loop over profiles
    for d in dirs:
        p = os.path.join(path, d)
        if os.path.isdir(p) and p.endswith('.default'):
            profiles.append(p)

    return profiles


def main():
    path = os.path.expanduser(os.path.join('~/.mozilla', 'firefox'))

    if os.path.isdir(path):
        list_pro = lookup_moz_profiles(path)
        if list_pro:
            for pro in list_pro:
                bookmarks(pro)


if __name__ == '__main__':
    main()
