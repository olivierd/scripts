/**
 * Get memory usage.
 *
 * cc -Wall -I/usr/include -lc -o mem-usage mem-usage.c
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <unistd.h> /* getpagesize() */


long
mem_get_by_bytes(const char *name)
{
	long buf;
	size_t len = sizeof(long);

	if (sysctlbyname(name, &buf, &len, NULL, 0) < 0) {
		return 0;
	}
	else {
		return buf;
	}
}

long
mem_get_by_pages(const char *name)
{
	long res = 0;

	res = mem_get_by_bytes(name);
	if (res > 0) {
		res = res * getpagesize ();
	}
	
	return res;
}

int
main(int argc, char *argv[])
{
	long mem_total, mem_free, mem_cache, mem_buf, mem_page;
	
	mem_total = mem_get_by_bytes("hw.physmem");
	mem_free = mem_get_by_pages("vm.stats.vm.v_free_count");
	mem_buf = mem_get_by_bytes("vfs.bufspace");
	mem_cache = mem_get_by_pages("vm.stats.vm.v_inactive_count");
	mem_page = mem_get_by_pages("vm.stats.vm.v_page_count");

	long user = mem_total - mem_free - mem_cache - mem_buf;
	
	/* in Xfce4-taskmanager */
	long used = mem_page - mem_free - mem_cache - mem_buf;

	
	float mem_percent1 = (user * 100) /  (double) mem_total;
	float mem_percent2 = (used * 100) / (double) mem_page;

	printf("%.0f%%\n", mem_percent1);
	printf("%.0f%%\n", mem_percent2);

	return 0;
}
