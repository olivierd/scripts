/**
* valac --pkg=glib-2.0 --pkg=gio-2.0 --pkg=accountsservice \
*		--vapidir=./vapi plank-config.vala
*
*/

static void
create_subdirectories (string path_name) {
    GLib.File file;

    file = GLib.File.new_for_path (path_name);
    if (!file.query_exists ()) {
        try {
            file.make_directory_with_parents ();
        } catch (GLib.Error err) {
            stderr.printf ("%s\n", err.message);
        }
    }
}

static string
set_path (string root_dir, string base_name) {
    string full_name;

    full_name = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S,
                                      root_dir, base_name);

    return full_name;
}

static GLib.List<GLib.File>?
list_directory (GLib.File file) {
    GLib.FileEnumerator file_enum;
    GLib.FileInfo info;
    GLib.FileType file_type;
    GLib.List<GLib.File> list_files;

    list_files = new GLib.List<GLib.File> ();

    try {
        file_enum = file.enumerate_children (GLib.FileAttribute.STANDARD_NAME,
                                             GLib.FileQueryInfoFlags.NOFOLLOW_SYMLINKS);

        while ((info = file_enum.next_file (null)) != null) {
            file_type = info.get_file_type ();
            if (file_type == GLib.FileType.REGULAR) {
                //stdout.printf ("%s\n", info.get_name ());
                string path;

                path = set_path (file.get_path (), info.get_name ());
                list_files.append (GLib.File.new_for_path (path));
            }
        }
    } catch (GLib.Error err) {
        stderr.printf ("%s\n", err.message);
    }

    return list_files;
}


static void
set_plank_user_config_dir () {
    unowned string user_config;
    string plank_config;
    GLib.File dst;
    GLib.File src;
    GLib.List<GLib.File>? files;

    user_config = GLib.Environment.get_user_config_dir ();

    // Plank's user config directory
    plank_config = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S,
                                         user_config, "plank",
                                         "dock1", "launchers");
    create_subdirectories (plank_config);

    src = GLib.File.new_for_path (set_path (GLib.Environment.get_home_dir (),
                                            "tmp/"));

    files = list_directory (src);
    foreach (GLib.File file in files) {
        try {
            string name;

            name = file.get_basename ();

            dst = GLib.File.new_for_path (set_path (plank_config,
                                          name));
            file.copy (dst, GLib.FileCopyFlags.TARGET_DEFAULT_PERMS);
        } catch (GLib.Error err) {
            stderr.printf ("%s\n", err.message);
        }
    }
}

static int
main(string[] argv) {
	Act.UserManager manager;
	GLib.SList<unowned Act.User> users;
    // Get name of the current user
    unowned string current_user = GLib.Environment.get_user_name ();

	manager = Act.UserManager.get_default ();
	users = manager.list_users ();

    /* Use the Meson build system*/
    //stdout.printf ("%s\n", Config.INSTALL_PREFIX);

    foreach (unowned Act.User user in users) {
        //stdout.printf ("%s\n", user.get_user_name ());
        if (user.get_user_name () == current_user) {
            set_plank_user_config_dir();
            break;
        }
    }

	return 0;
}
