This simple script (Vala) copy some files into `~/.config/plank/` subdirectories.

You need to create temporary directory and add some files.

    mkdir ~/tmp
    touch ~/tmp/file-01.txt
    touch ~/tmp/file-023.json

If you use the Meson build system:

1. Install `meson` and `ninja`

2. Run following commands

    meson builddir
    cd builddir
    ninja

Otherwise use the `valac` compiler:

    valac --pkg=glib-2.0 --pkg=gio-2.0 --pkg=accountsservice \
        --vapidir=./vapi plank-config.vala
