#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pathlib

import gi
gi.require_version('GLib', '2.0')
gi.require_version('AccountsService', '1.0')
from gi.repository import GLib, AccountsService


def set_plank_user_config_dir(user_obj):
    user_config = GLib.get_user_config_dir()
    if user_config is None or user_config == '':
        home_dir = user_obj.get_home_dir()
        p = pathlib.Path(home_dir).resolve()
        # We need to cast!
        user_config = '{0}'.format(p.joinpath('.config'))

    path = pathlib.Path(user_config)
    plank_dir = path.joinpath('plank', 'dock1',
                              'launchers')
    if not plank_dir.exists():
        plank_dir.mkdir(parents=True)
    print('{0}'.format(plank_dir))

def main():
    manager = AccountsService.UserManager.get_default()
    for u in iter(manager.list_users()):
        # Check only current user (not the whole)
        if u.get_user_name() == GLib.get_user_name():
            set_plank_user_config_dir(u)
            break

if __name__ == '__main__':
    main()
