/**
 * Display CPU usage.
 *
 * cc -Wall -I/usr/include -lc -o cpu cpu-usage.c
 */

#include <stdio.h>
#include <stdlib.h> /* malloc(), free(), ... */
#include <sys/resource.h> /* CPUSTATES, ... */
#include <sys/types.h>
#include <sys/sysctl.h>


void
cpu_update(int nb_cpu)
{
	long used, total;
	size_t cp_size = sizeof(long) * CPUSTATES * nb_cpu;
	long *cp_times = malloc(cp_size);

	if (sysctlbyname("kern.cp_times", cp_times, &cp_size, NULL, 0) < 0) {
		perror("sysctlbyname");
		free(cp_times);
	}

	used = cp_times[CP_USER] + cp_times[CP_NICE];
	total = used + cp_times[CP_SYS] + cp_times[CP_INTR] + cp_times[CP_IDLE];

	float cpu_percent = (used * 100) / (double) total;

	printf("%.0f%%\n", cpu_percent);

	free(cp_times);
}

int
detect_cpus(void)
{
	static int mib[] = {CTL_HW, HW_NCPU};
	int buf;
	size_t length_buf = sizeof(int);

	if (sysctl(mib, 2, &buf, &length_buf, NULL, 0) < 0) {
		return 0;
	}
	else {
		return buf;
	}
}

int
main(int argc, char *argv[])
{
	int cpu_nb;

	cpu_nb = detect_cpus();
	if (cpu_nb > 0) {
		cpu_update(cpu_nb);
	}

	return 0;
}
