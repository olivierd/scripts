#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import platform
import random
import re
import sys

try:
    import gi

    gi.require_version('GLib', '2.0')
    gi.require_version('Soup', '2.4')

    from gi.repository import GLib, Soup
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


class ArteTv(object):
    def __init__(self, lang, when=None, resource=None):
        self.lang = lang
        self.res = {}

        quality_enum = {1: ['HTTPS_EQ_1', 'HTTPS_MP4_EQ_1'],
                        2: ['HTTPS_HQ_1', 'HTTPS_MP4_HQ_1'],
                        3: ['HTTPS_MQ_1', 'HTTPS_MP4_MQ_1']}
        self.quality = quality_enum[2]

        self.type_enum = ['category', 'guide_list', 'listing', 'content']

        if resource is not None:
            self.canonical_uri = resource
        else:
            if when is not None:
                path = '{0}/guide/{1}/'.format(self.lang, when)
            else:
                path = '{0}/guide/'.format(self.lang)

            self.canonical_uri = 'https://www.arte.tv/{0}'.format(path)

        # Build user agent components
        product = 'Soup/{0}.{1}.{2}'.format(Soup.get_major_version(),
                                            Soup.get_minor_version(),
                                            Soup.get_micro_version())
        system = '(X11; {0} {1})'.format(platform.system(),
                                         platform.machine())

        self.session = Soup.Session.new()
        self.session.props.user_agent = '{0} {1}'.format(product, system)

    def arte_tv_headers(self, name, value, user_data):
        '''Only use for debug.'''
        print('{0} {1}'.format(name, value))

    def arte_tv_convert_seconds(self, duration):
        '''Transforms only seconds to minutes as web page.'''
        if duration is None:
            return 'Undefined'
        elif duration < 60:
            return '{0:02d}s'.format(duration)
        elif duration >= 60:
            q, r = divmod(duration, 60)
            return '{0:02d}min'.format(q)

    def arte_tv_extract_json(self, content):
        json_data = None

        # Pattern to search
        p = re.compile('window.__INITIAL_STATE__ = (?P<data>\{.+?\});')

        res = p.search(content)
        if res is not None:
            json_data = res.group('data')

        return json_data

    def arte_tv_get_video_information(self, seq, prog_id):
        items = self.res.get(prog_id)

        if 'width' in seq:
            items.append(seq.get('width'))
        if 'height' in seq:
            items.append(seq.get('height'))
        if 'bitrate' in seq:
            items.append(seq.get('bitrate'))
        if 'url' in seq:
            items.append(seq.get('url'))

        #    self.res[prog_id] = items
        print(items)

    def arte_tv_deserialize_json(self, sequence):
        flag = False
        items = []

        for seq in sequence:
            if 'programId' in seq:
                value = seq.get('programId')
                if value is not None and not value.startswith('RC-'):
                    flag = True
                    items.append(value)
            if flag and 'url' in seq:
                items.append(seq.get('url'))
            if flag and 'title' in seq:
                items.insert(1, seq.get('title'))
            if flag and 'subtitle' in seq:
                items.insert(2, seq.get('subtitle', None))
            if flag and 'duration' in seq:
                items.insert(3,
                             self.arte_tv_convert_seconds(seq.get('duration')))

                if items[0] not in self.res:
                    self.res[items[0]] = items

                items = []
                flag = False

    def arte_tv_set_uri_api(self, prog_id):
        host = 'https://api.arte.tv'
        path = 'api/player/v1/config/{0}/{1}'.format(self.lang, prog_id)

        return '{0}/{1}'.format(host, path)

    def arte_tv_parse_json(self, content, prog_id=None):
        data = json.loads(content)
        key = None

        if 'videoJsonPlayer' in data:
            # We ensure, there is no empty list
            if data['videoJsonPlayer'].get('VSR'):
                # We made shallow copy (to avoid long line)
                d = data['videoJsonPlayer']['VSR'].copy()

                if self.quality[0] in d:
                    key = self.quality[0]
                elif self.quality[1] in d:
                    key = self.quality[1]

                if key is not None:
                    self.arte_tv_get_video_information(d.get(key), prog_id)
        else:
            # Iterate over keys, because they vary
            for k in iter(data['pages']['list']):
                if '_{0}_'.format(self.lang) in k:
                    key = k
                    break

            d = data['pages']['list'][key].copy()

            for zone in d.get('zones'):
                if zone.get('type') in self.type_enum:
                    self.arte_tv_deserialize_json(zone.get('data'))

    def arte_tv_request(self, prog_id=None):
        if prog_id is None:
            msg = Soup.Message.new('GET', self.canonical_uri)
        else:
            msg = Soup.Message.new('GET', self.arte_tv_set_uri_api(prog_id))
        self.session.send_message(msg)

        if msg.status_code == 200:
            #msg.response_headers.foreach(self.arte_tv_headers,
                                         #None)
            # It's a tuple
            content_type = msg.response_headers.get_content_type()
            if 'html' in content_type[0]:
                data = self.arte_tv_extract_json(msg.response_body.data)
                if data is not None:
                    self.arte_tv_parse_json(data)
                else:
                    print('Can\'t extract JSON data from HTML page')
            elif 'json' in content_type[0]:
                self.arte_tv_parse_json(msg.response_body.data, prog_id)
        else:
            print('Error: {0}'.format(msg.status_code))


def get_iso3166_a2():
    '''Get the country code, following the ISO 3166 alpha-2 code.'''

    iso_code = None

    list_env = GLib.get_environ()
    locale = GLib.environ_getenv(list_env, 'LC_CTYPE')
    if locale is not None:
        tokens = locale.split('_')
        if len(tokens) == 2:
            iso_code = tokens[0]
    else:
        locale = GLib.environ_getenv(list_env, 'LANG')
        if locale is not None:
            tokens = locale.split('_')
            if len(tokens) == 2:
                iso_code = tokens[0]

    # We define at least a value
    if iso_code is None:
        iso_code = 'fr'

    return iso_code

def get_random_elem(sequence):
    keys = list(sequence.keys())

    if len(keys) == 1:
        return keys[0]
    elif len(keys) > 1:
        return random.choice(keys)

def main():
    lang = get_iso3166_a2()
    #url='https://www.arte.tv/fr/videos/082163-000-A/vicky-cristina-barcelona/'
    #url='https://www.arte.tv/fr/guide/'
    url='https://www.arte.tv/fr/videos/080095-000-A/etrange-tasmanie/'

    a = ArteTv(lang, when=None, resource=url)
    a.arte_tv_request()

    a.arte_tv_request(get_random_elem(a.res))


if __name__ == '__main__':
    main()
