#!/bin/sh

# Get the default profile
profile=$(gsettings get org.gnome.Terminal.ProfilesList default | sed "s/'//g")

# Schema (the whole path!)
schema=org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profile}/

# List keys values recursively
#gsettings list-recursively ${schema}

# Font, default Monospace 12
gsettings set ${schema} font "Monospace 11"

# Scrollbar policy, default always
gsettings set ${schema} scrollbar-policy never

# Light theme, default dark
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'light'
