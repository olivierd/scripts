#!/bin/sh

# Display full date and time
gsettings set org.gnome.desktop.interface clock-show-date true

# Display icons on desktop
gsettings set org.gnome.desktop.background show-desktop-icons true

# Resize icons
gsettings set org.gnome.nautilus.icon-view default-zoom-level 'standard'

# Disable external search (GNOME shell)
gsettings set org.gnome.desktop.search-providers disable-external true

# Disable blink cursor (in terminal application)
gsettings set org.gnome.desktop.interface cursor-blink false

# Disable 'lock screen'
#gsettings set org.gnome.desktop.lockdown disable-lock-screen true

# Disable lock for screensaver
gsettings set org.gnome.desktop.screensaver lock-enabled false

# Disable all providers (for online accounts)
gsettings set org.gnome.online-accounts whitelisted-providers []

# Buttons layout
gsettings set org.gnome.desktop.wm.preferences button-layout ':minimize,maximize,close'

# gnome-terminal
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'system'
