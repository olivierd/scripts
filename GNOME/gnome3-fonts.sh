#!/bin/sh

# default: Cantarell 11
gsettings set org.gnome.desktop.interface font-name 'Cantarell 10'
# default: Sans 11
gsettings set org.gnome.desktop.interface document-font-name 'Sans 10'
# default: Monospace 11
gsettings set org.gnome.desktop.interface monospace-font-name 'Monospace 10'
# default: Cantarell Bold 11
gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Cantarell Bold 10'
