#!/bin/sh

LEGACY_VERSION="2.14.2" # current version on server
UPDATE_VERSION="2.14.3"

###
CWD=$(pwd)
HOST="http://download.dotclear.org"


check_binary()
{
	if [ -x $(which ${1} 2>/dev/null) ]; then
		echo $(which ${1} 2>/dev/null)
	fi
}

fetch()
{
	URL="${HOST}/attic/dotclear-${2}.tar.gz"
	status=$(${1} -s -o /dev/null -I -w "%{http_code}" ${URL})
	if [ $status -eq 200 ]; then
		${1} -O ${URL}
	else
		URL="${HOST}/latest/dotclear-${2}.tar.gz"
		status=$(${1} -s -o /dev/null -I -w "%{http_code}" ${URL})
		if [ $status -eq 200 ]; then
			${1} -O ${URL}
		fi
	fi
		
}

fetch_tarball()
{
	if [ -n "${LEGACY_VERSION}" ]; then
		fetch ${1} ${LEGACY_VERSION}
	fi
}

extract_tarball()
{
	if [ -n "${LEGACY_VERSION}" ]; then
		if [ -e "dotclear-${LEGACY_VERSION}.tar.gz" ]; then
			tar --atime-preserve=system -xzf \
			dotclear-${LEGACY_VERSION}.tar.gz

			mv dotclear dotclear-${LEGACY_VERSION}
		fi
	fi
}

fetch_patch()
{
	if [ -n "${LEGACY_VERSION}" ] && [ -n "${UPDATE_VERSION}" ]; then
		URL="${HOST}/patches/${2}.gz"

		status=$(${1} -s -o /dev/null -I -w "%{http_code}" ${URL})
		if [ $status -eq 200 ]; then
			${1} -O ${URL}

			gunzip ${2}.gz
		fi
	fi
}

apply_patch()
{
	if [ -n "${LEGACY_VERSION}" ]; then
		touch ~/tmp/deleted.txt ; touch ~/tmp/updated.txt
				
		cd dotclear-${LEGACY_VERSION}

		${1} -b -p1 < ../${2}

		#find . -name '*.orig' > ~/tmp/files.txt
		#find . -name '*.orig' | sed -e 's|.orig|| ; s|./||' > ~/tmp/files.txt
		for f in $(find . -name '*.orig'); do
			if [ -e ${f%.orig} ]; then
				echo ${f%.orig} | sed -e 's|./||' >> ~/tmp/updated.txt
			else
				echo ${f%.orig} | sed -e 's|./||' >> ~/tmp/deleted.txt
			fi
		done

		cd ..
	fi
}

main()
{
	# Check if 'curl' and 'patch' are installed
	curl_=$(check_binary "curl")
	patch_=$(check_binary "patch")

	if [ -n "${curl_}" ] && [ -n "${patch_}" ]; then
		if [ ! -d ~/tmp ]; then
			mkdir ~/tmp
		fi

		cd ~/tmp

		fetch_tarball ${curl_}

		extract_tarball

		PATCH="${LEGACY_VERSION}-${UPDATE_VERSION}.diff"

		fetch_patch ${curl_} ${PATCH}

		apply_patch ${patch_} ${PATCH}

		cd ${CWD}
	fi
}

main
