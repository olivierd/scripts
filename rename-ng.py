#!/usr/bin/env python3

"""Add zero after '_'."""

import argparse
import os

def get_ncontent(rootdir, is_file=False):
    """Return list of directories or files from
    root directory."""

    list = []

    if os.path.exists(rootdir):
        for e in os.scandir(rootdir):
            if is_file and e.is_file():
                list.append(e.path)
            elif not is_file and e.is_dir(follow_symlinks=False):
                list.append(e.path)

    return list

def add_nzeros(nb, limit):
    """Add zero as prefix."""

    if limit <= 100:
        return '{0:03d}'.format(int(nb))
    elif limit > 100 and limit <= 1000:
        return '{0:04d}'.format(int(nb))

def rename_(seq):
    length = len(seq)

    for s in seq:
        root = os.path.dirname(s)
        base = os.path.basename(s)

        tokens = base.split('_')
        if len(tokens) == 2:
            tt = tokens[1].split('.')
            n = '.'.join([add_nzeros(tt[0], length), tt[-1]])

            name = '_'.join([tokens[0], n])

            if name is not None:
                os.rename(s, os.path.join(root, name))

def main(opt):
    # Normalize path
    if opt.startswith('~'):
        root = os.path.expanduser(opt)
    else:
        root = os.path.abspath(opt)

    dirs = get_ncontent(root)
    if dirs:
        for d in dirs:
            rename_(get_ncontent(d, is_file=True))
    else:
        rename_((get_ncontent(root, is_file=True)))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', nargs=1, help='Root directory')

    args = parser.parse_args()
    main(''.join(args.directory))
