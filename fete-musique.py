#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import io
import pathlib
import platform
import time

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Soup', '2.4')
from gi.repository import GLib, Soup

class MusicParty(object):

    def __init__(self):

        host = 'data.culture.gouv.fr'
        self.uri = self.set_soup_uri(host)

        # User agent components
        product = 'Soup/{0}.{1}.{2}'.format(Soup.get_major_version(),
                                            Soup.get_minor_version(),
                                            Soup.get_micro_version())
        system = '(X11; {0} {1})'.format(platform.system(),
                                         platform.machine())

        self.session = Soup.Session.new()
        self.session.props.user_agent = '{0} {1}'.format(product,
                                                         system)

    def set_soup_uri(self, host):
        '''Return Soup.URI object.'''
        year = time.gmtime(time.time()).tm_year

        uri = Soup.URI.new('https://{0}'.format(host))
        # Path
        path = '/explore/dataset/fete-de-la-musique-{0}/download'.format(year)
        uri.set_path(path)
        # Query
        uri.set_query('timezone=Europe/Berlin&format=json')

        return uri

    def perform_query(self):
        p = pathlib.Path.home()

        msg = Soup.Message.new_from_uri('GET', self.uri)
        msg.request_headers.append('Content-Type', 'application/json')
        self.session.send_message(msg)

        if msg.status_code == 200:
            res, t, param = msg.response_headers.get_content_disposition()
            if res:
                filename = param.get('filename')

                with io.BufferedWriter(io.FileIO(p / filename, 'w')) as fd:
                    fd.write(bytes(msg.response_body.data, 'utf-8'))


def main():
    mp = MusicParty()
    mp.perform_query()


if __name__ == '__main__':
    main()


