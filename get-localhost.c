/**
* Display the standard host name.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
    size_t name_max = sysconf (_SC_HOST_NAME_MAX);
    char localhost[name_max + 1];

    if (gethostname (localhost, sizeof localhost) == 0) {
        fprintf (stdout, "%s\n", localhost);
    }

    return 0;
}
