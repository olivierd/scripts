#!/bin/sh
# $Id$
# Create list of packages to build with poudriere.
#

get_hostname()
{
	echo $(hostname -s)
}

get_available_pkg()
{
	for p in $($(which pkg) info | $(which awk) '{printf "%s\n", $1}'); do
		echo $($(which pkg) query %o ${p}) >> ${1};
	done
}

main()
{
	filename="${HOME}/$(get_hostname).txt"

	if [ ! -e ${filename} ]; then
		$($(which touch) ${filename});
	else
		$($(which cat) /dev/null > ${filename});
	fi

	get_available_pkg ${filename}
}

main
