/**
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 gio-2.0 \
 * libwebp libwebpdemux cairo gdk-3.0` \
 * -o anim-webp anim-webp.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>

#include <webp/decode.h>
#include <webp/demux.h>
#include <cairo.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>


static gboolean
is_webp (GFile *file)
{
    GFileInfo   *info;
    GError      *error = NULL;
    const gchar *mime_type;
    gboolean     res = FALSE;

    g_return_val_if_fail (G_IS_FILE (file), FALSE);

    info = g_file_query_info (file,
                              G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                              G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                              NULL, &error);
    if (info == NULL) {
        g_fprintf (stderr, "%s\n", error->message);
        g_error_free (error);
    }
    else {
        mime_type = g_file_info_get_content_type (info);
        if (g_strcmp0 (mime_type, "image/webp") == 0) {
            res = TRUE;
        }
        g_object_unref (info);
    }
    return res;
}

/*
static void
convert_webp (const uint8_t        *data, size_t data_size,
              WebPBitstreamFeatures *features)
{
    gint width, height;

    if (WebPGetFeatures (data, data_size, features) != VP8_STATUS_OK) {
        g_fprintf (stderr, "Unable to retrieve the bitstream features\n");
    }
    else {
        width = features->width;
        height = features->height;

        g_fprintf (stdout, "%dx%d\n", width, height);
    }
}*/

static GdkPixbuf *
convert_webp_v2 (const uint8_t *data, size_t data_size,
                 WebPDecoderConfig *config)
{
    gint width, height;
    gint stride;
    cairo_surface_t *surface;
    GdkPixbuf *pixbuf = NULL;

    if (WebPGetFeatures (data, data_size, &config->input)
        != VP8_STATUS_OK) {
        g_fprintf (stderr, "Unable to retrieve the bitstream features\n");
    }
    else {
        width = config->input.width;
        height = config->input.height;

        /*g_fprintf (stdout, "%dx%d\n", width, height);*/
        surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
                                              width, height);
        cairo_surface_flush (surface);

        config->options.no_fancy_upsampling = 1;

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
        config->output.colorspace = MODE_BGRA;
#elif G_BYTE_ORDER == G_BIG_ENDIAN
        config->output.colorspace = MODE_ARGB;
#endif
        config->output.u.RGBA.rgba = (uint8_t *) cairo_image_surface_get_data (surface);
        stride = cairo_image_surface_get_stride (surface);
        config->output.u.RGBA.stride = stride;
        config->output.u.RGBA.size = stride * height;
        config->output.is_external_memory = 1;

        if (WebPDecode (data, data_size, config) == VP8_STATUS_OK) {
            cairo_surface_mark_dirty (surface);
            if (cairo_surface_status (surface) == CAIRO_STATUS_SUCCESS) {
                pixbuf = gdk_pixbuf_get_from_surface (surface, 0, 0,
                                                      width, height);
            }
        }
        else {
            g_fprintf (stderr, "Can't decode image\n");
        }
        WebPFreeDecBuffer (&config->output);
        cairo_surface_destroy (surface);
    }
    return pixbuf;
}

static void
save_to_png (GFile *file, GdkPixbuf *pixbuf)
{
    gchar *output;
    GError *error = NULL;

    output = g_strconcat (g_file_get_path (file), ".png", NULL);

    if (! gdk_pixbuf_save (pixbuf, output,
                           "png", &error,
                           "compression", "3", NULL)) {
        g_fprintf (stderr, "%s\n", error->message);
        g_error_free (error);
    }
    g_free (output);
}

static void
load_image (GFile *file)
{
    WebPDecoderConfig config;
    WebPData          data;
    WebPDemuxer      *demux;
    WebPIterator      iter;
    uint32_t          flags;
    GError           *error = NULL;
    uint8_t          *content;
    gsize             length;
    GdkPixbuf        *pixbuf;

    if (! WebPInitDecoderConfig (&config)) {
        g_fprintf (stderr, "Can't initialize WebPDecoder\n");
    }
    else {
        if (! g_file_load_contents (file, NULL,
                                    (gchar **)&content,
                                    &length, NULL,
                                    &error)) {
            g_fprintf (stderr, "%s\n", error->message);
            g_error_free (error);
        }
        else {
            data.bytes = content;
            data.size = length;

            demux = WebPDemux (&data);
            flags = WebPDemuxGetI (demux, WEBP_FF_FORMAT_FLAGS);
            if (flags & ANIMATION_FLAG) {
                WebPData frame;
                if (! WebPDemuxGetFrame (demux, 1, &iter)) {
                    g_fprintf (stderr, "Unable to get frame iteration\n");
                }
                else {
                    frame = iter.fragment;

                    pixbuf = convert_webp_v2 (frame.bytes, frame.size,
                                              &config);

                    WebPDemuxReleaseIterator (&iter);

                    if (pixbuf != NULL) {
                        save_to_png (file, pixbuf);

                        g_object_unref (pixbuf);
                    }
                }
            }
            else {
                pixbuf = convert_webp_v2 (data.bytes, data.size,
                                          &config);
                if (pixbuf != NULL) {
                    save_to_png (file, pixbuf);

                    g_object_unref (pixbuf);
                }
            }
            WebPDemuxDelete (demux);

            g_free (content);
        }
    }
}

int
main (int argc, char *argv[])
{
    GFile *file;

    if (argc < 2) {
        g_fprintf (stderr, "Missing input FILE\n");
    }
    else {
        file = g_file_new_for_commandline_arg (argv[1]);
        if (is_webp (file)) {
            load_image (file);
        }

        g_object_unref (file);
    }
    return 0;
}
