/**
 * gcc -Wall `pkgconf --cflags --libs glib-2.0` -o test-date test-date.c
 */

#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <glib.h>


gchar *format_date (time_t time, gchar *format)
{
	struct tm *tm;
	gchar buf[40];
	size_t size;

	tm = gmtime (&time);
	if (G_UNLIKELY (tm == NULL) || tm->tm_year <= 70) {
		return g_strdup ("-");
	}
	size = strftime (buf, 40, format, tm);
	return (size ? g_strdup (buf) : g_strdup ("-"));
}

const gchar *remove_timezone_offset (const gchar *date)
{
	GRegex *re = NULL;
	const gchar *pattern = "[+-][0-9]{2}:[0-9]{2}";
	const gchar *res;

	re = g_regex_new (pattern, 0, 0, NULL);
	if (re != NULL && g_regex_match (re, date, 0, NULL)) {
		res = g_regex_replace(re, date, -1, 0, "", 0, NULL);
	}
	else {
		res = date;
	}

	return res;
}

int main (int args, char *argv[])
{
	const gchar *date = "2019-02-23T07:27:57+01:00";
	struct tm tm;
	time_t t;

	memset (&tm, 0, sizeof (struct tm));
	/* use timezone */
	tm.tm_isdst = -1;

	/*fprintf (stdout, "%s\n", remove_timezone_offset (date));*/
	if (strptime (remove_timezone_offset (date),
                  "%Y-%m-%dT%H:%M:%S",
                  &tm) != NULL) {
		t = mktime(&tm);
		if (t == -1) {
			fprintf (stderr, "Error\n");
		}
		else {
			fprintf (stdout, "%s\n",
                     format_date (t, "%Y-%m-%d %H:%M:%S"));
		}
	}

	return 0;
}
