/**
 * Test some librsvg functions
 *
 * $CC -Wall `pkgconf --cflags --libs gio-2.0 librsvg-2.0 gdk-3.0` -lm \
 * -o test-rsvg test-rsvg.c
 */

#include <stdio.h>
#include <math.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>

#include <cairo.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <librsvg/rsvg.h>


#define THUMBNAIL_WIDTH 128
#define THUMBNAIL_HEIGHT 128


static gboolean
mime_type_supported (GFileInfo *info)
{
    gboolean res = FALSE;
    const gchar *mime_type;

    g_return_val_if_fail (G_IS_FILE_INFO (info), FALSE);

    mime_type = g_file_info_get_content_type (info);
    if (g_strcmp0 (mime_type, "image/svg+xml") == 0 ||
        g_strcmp0 (mime_type, "image/svg+xml-compressed") == 0)
        res = TRUE;

    return res;
}

/*static void
get_dimensions_from_svg_file (RsvgHandle *loaded_svg)
{
    RsvgDimensionData dimensions;

    g_return_if_fail (RSVG_IS_HANDLE (loaded_svg));

    rsvg_handle_get_dimensions (loaded_svg, &dimensions);
    g_fprintf (stdout, "%dx%d\n", dimensions.width,
               dimensions.height);
}*/

static RsvgDimensionData
get_dimensions_from_svg_file (RsvgHandle *loaded_svg)
{
    RsvgDimensionData dimensions;

    rsvg_handle_get_dimensions (loaded_svg, &dimensions);

    return dimensions;
}

static GdkPixbuf *
scale_pixbuf (GdkPixbuf *source,
              gint       source_width,
              gint       source_height)
{

    gdouble w_ratio, h_ratio;
    gint    dest_width = THUMBNAIL_WIDTH;
    gint    dest_height = THUMBNAIL_HEIGHT;

    /* return the same pixbuf if no scaling is required */
    if (source_width <= dest_width && source_height <= dest_height)
        return source;

    /* determine which axis needs to be scaled down more */
    w_ratio = (gdouble) source_width / (gdouble) dest_width;
    h_ratio = (gdouble) source_height / (gdouble) dest_height;

    /* adjust the other axis */
    if (h_ratio > w_ratio)
        dest_width = rint (source_width / h_ratio);
    else
        dest_height = rint (source_height / w_ratio);

    return gdk_pixbuf_scale_simple (source,
                                    MAX (dest_width, 1),
                                    MAX (dest_height, 1),
                                    GDK_INTERP_BILINEAR);
}

static GdkPixbuf *
generate_pixbuf (RsvgHandle *loaded_svg,
                 gint        width,
                 gint        height)
{
    cairo_surface_t *surface;
    cairo_t         *cr;
    GdkPixbuf       *pixbuf = NULL;

    g_return_val_if_fail (RSVG_IS_HANDLE (loaded_svg), NULL);

    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
                                          width, height);
    cr = cairo_create (surface);

    cairo_save (cr);
    if (cairo_status (cr) == CAIRO_STATUS_SUCCESS) {
        if (rsvg_handle_render_cairo (loaded_svg, cr)) {
            cairo_restore (cr);

            pixbuf = gdk_pixbuf_get_from_surface (surface,
                                                  0, 0,
                                                  width, height);
        }
        else {
            g_fprintf (stderr, "No render\n");
        }
    }
    else
        g_fprintf (stderr, "Nooo\n");

    cairo_destroy (cr);
    cairo_surface_destroy (surface);

    if (pixbuf != NULL) {
        return scale_pixbuf (pixbuf, width, height);
    }
    else {
        return pixbuf;
    }
}

static void
convert_svg (GFile *file)
{
    GFileInputStream *stream;
    GError           *error = NULL;
    RsvgHandle       *handle = NULL;
    RsvgDimensionData dimensions;
    gint              width, height;
    GdkPixbuf        *pixbuf;

    stream = g_file_read (file, NULL, &error);
    if (stream == NULL) {
        g_fprintf (stderr, "%s\n", error->message);
        g_error_free (error);
    }
    else {
        handle = rsvg_handle_new_from_stream_sync (G_INPUT_STREAM (stream),
                                                   file,
                                                   RSVG_HANDLE_FLAG_KEEP_IMAGE_DATA,
                                                   NULL, &error);
        if (handle == NULL) {
            g_fprintf (stderr, "%s\n", error->message);
            g_error_free (error);
        }
        else {
            rsvg_handle_set_dpi (handle, 90.0);

            dimensions = get_dimensions_from_svg_file (handle);
            if (dimensions.width > 0 || dimensions.height > 0) {
                /* Size of SVG image */
                width = dimensions.width;
                height = dimensions.height;

                pixbuf = generate_pixbuf (handle, width, height);
                if (pixbuf != NULL) {
                    /*g_fprintf (stdout, "%dx%d\n",
                               gdk_pixbuf_get_width (pixbuf),
                               gdk_pixbuf_get_height (pixbuf));*/
                    gchar *new_filename;

                    new_filename = g_strconcat (g_file_get_path (file),
                                                ".png", NULL);

                    if (gdk_pixbuf_save (pixbuf, new_filename,
                                         "png", NULL,
                                         "compression", "3", NULL))
                        g_fprintf (stdout, "Yes\n");

                    g_free (new_filename);
                    g_object_unref (pixbuf);
                }
                else {
                    g_fprintf (stderr, "No pixbuf\n");
                }
            }
        }
        g_object_unref (handle);
    }
    g_clear_object (&stream);
}


int
main (int argc, char *argv[])
{
    GFile            *file;
    GFileInfo        *info;
    GError           *error = NULL;

    if (argc < 2) {
        g_fprintf (stderr, "Missing FILE\n");
    }
    else {
        file = g_file_new_for_path (argv[1]);

        info = g_file_query_info (file,
                                  G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                  G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                  NULL, &error);
        if (info == NULL) {
            g_fprintf (stderr, "%s\n", error->message);
            g_error_free (error);
        }
        else {
            if (mime_type_supported (info)) {
                convert_svg (file);
            }
            else {
                g_fprintf (stderr, "File not supported\n");
            }

            g_object_unref (info);
        }
        g_clear_object (&file);
    }

    return 0;
}
