/**
 * Display CPU information.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/sysctl.h>


int
find_cpu_freq(void)
{
	int buf;
	size_t length_buf = sizeof(buf);

	if (sysctlbyname("dev.cpu.0.freq", &buf, &length_buf, NULL, 0) < 0) {
		return 0;
	}
	else {
		return buf;
	}
}

int
detect_cpu_number(void)
{
	static int mib[] = {CTL_HW, HW_NCPU};
	int buf;
	size_t length_buf = sizeof(int);

	if (sysctl(mib, 2, &buf, &length_buf, NULL, 0) < 0) {
		return 0;
	}
	else {
		return buf;
	}
}

int
main(int argc, char *argv[])
{
	int nb_cpu, freq;

	nb_cpu = detect_cpu_number();
	if (nb_cpu > 0) {
		freq = find_cpu_freq();
		if (freq > 0)
			printf("CPU frequency: %d MHz\n", freq);
	}
	else {
		printf("No CPU found\n");
	}

	return 0;
}
