/**
 * Use AccountsService in order to get avatar of user.
 *
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 gio-2.0` -o icon-file \
 *      icon-file.c
 */

#include <stdio.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>


int main (int args, char *argv[])
{
	GDBusConnection *bus;
	GError          *err = NULL;
	GVariant        *variant, *res;
	const gchar     *user_path;

	bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &err);
	if (bus == NULL) {
		fprintf (stderr, "Failed to get system bus: %s\n", err->message);
		g_error_free (err);
	}
	else {
		variant = g_dbus_connection_call_sync (bus,
                                               "org.freedesktop.Accounts",
                                               "/org/freedesktop/Accounts",
                                               "org.freedesktop.Accounts",
                                               "FindUserByName",
                                               g_variant_new ("(s)",
                                                              g_get_user_name ()),
                                               G_VARIANT_TYPE ("(o)"),
                                               G_DBUS_CALL_FLAGS_NONE,
                                               -1, NULL, &err);
		if (variant == NULL) {
			fprintf (stderr,
                     "Could not contact accounts service to look up: %s\n",
                     err->message);
			g_error_free (err);
			g_object_unref (bus);
		}
		else {
			user_path = g_variant_get_string (g_variant_get_child_value (variant,
                                                                         0),
                                              NULL);

			g_variant_unref (variant);
			variant = g_dbus_connection_call_sync (bus,
                                                   "org.freedesktop.Accounts",
                                                   user_path,
                                                   "org.freedesktop.DBus.Properties",
                                                   "Get",
                                                   g_variant_new ("(ss)",
                                                                  "org.freedesktop.Accounts.User",
                                                                  "IconFile"),
                                                   G_VARIANT_TYPE ("(v)"),
                                                   G_DBUS_CALL_FLAGS_NONE,
                                                   -1, NULL, &err);
			if (variant == NULL) {
				fprintf (stderr, "Error: %s\n", err->message);
				g_error_free (err);
				g_object_unref (bus);
			}
			else {
				g_variant_get_child (variant, 0, "v", &res);
				fprintf (stdout, "%s\n", g_variant_get_string (res, NULL));

				g_variant_unref (res);
				g_variant_unref (variant);
			}
		}
		g_object_unref (bus);
	}

	return 0;
}
