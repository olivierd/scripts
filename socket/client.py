#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: 0BSD
# Olivier Duchateau <duchateau.olivier@gmail.com>
#

'''
Simple script which monitors events from devd daemon. It is
useful with removable media (such USB key, µ SD and so on).

It requires GObject Introspection.
'''

import pathlib
import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, Gio


def build_path(dirname, filename):
	if isinstance(dirname, pathlib.Path):
		return dirname / filename

def check_file_path(dirname, filename):
	res = False

	if isinstance(dirname, pathlib.Path):
		p = build_path(dirname, filename)
		if p.exists():
			res = True

	return res

def read_async_cb(src, result, user_data):
	main_loop = user_data

	res = src.read_bytes_finish(result)
	if res is None:
		print('Error')
		main_loop.quit()
	elif res.get_size() == 0:
		src.close()

		print('End')
		main_loop.quit()
	else:
		# It is bytes object
		print(res.get_data())

		src.read_bytes_async(10240, GLib.PRIORITY_DEFAULT, None,
                             read_async_cb, main_loop)

def connect_async_ready_cb(src, result, user_data):
	main_loop = user_data

	conn = src.connect_finish(result)
	if conn is not None:
		# It is an Gio.InputStream object
		stream = conn.get_input_stream()

		stream.read_bytes_async(10240, GLib.PRIORITY_DEFAULT, None,
                               read_async_cb,
                               main_loop)
	else:
		print('Error')
		main_loop.quit()

def main():
	if sys.platform.startswith('freebsd'):
		p = pathlib.Path('/var/run')

		# Check socket
		if check_file_path(p, 'devd.seqpacket.pipe'):
			socket_type = Gio.SocketType.SEQPACKET

			full_path = build_path(p, 'devd.seqpacket.pipe')
		elif check_file_path(p, 'devd.pipe'):
			socket_type = Gio.SocketType.STREAM

			full_path = build_path(p, 'devd.pipe')
		else:
			print('No such file')
			sys.exit(0)
	else:
		print('Platform not supported')
		sys.exit(0)

	try:
		loop = GLib.MainLoop.new(None, True)

		address = Gio.UnixSocketAddress.new('{0}'.format(full_path))
		client = Gio.SocketClient.new()
		client.set_socket_type(socket_type)

		client.connect_async(address, None,
                             connect_async_ready_cb,
                             loop)

		loop.run()

	except GLib.Error as err:
		print('{0} {1}'.format(err.code, err.message))
		sys.exit(0)


if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print('Keyboard has been caught')
