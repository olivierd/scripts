#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# Olivier Duchateau <duchateau.olivier@gmail.com>
#

'''
Simple script which monitors events from devd daemon. It is
useful with removable media (such USB key, µSD and so on).

No external modules are required.
'''

import pathlib
import socket
import sys


def build_path(path, filename):
	if isinstance(path, pathlib.Path):
		return path / filename

def check_file_path(path, filename):
	res = False

	if isinstance(path, pathlib.Path):
		p = build_path(path, filename)
		if p.exists():
			res = True

	return res

def listen_daemon(s, path):
	if isinstance(path, pathlib.Path):
		s.connect('{0}'.format(path))
	else:
		s.connect(path)

	while True:
		try:
			# It is bytes object
			data = s.recv(1024)
			print(data)
		except KeyboardInterrupt:
			print('Keyboard has been caught')
			break

	s.close()

def main():
	if sys.platform.startswith('freebsd'):
		p = pathlib.Path('/var/run')

		# Check socket file
		if check_file_path(p, 'devd.seqpacket.pipe'):
			socket_type = socket.SOCK_SEQPACKET
			full_path = build_path(p, 'devd.seqpacket.pipe')
		elif check_file_path(p, 'devd.pipe'):
			socket_type = socket.SOCK_STREAM
			full_path = build_path(p, 'devd.pipe')
		else:
			print('No such file')
			sys.exit(0)

		if full_path is not None:
			s = socket.socket(family=socket.AF_UNIX, type=socket_type)

			listen_daemon(s, full_path)

	else:
		print('Platform not supported')
		sys.exit(0)


if __name__ == '__main__':
	main()
