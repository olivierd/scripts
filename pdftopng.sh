#!/bin/sh -eu
#
# SPDX-License-Identifier: Unlicense
#
# Extract first page of PDF, then convert to PNG.
# It requires poppler-utils, ghostscript, and imagemagick.
#

PRGNAME="${0##*/}"

usage()
{
    cat <<EOF >&2
usage: $PRGNAME option

options:
    -h  show this help
    -i  PDF file
EOF
    exit 0
}

transform()
{
    dirname=$(dirname ${1})
    name=$(basename ${1} .pdf)

    # Extract first page of PDF
    pdfseparate -f 1 -l 1 ${1} "${dirname}/_tmp-%d.pdf"

    tmp_pdf=$(find ${dirname} -name '_tmp-*.pdf')

    if [ -e ${tmp_pdf} ]; then
        # Use GhostScript to convert temporary PDF
        # restriction with ImageMagick
        gs -q -r150 -sDEVICE=pngalpha -o "${tmp_pdf}.png" \
            "${tmp_pdf}"

        convert "${tmp_pdf}.png" -resize 50% \
            "${dirname}/${name}.png"

        rm ${tmp_pdf}*
    fi

    exit 0
}

opts=$(getopt hi: ${*})
if [ ${?} -ne 0 ]; then
    usage
fi
set -- ${opts}

# No options were given
if [ ${#} -eq 1 ]; then
    usage ;
else
    while true ; do
        case "${1}" in
        -h)
            usage ;
            shift
            ;;
        -i)
            transform ${2}
            ;;
        --)
            shift ; break
            ;;
        esac
    done
fi
