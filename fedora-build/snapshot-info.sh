#!/bin/sh

# Shell script to populate .spec.in template (we use snapshot).
#

PRGNAME="${0##*/}"
CWD=$(pwd)

### DON'T CHANGE SOMETHING BELOW
MODULES="xfce apps panel-plugins thunar-plugins"
URL="https://git.xfce.org"

usage()
{
		cat <<EOF >&2
usage: ${PRGNAME} [-h] -p PROJECT

options:
 -h		display this help
 -p		name of project
EOF
		exit 0
}

err()
{
		echo "$@" >&2
		exit 1
}

check_vcs()
{
		if [ -x $(which ${1} 2>/dev/null) ]; then
				echo $(which ${1} 2>/dev/null)

				exit 0
		fi
}

get_module()
{
		for i in ${MODULES}; do
				code=$(curl -s -o /dev/null -I -w "%{http_code}" "${URL}/${i}/${1}")
				if [ $code -eq 200 ]; then
						module=${i}
				fi
		done

		echo ${module}
}

get_latest_commit()
{
		commit=$(${1} ls-remote https://git.xfce.org/${2}/${3} \
								 | grep heads/master \
								 | awk '{printf("%s", $1);}')

		echo ${commit}
}

date_from_latest_commit()
{

		date_=$(${1} log --date=format:%Y%m%d --max-count=1 ${2} \
								| grep Date | awk '{printf("%s", $2);}')

		echo ${date_}
}

get_date()
{
		current_date=$(LANG=C date +"%a %b %d %Y")

		echo ${current_date}
}

get_project_metadata()
{
		if [ -n "${2}" ]; then
				cd ~/rpmbuild/SOURCES
				
				${1} clone -q -n git://git.xfce.org/${2}/${3}

				cd ${3}
				
				# The latest commit (40-byte hexadecimal string)
				h=$(${1} rev-parse heads/master)
				
				# The SHA-1 truncate
				s=$(${1} rev-parse --short heads/master)
				
				# Date from the latest commit
				d=$(date_from_latest_commit ${1} ${h})
				
				cd .. ; rm -Rf ${3}
				
				# Get current date (for the changelog entry)
				cur_date=$(get_date)
				
				sed -e "s|##DATE##|$(echo ${d})| ; s|##LONG_HASH##|$(echo ${h})| ;
								 s|##MODULE##|$(echo ${2})| ;
								 s|##CHANGELOG##|$(echo ${cur_date})| ;
					 			 s|##HASH##|$(echo ${s})|" \
										 ~/rpmbuild/SPECS/${3}.spec.in > ~/rpmbuild/SPECS/${3}.spec
		fi
}

main()
{
		if [ -n "${1}" ]; then
				git=$(check_vcs "git")

				# Get module
				module=$(get_module ${1})

				if [ -n "${git}" ]; then
						get_project_metadata ${git} ${module} ${1}
						
						cd ${CWD}
				else
						if [ -z "${git}" ]; then
								err "ERROR: 'git' not found"
						fi
				fi

				exit 0
		else
				err "ERROR: project is not defined!"
		fi
}

while getopts "hp:" opt; do
		case ${opt} in
				h ) usage ;;
				p ) main ${OPTARG} ;;
				\? ) usage ;;
		esac
done

shift $((${OPTIND} -1))
