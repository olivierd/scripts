/**
 * Test the GLib method g_get_system_data_dirs()
 *
 * gcc `pkgconf --cflags --libs glib-2.0` -o system-data system-data.c
 *
 */

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>

int 
main (int argc, char *argv[])
{
	const gchar * const *dirs;
	gchar               *tmp_path, *path = NULL;
	gint                 i;

	dirs = g_get_system_data_dirs ();
	for (i = 0; dirs[i]; i++) {
		tmp_path = g_build_path (G_DIR_SEPARATOR_S, dirs[i],
                             "backgrounds", "xfce");
		if (g_file_test (tmp_path, G_FILE_TEST_IS_DIR)) {
			path = g_strdup (tmp_path);
		}
		g_free (tmp_path);
	}

	if (path != NULL)
	{
		printf ("%s\n", path);
		g_free (path);
	}

	return 0;
}
