#!/bin/sh

# Clone the FreeBSD GNOME development repo.
#

WRKDIR=${HOME}/tmp

# Don't change anything below!
CWD=$(pwd)

if [ ! -d ${WRKDIR} ]; then
    mkdir ${WRKDIR} ;
fi

cd ${WRKDIR}

# Fetch the 3.32 branch only!
git clone --branch gnome-3.32 --single-branch \
    https://github.com/freebsd/freebsd-ports-gnome.git

touch ${WRKDIR}/ports.txt

for file in $(find freebsd-ports-gnome/ -type f -name 'Makefile'); do
    grep "MASTER_SITES=	GNOME" ${file} && echo $(dirname ${file#freebsd-ports-gnome/}) >> ${WRKDIR}/ports.txt;
done

cd ${CWD}
