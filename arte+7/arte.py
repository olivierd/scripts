#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2017-2022 Olivier Duchateau <duchateau.olivier@gmail.com>
#
# SPDX-License-Identifier: 0BSD
#

'''Get the MPEG-4 stream of program from arte.tv.
It supports:
- ARTE Concert
- ARTE Creative
- ARTE Programs (former Arte+7)
'''

import argparse
import io
import json
import os
import pathlib
import re
import sys
import unicodedata

try:
    import certifi
    import urllib3
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


class Download(object):
    '''Fetch a stream from URL.'''

    def __init__(self, url, dirname, title):
        fragment = urllib3.util.url.parse_url(url)

        self.path = fragment.path
        if self.path is not None:
            if not isinstance(dirname, pathlib.Path):
                p = pathlib.Path(dirname)
            else:
                p = dirname
            self.lookup_directory(p)
            self.filename = p / title

            self.conn = urllib3.HTTPSConnectionPool(fragment.host,
                                                    cert_reqs='CERT_REQUIRED',
                                                    ca_certs=certifi.where())
        else:
            self.conn = None

    def lookup_directory(self, path):
        '''Check if folder exists, if not create new directory.'''
        if not isinstance(path, pathlib.Path):
            p = pathlib.Path(path)
        else:
            p = path

        if not p.exists():
            p.mkdir(parents=True)

    def get_stream(self):
        tmp_filename = self.filename.with_suffix('.tmp')

        if self.conn is not None:
            if self.filename.with_suffix('.mp4').exists():
                print('Video already exists')
                sys.exit(0)

            if tmp_filename.exists():
                mode = 'ab'
                size = tmp_filename.stat().st_size
                headers = {'Range': 'bytes={0}-'.format(size)}
            else:
                mode = 'wb'
                headers = None

            res = self.conn.request('GET', self.path, headers=headers,
                                    preload_content=False)
            if res.status == 200 or res.status == 206:
                with io.BufferedWriter(io.FileIO(tmp_filename, mode)) as f:
                    for chunk in res.stream():
                        f.write(chunk)

                res.release_conn()

                tmp_filename.rename(self.filename.with_suffix('.mp4'))
            else:
                print('Wrong response from server')
                sys.exit(-1)

class ArteAPI(object):
    '''Use arte.tv API in order get stream URI.'''

    def __init__(self, video_id, lang, quality_key):
        host = 'api.arte.tv'

        quality_enum = {1: ['HTTPS_EQ_1', 'HTTPS_MP4_EQ_1'],
                        2: ['HTTPS_HQ_1', 'HTTPS_MP4_HQ_1'],
                        3: ['HTTPS_MQ_1', 'HTTPS_MP4_MQ_1']}
        self.quality = quality_enum.get(quality_key)
        self.title = []
        self.stream = None

        if video_id is not None:
            self.vid = video_id
            self.lang = lang
            self.conn = urllib3.HTTPSConnectionPool(host,
                                                    cert_reqs='CERT_REQUIRED',
                                                    ca_certs=certifi.where())
        else:
            self.conn = None

    def arte_deserialize_json(self, content):
        data = json.loads(content)
        key = None

        if 'videoJsonPlayer' in data:
            d = data.get('videoJsonPlayer').copy()

            if 'VTI' in d:
                self.title.append(d.get('VTI'))
                if 'subtitle' in d:
                    self.title.append(d.get('subtitle'))

            # We ensure, there is no empty list
            if 'VSR' in d and d.get('VSR'):
                if self.quality[0] in d.get('VSR'):
                    key = self.quality[0]
                elif self.quality[1] in d.get('VSR'):
                    key = self.quality[1]

                if key is not None and key in d.get('VSR'):
                    self.stream = d.get('VSR').get(key).get('url')
            else:
                print('Video is not yet or anymore available')
                sys.exit(0)

    def arte_request(self):
        headers = {'content-type': 'application/json'}

        if self.conn is not None:
            path = '/api/player/v1/config/{0}/{1}'.format(self.lang,
                                                          self.vid)
            res = self.conn.request('GET', path, headers=headers)
            if res.status == 200:
                self.arte_deserialize_json(res.data.decode('utf-8'))

            self.conn.close()

class Util(object):
    '''Some utilities methods.'''

    def __init__(self, url):
        fragment = urllib3.util.url.parse_url(url)

        self.lang = self.get_iso3166()
        self.path = fragment.path

        self.conn = urllib3.HTTPSConnectionPool(fragment.host,
                                                cert_reqs='CERT_REQUIRED',
                                                ca_certs=certifi.where())

    def get_iso3166(self):
        '''ISO 3366-1 alpha-2 code of two-letter country code.'''
        lang = None
        code = None

        lang = os.environ['LANG']
        if lang is not None or lang != 'C':
            tokens = lang.split('_')
            if len(tokens) == 2:
                code = tokens[0]
        else:
            lang = os.environ['LC_CTYPE']
            if lang is not None or lang != 'C':
                tokens = lang.split('_')
                if len(tokens) == 2:
                    code = tokens[0]

        if code in ['de', 'fr', 'es', 'en', 'it', 'pl']:
            return code
        else:
            return 'de'

    def find_video_reference(self, content):
        # Pattern to search
        p = re.compile(b'page_name":"(?P<id>.+?)",')

        res = p.search(content)
        if res is not None:
            internal_ref = res.group('id')
            return self._extract_video_id(internal_ref.decode('utf-8'))
        else:
            return None

    def _extract_video_id(self, data):
        tokens = data.split('_')

        if len(tokens) == 2:
            return tokens[0]
        else:
            return None

    def get_video_id(self):
        '''Find identifiant of video from HTML page.'''
        video_id = None
        headers = {'content-type': 'text/html'}

        res = self.conn.request('GET', self.path, headers=headers)
        if res.status == 200:
            video_id = self.find_video_reference(res.data)
            if video_id is None:
                print('Unable to get identifiant of video')
                sys.exit(0)
        else:
            print('Bad request')
            sys.exit(0)

        self.conn.close()

        return video_id

    def slugify(self, iterable, sep=b'-'):
        result = []
        p = re.compile(r'[\t !"#$%:&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

        title = ' '.join(iterable)

        for c in p.split(title.lower()):
            c = unicodedata.normalize('NFKD', c).encode('ascii', 'ignore')
            if c:
                result.append(c)

        # Return bytes object!
        return sep.join(result)

def absolute_path(path):
    '''Returns an absolute pathlib.Path object.'''
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            p = path
        else:
            p = path.resolve()
    else:
        if path.startswith('~'):
            p = pathlib.Path(path).expanduser()
        else:
            p = pathlib.Path(path).resolve()

    return p

def valid_uri(resource):
    '''Check right host.'''
    res = False

    fragment = urllib3.util.url.parse_url(resource)
    if 'arte.tv' in fragment.host:
        res = True

    return res

def main(args):
    if valid_uri(args.url):
        # Let's go
        util = Util(args.url)
        video_id = util.get_video_id()

        if video_id is not None:
            a = ArteAPI(video_id, util.lang, args.quality)
            a.arte_request()

            fetch = Download(a.stream, args.path,
                             util.slugify(a.title).decode('utf-8'))
            fetch.get_stream()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-q', '--quality', type=int,
                        choices=[1, 2, 3],
                        default=2,
                        dest='quality',
                        help='quality of video (1 is higher than 3)')
    parser.add_argument('-d', '--directory',
                        default=pathlib.Path.home(),
                        dest='path',
                        type=absolute_path,
                        help='directory to store the video')
    parser.add_argument('url',
                        help='link to broadcast')
    main(parser.parse_args())
