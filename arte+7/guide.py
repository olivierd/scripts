#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Get list of programs from arte.tv/<LANGUAGE>/guide/ """

import json
import os
import re
import sys
import time

try:
    import certifi
    import urllib3
except ImportError:
    print('Additional modules are required')
    sys.exit(1)


class ArteGuide(object):

    def __init__(self, when=None):
        self.progs_list = []
        self.uri = 'https://www.arte.tv'

        if when is None:
            self.date = self._get_day()
        else:
            self.date = when
        self.lang = self._guess_locale()
        self.conn = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',
                                        ca_certs=certifi.where())

    def _guess_locale(self):
        lang = None

        lang = os.environ['LANG']
        if lang is not None or lang != 'C':
            iso = lang.split('_')[0]

            if iso in ['de', 'en', 'es', 'fr', 'pl']:
                return iso 
            else:
                return 'en'
        else:
            return 'en'

    def _get_json_data(self, content):
        # Pattern to search
        p = re.compile(b'window.__INITIAL_STATE__ = (?P<data>\{.+?\});')

        res = p.search(content)
        if res is not None:
            return res.group('data')

    def _get_day(self):
        date = time.strftime('%y-%m-%d', time.gmtime(None))

        return date

    def _parse_json_data(self, sequence):
        key = None
        title = []
        program = {}
        
        for seq in sequence:
            if 'programId' in seq:
                key = seq['programId']
                program['id'] = key
                print(program)
            if key is not None and 'url' in seq:
                program['url'] = seq['url']
            if 'title' in seq:
                title.append(seq['title'])
            if key is not None and 'subtitle' in seq:
                if seq['subtitle'] is not None:
                    title.append(seq['subtitle'])

                if title:
                    program['title'] = ' - '.join(title)
                title = []
            if key is not None and 'duration' in seq:
                program['duration'] = seq['duration']

                self.progs_list.append(program)

                program = {}
                key = None

    def _parse_json_in_html(self, content):
        """Parse JSON data contains within HTML page."""

        prg = {}

        # Build the key
        key = 'TV_GUIDE_%s_{"day":"%s"}' % (self.lang, self.date)

        doc = json.loads(content.decode('utf-8'))

        # We made shallow copy
        prg = doc['pages']['list'][key].copy()

        for zone in prg['zones']:
            if zone.get('code') == 'listing_TV_GUIDE':
                self._parse_json_data(zone.get('data'))
                #for i in zone.get('data'):
                    #print(i.keys())

    def request(self, path):
        json_data = None
        resource = '%s/%s/%s/' % (self.uri, self.lang, path)

        res = self.conn.request('GET', resource)

        if res.status == 200:
            if 'html' in res.getheader('Content-Type'):
                json_data = self._get_json_data(res.data)
                if json_data is not None:
                    self._parse_json_in_html(json_data)
                else:
                    print('Can\'t find programs')
        else:
            print('An error occured!')

        if self.progs_list:
            print(self.progs_list)


def main():
    path = 'guide'

    ag = ArteGuide()
    ag.request(path)


if __name__ == '__main__':
    main()
