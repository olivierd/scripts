/**
 * Example using GRegex.
 *
 * gcc -Wall `pkgconf --cflags --libs glib-2.0` -o regex-test \
 *      regex-test.c
 */

#include <stdio.h>

#include <glib.h>

int main (int args, char *argv[])
{
	GRegex      *re = NULL;
	/* Pattern to search */
	const gchar *pattern = "[+-][0-9]{2}:[0-9]{2}$";
	/* The string to scan */
	const gchar *date = "2019-02-28T03:27:58+01:00";
	gchar       *res;

	re = g_regex_new (pattern, 0, 0, NULL);
	if (re != NULL) {
		if (g_regex_match (re, date, 0, NULL)) {
			res = g_regex_replace (re, date, -1, 0, "Z", 0, NULL);
			fprintf (stdout, "%s\n", res);

			g_free (res);
		}
	}

	g_regex_unref (re);

	return 0;
}
