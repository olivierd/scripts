/**
 * Display processes.
 *
 * cc -Wall -I/usr/include -lc -lkvm \
 * `pkgconf --cflags --libs glib-2.0` \
 * -o process struct-process.c
 */

#include <stdio.h>
#include <stdlib.h> /* free() */
#include <paths.h> /* _PATH_DEVNULL */
#include <unistd.h> /* getuid() */
#include <string.h>
#include <fcntl.h>
#include <kvm.h>
#include <sys/param.h>
#include <sys/sysctl.h>
#include <sys/user.h> /* it contains kinfo_proc structure */
#include <sys/types.h>
#include <pwd.h>
#include <sys/vmmeter.h> /* MAXSLP */

#include <glib.h>

struct task
{
	pid_t pid;
	pid_t ppid; /* parent PID */
	char cmd[256];
	char user[256];
	uid_t uid;
	char state[16];
	int prio;
};

void
display_process(struct kinfo_proc *kp, struct task *task)
{
	struct passwd *pw;
	int i = 0;

	task->pid = kp->ki_pid;
	task->ppid = kp->ki_ppid;
	strlcpy(task->cmd, kp->ki_comm, sizeof(task->cmd));
	
	pw = getpwuid(kp->ki_uid);
	if (pw != NULL)
	{
		strlcpy(task->user, pw->pw_name, sizeof(task->user));
	}

	task->uid = kp->ki_uid;
	
	switch (kp->ki_stat)
	{
	case SSTOP:
		task->state[i] = 'T';
		break;

	case SSLEEP:
		if (kp->ki_tdflags & TDF_SINTR)
			task->state[i] = kp->ki_slptime >= MAXSLP ? 'I' : 'S';
		else
			task->state[i] = 'D';
		break;

	case SRUN:
	case SIDL:
		task->state[i] = 'R';
		break;

	case SWAIT:
		task->state[i] = 'W';
		break;

	case SLOCK:
		task->state[i] = 'L';
		break;

	case SZOMB:
		task->state[i] = 'Z';
		break;

	default:
		task->state[i] = '?';
	}
	i++;
	task->state[i] = '\0';

	task->prio = kp->ki_nice;
	
	printf("%d (%s) %s [%s]\n", (int) task->pid, task->state, task->cmd, task->user);
}

struct task *
display_process_v2(struct kinfo_proc *kp, struct task *task)
{
	struct passwd *pw;
	int i = 0;

	task->pid = kp->ki_pid;
	task->ppid = kp->ki_ppid;
	strlcpy(task->cmd, kp->ki_comm, sizeof(task->cmd));
	
	pw = getpwuid(kp->ki_uid);
	if (pw != NULL)
	{
		strlcpy(task->user, pw->pw_name, sizeof(task->user));
	}

	task->uid = kp->ki_uid;
	
	switch (kp->ki_stat)
	{
	case SSTOP:
		task->state[i] = 'T';
		break;

	case SSLEEP:
		if (kp->ki_tdflags & TDF_SINTR)
			task->state[i] = kp->ki_slptime >= MAXSLP ? 'I' : 'S';
		else
			task->state[i] = 'D';
		break;

	case SRUN:
	case SIDL:
		task->state[i] = 'R';
		break;

	case SWAIT:
		task->state[i] = 'W';
		break;

	case SLOCK:
		task->state[i] = 'L';
		break;

	case SZOMB:
		task->state[i] = 'Z';
		break;

	default:
		task->state[i] = '?';
	}
	i++;
	task->state[i] = '\0';

	task->prio = kp->ki_nice;
	
	return task;
}

void
display_process_v3(struct kinfo_proc *kp, struct task *task)
{
	struct passwd *pw;
	int i = 0;

	task->pid = kp->ki_pid;
	task->ppid = kp->ki_ppid;
	strlcpy(task->cmd, kp->ki_comm, sizeof(task->cmd));
	
	pw = getpwuid(kp->ki_uid);
	if (pw != NULL)
	{
		strlcpy(task->user, pw->pw_name, sizeof(task->user));
	}

	task->uid = kp->ki_uid;
	
	switch (kp->ki_stat)
	{
	case SSTOP:
		task->state[i] = 'T';
		break;

	case SSLEEP:
		if (kp->ki_tdflags & TDF_SINTR)
			task->state[i] = kp->ki_slptime >= MAXSLP ? 'I' : 'S';
		else
			task->state[i] = 'D';
		break;

	case SRUN:
	case SIDL:
		task->state[i] = 'R';
		break;

	case SWAIT:
		task->state[i] = 'W';
		break;

	case SLOCK:
		task->state[i] = 'L';
		break;

	case SZOMB:
		task->state[i] = 'Z';
		break;

	default:
		task->state[i] = '?';
	}
	i++;
	task->state[i] = '\0';

	task->prio = kp->ki_nice;
}

int
main(int argc, char *argv[])
{
	kvm_t *kd;
	struct kinfo_proc *kp;
	struct task task;
	int cnt;
	GArray *task_list;

	/*
	struct task *task = malloc(sizeof(struct task));
	if(task == NULL)
		free(task);
	*/
	
	task_list = g_array_new(FALSE, FALSE, sizeof(struct task));
	
	kd = kvm_open(NULL, _PATH_DEVNULL, NULL, O_RDONLY, "kvm_open");
	if (kd == NULL)
		kvm_close(kd);

	kp = kvm_getprocs(kd, KERN_PROC_UID, getuid(), &cnt);
	if (kp == NULL)
	{
		perror("kvm_getprocs");
		kvm_close(kd);
	}

	//kvm_close(kd);

	g_array_set_size(task_list, cnt);
	for (int i = 0; i < cnt; kp++, i++)
	{
		//display_process(kp, &task);
		//display_process_v2(kp, task);
		display_process_v3(kp, &task);
		g_array_append_val(task_list, task);
	}

	if (task_list->len > 0)
	{
		for (int j = 0; j < task_list->len; j++)
		{
			struct task *t = &g_array_index(task_list, struct task, j);
			if (t->pid > 0)
				printf("%d (%s) %s [%s]\n", (int) t->pid, t->state, t->cmd, t->user);
		}
	}

	kvm_close(kd);
	//free(task);

	return 0;
}
