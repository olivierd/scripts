#!/usr/bin/env python2
# -*- coding: UTF-8 -*-

import argparse
import datetime
import os
import xml.etree.ElementTree as ET


class SVNLog(object):

    def __init__(self, tree_obj, author='', year=None):
        self.tree = tree_obj
        if author is None or author == '':
            # Me, myself and I
            self.author = 'olivierd'
        else:
            self.author = author
        if year is None:
            self.year = datetime.date.today().year
        else:
            self.year = year
        self.commits = {}

    def svn_log_lookup_date(self, string):
        tokens = string.split('T')
        if len(tokens) == 2:
            return tokens
        else:
            return None
    
    def svn_log_split_date(self, string, idx=0):
        date = self.svn_log_lookup_date(string)
        if date is not None and idx in [0, 1, 2]:
            return date[0].split('-')[idx]
        else:
            return None

    def svn_log_dic_update(self, string, val):
        month = self.svn_log_split_date(string, 1)
        if month in self.commits:
            # Get values of 'month' key
            values = self.commits.get(month)
            # Add new value
            values.append(val)

            self.commits[month] = values
        else:
            self.commits[month] = [val]
        
    def svn_log_parse(self):
        rev = ''
        valid = False
        
        root = self.tree.getroot()

        for node in root.iter():
            if node.tag == 'logentry':
                rev = node.attrib['revision']
            elif node.tag == 'author':
                if node.text == self.author:
                    valid = True
            elif node.tag == 'date' and valid:
                # Get year
                y = self.svn_log_split_date(node.text)
                if y is not None:
                    if int(y) == self.year:
                        self.svn_log_dic_update(node.text, rev)
                    elif int(y) < self.year:
                        break

                valid = False
                res = ''


def save(dic, path):
    filename = 'data.txt'

    f = open(os.path.join(path, filename), 'w')
    
    # Sorted keys
    keys = sorted(dic)
    for k in keys:
        f.write('%s\t%d%s' % (k, len(dic[k]), os.linesep))
    
    f.close()    

def main(args):
    filename = os.path.abspath(''.join(args.log))

    if os.path.exists(filename):
        l = SVNLog(ET.parse(filename),author=args.author,
                   year=args.year)
        l.svn_log_parse()

        if l.commits:
            save(l.commits, os.path.expanduser('~'))
    else:
        print 'No such file or directory'
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('log', nargs=1,
                        help='SVN log (need full path)')
    parser.add_argument('-y', type=int, dest='year', metavar='YEAR',
                        help='when (eg. 2016)')
    parser.add_argument('-u', dest='author', metavar='AUTHOR',
                        help='who (author of commits)')
    
    main(parser.parse_args())
