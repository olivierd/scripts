#!/bin/sh
#
# Shell script in order to generate PNG file of commits per month.
#

CWDIR=`pwd`

if [ $# -eq 1 ]; then
		if [ -d /usr/ports ]; then
				cd /usr/ports ;

				svn log --xml > ~/svn.log ;

				cd $CWDIR ;

				python2 svnlog.py ~/svn.log -y $1 ;

				if [ -e ~/data.txt ]; then
						Rscript barplot.R $1 ;

						mv ~/commits.png ~/commits-$1.png ;
				fi

				rm ~/data.txt ;
				rm ~/svn.log ;
		fi
fi
