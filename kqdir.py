#!/usr/bin/env python
# *-*- coding: iso-8859-15
# $Id$

"""Monitoring a given directory on BSD systems."""


import os
import select
import sys


def main(arg):
		# Normalize top directory path.
		if arg.startswith("~"):
				top_dir = os.path.expanduser("~")
		else:
				top_dir = os.path.abspath(arg)

		try:
				# Get file descriptor.
				fd = os.open(top_dir, os.O_RDONLY | os.O_NONBLOCK)

				kq = select.kqueue()

				# KQ_NOTE_WRITE is the only filter flags (fflags), when we are
				# monitoring a directory.
				ke = select.kevent(fd, filter=select.KQ_FILTER_VNODE, \
													 flags=select.KQ_EV_ADD | select.KQ_EV_CLEAR, \
													 fflags=select.KQ_NOTE_WRITE)

				#
				kq.control([ke], 0, 0)

				while True:
						kq_events = kq.control(None, 1)
						for event in kq_events:
								if event.fflags == select.KQ_NOTE_WRITE:
										print "Something happened in %s" % top_dir

				kq.close()
				
				os.close(fd)
		except IOError:
				pass

if __name__ == "__main__":
		if len(sys.argv) == 2:
				main(sys.argv[1])
		else:
				print "Usage: python %s <dirname>" % os.path.basename(sys.argv[0])
