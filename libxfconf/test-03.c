#include <stdio.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <xfconf/xfconf.h>

#include "dummy-conf.h"

int
main (int argc, char *argv[]) {
	GError *err = NULL;
	DummyConf *conf;

	if (xfconf_init (&err)) {
		conf = dummy_conf_new ();

		/* before change */
		g_fprintf (stdout, "%d\n", dummy_conf_get_int_prop (conf,
                                                            "window-height"));
		if (dummy_conf_get_boolean_prop (conf, "recursive")) {
			dummy_conf_set_int_prop (conf, "window-height", 500);
			/* after change */
			g_fprintf (stdout, "%d\n",
                       dummy_conf_get_int_prop (conf,
                                                "window-height"));
		}
	}

	if (err != NULL) {
		g_fprintf (stderr, "Error: %s\n", err->message);
		g_error_free (err);
	}

	xfconf_shutdown ();

	return 0;
}
