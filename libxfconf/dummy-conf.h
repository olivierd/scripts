#ifndef DUMMY_CONF_H_
#define DUMMY_CONF_H_

#include <glib-object.h>

G_BEGIN_DECLS;

typedef struct _DummyConfClass DummyConfClass;
typedef struct _DummyConf      DummyConf;

#define DUMMY_TYPE_CONF            (dummy_conf_get_type ())
#define DUMMY_CONF(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DUMMY_TYPE_CONF, DummyConf))
#define DUMMY_CONF_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DUMMY_TYPE_CONF, DummyConfClass))
#define DUMMY_IS_CONF(o)           (G_TYPE_CHECK_INSTANCE_TYPE ((o), DUMMY_TYPE_CONF))
#define DUMMY_IS_CONF_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DUMMY_TYPE_CONF))
#define DUMMY_CONF_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DUMMY_TYPE_CONF, DummyConfClass))

GType      dummy_conf_get_type (void);
DummyConf *dummy_conf_new (void);

/* Convenience functions */
gboolean   dummy_conf_get_boolean_prop (DummyConf *, const gchar *);
void       dummy_conf_set_boolean_prop (DummyConf *, const gchar *,
                                        gboolean);
gint       dummy_conf_get_int_prop (DummyConf *, const gchar *);
void       dummy_conf_set_int_prop (DummyConf *, const gchar *, gint);

G_END_DECLS;

#endif
