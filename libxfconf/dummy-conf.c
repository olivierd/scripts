
#include <stdio.h>

#include <glib.h>
#include <glib/gprintf.h>

#include <xfconf/xfconf.h>

#include "dummy-conf.h"


struct _DummyConfClass {
	GObjectClass __parent__;
};

struct _DummyConf {
	GObject __parent__;

	XfconfChannel *channel;
	gulong         property_changed_id;
};

/* Property identifiers */
enum {
	PROP_0,
	PROP_WIDTH,
	PROP_HEIGHT,
	PROP_RECURSIVE,
	N_PROPERTIES
};

static void dummy_conf_finalize (GObject *object);
static void dummy_conf_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *spec);
static void dummy_conf_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *spec);
static void dummy_conf_prop_changed (XfconfChannel *channel,
                                     const gchar   *prop_name,
                                     const GValue  *value,
                                     DummyConf     *conf);

G_DEFINE_TYPE (DummyConf, dummy_conf, G_TYPE_OBJECT)

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void
dummy_conf_finalize (GObject *object) {
	DummyConf *conf = DUMMY_CONF (object);

	/* disconnect from the updates */
	g_signal_handler_disconnect (conf->channel, conf->property_changed_id);

	(*G_OBJECT_CLASS(dummy_conf_parent_class)->finalize) (object);
}

static void
dummy_conf_get_property (GObject *object, guint prop_id,
                         GValue *value,
                         GParamSpec *pspec) {
	DummyConf   *conf = DUMMY_CONF (object);
	GValue       src = { 0, };
	const gchar *nick;
	gchar      **array;

	/* only set defaults if channel is not set */
	if (G_UNLIKELY (conf->channel == NULL)) {
		g_param_value_set_default (pspec, value);
		return;
	}

	/* build property name */
	nick = g_strdup_printf ("%s", g_param_spec_get_nick (pspec));

	if (G_VALUE_TYPE (value) == G_TYPE_STRV) {
		array = xfconf_channel_get_string_list (conf->channel, nick);
		g_value_take_boxed (value, array);
	}
	else if (xfconf_channel_get_property (conf->channel, nick, &src)) {
		if (G_VALUE_TYPE (value) == G_VALUE_TYPE (&src))
			g_value_copy (&src, value);
		else if (!g_value_transform (&src, value))
			g_fprintf (stderr, "Failed to transform property %s\n", nick);
		g_value_unset (&src);
	}
	else {
		g_param_value_set_default (pspec, value);
	}
}

static void
dummy_conf_set_property (GObject *object, guint prop_id,
                         const GValue *value,
                         GParamSpec *pspec) {
	DummyConf   *conf = DUMMY_CONF (object);
	GValue       dst = { 0, };
	const gchar *nick;
	gchar      **array;

	/* leave if the channel is not set */
	if (G_UNLIKELY (conf->channel == NULL))
		return;

	/* build property name */
	nick = g_strdup_printf ("%s", g_param_spec_get_nick (pspec));

	/* freeze */
	g_signal_handler_block (conf->channel, conf->property_changed_id);

	if (G_VALUE_HOLDS_ENUM (value)) {
		g_value_init (&dst, G_TYPE_STRING);
		if (g_value_transform (value, &dst))
			xfconf_channel_set_property (conf->channel, nick, &dst);
		g_value_unset (&dst);
	}
	else if (G_VALUE_HOLDS (value, G_TYPE_STRV)) {
		array = g_value_get_boxed (value);
		if (array != NULL && *array != NULL)
			xfconf_channel_set_string_list (conf->channel, nick,
                                            (const gchar * const *) array);
		else
			xfconf_channel_reset_property (conf->channel, nick, FALSE);
	}
	else {
		/* other types we support directly */
		xfconf_channel_set_property (conf->channel, nick, value);
	}

	g_signal_handler_unblock (conf->channel, conf->property_changed_id);
}

static void
dummy_conf_prop_changed (XfconfChannel *channel, const gchar *prop_name,
                         const GValue *value,
                         DummyConf *conf) {
	GParamSpec *pspec;

	/* check if the property exists and emit change */
	pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (conf),
                                          prop_name + 1);
	if (G_LIKELY (pspec != NULL))
		g_object_notify_by_pspec (G_OBJECT (conf), pspec);
}

static void
dummy_conf_class_init (DummyConfClass *klass) {
	GObjectClass *obj_class = G_OBJECT_CLASS (klass);

	obj_class->finalize = dummy_conf_finalize;

	obj_class->get_property = dummy_conf_get_property;
	obj_class->set_property = dummy_conf_set_property;

	/* Xfconf property: /window/width */
	obj_properties[PROP_WIDTH] = g_param_spec_int ("window-width",
                                                   "/window/width",
                                                   "",
                                                   0, G_MAXINT,
                                                   600,
                                                   G_PARAM_READWRITE);

	/* Xfconf property: /window/height */
	obj_properties[PROP_HEIGHT] = g_param_spec_int ("window-height",
                                                    "/window/height",
                                                    "",
                                                    0, G_MAXINT,
                                                    480,
                                                    G_PARAM_READWRITE);

	/* Xfconf property: /subdir */
	obj_properties[PROP_RECURSIVE] = g_param_spec_boolean ("recursive",
                                                           "/recursive",
                                                           "",
                                                           FALSE,
                                                           G_PARAM_READWRITE);

	/* install all properties */
	g_object_class_install_properties (obj_class, N_PROPERTIES,
                                       obj_properties);
}

/* Initialize DummyConf instance */
static void
dummy_conf_init (DummyConf *conf) {
	//const gchar check_prop[] = "/recursive";

	/* load the channel */
	conf->channel = xfconf_channel_get ("xfce4-dummy");

	/* check one of property */
	/*if (!xfconf_channel_has_property (conf->channel, check_prop)) {
		xfconf_channel_set_bool (conf->channel, check_prop, FALSE);
	}*/

	conf->property_changed_id = g_signal_connect (G_OBJECT (conf->channel),
                                                  "property-changed",
                                                  G_CALLBACK (dummy_conf_prop_changed),
                                                  conf);
}

DummyConf *
dummy_conf_new (void) {
	return g_object_new (DUMMY_TYPE_CONF, NULL);
}

gboolean
dummy_conf_get_boolean_prop (DummyConf *conf, const gchar *prop_name) {
	gboolean value;
	GValue val = { 0, };

	g_value_init (&val, G_TYPE_BOOLEAN);

	g_object_get_property (G_OBJECT(conf), prop_name, &val);
	value = g_value_get_boolean (&val);

	g_value_reset (&val);

	return value;
}

void
dummy_conf_set_boolean_prop (DummyConf *conf, const gchar *prop_name,
                             gboolean value) {
	GValue val = { 0, };

	g_value_init (&val, G_TYPE_BOOLEAN);

	g_value_set_boolean (&val, value);
	g_object_set_property (G_OBJECT(conf), prop_name, &val);

	g_value_reset (&val);
}

gint
dummy_conf_get_int_prop (DummyConf *conf, const gchar *prop_name) {
	gint value;
	GValue val = { 0, };

	g_value_init (&val, G_TYPE_INT);

	g_object_get_property (G_OBJECT (conf), prop_name, &val);
	value = g_value_get_int (&val);

	g_value_reset (&val);

	return value;
}

void
dummy_conf_set_int_prop (DummyConf *conf, const gchar *prop_name,
                         gint value) {
	GValue val = { 0, };

	g_value_init (&val, G_TYPE_INT);

	g_value_set_int (&val, value);
	g_object_set_property (G_OBJECT (conf), prop_name, &val);

	g_value_reset (&val);
}
