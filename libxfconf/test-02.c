/**
 * Simple test, using the libxfconf-0 API.
 *
 * $CC `pkgconf --cflags --libs glib-2.0 libxfconf-0` test-02.c -o test-02
 */

#include <stdio.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <xfconf/xfconf.h>

int main (int argc, char *argv[]) {
	GError *err = NULL;
	gchar **channels;
	int     i;

	if (xfconf_init (&err)) {
		channels = xfconf_list_channels ();
		for (i = 0; channels[i]; i++) {
			g_fprintf (stdout, "%s\n", channels[i]);
		}

		g_strfreev (channels);
	}

	if (err != NULL) {
		g_fprintf (stderr, "Error: %s\n", err->message);
		g_error_free (err);
	}

	xfconf_shutdown ();

	return 0;
}
