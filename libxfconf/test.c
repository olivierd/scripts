/**
 * Simple test, using the libxfconf-0 API.
 *
 * $CC `pkgconf --cflags --libs glib-2.0 libxfconf-0` test.c -o test
 */

#include <stdio.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <xfconf/xfconf.h>

int main (int argc, char *argv[]) {
	GError        *err = NULL;
	XfconfChannel *channel;

	if (xfconf_init (&err)) {
		channel = xfconf_channel_get ("xfce4-desktop");

		if (xfconf_channel_has_property (channel, "/last/window-height")) {
			g_fprintf (stdout, "Ok\n");
		}
		else {
			g_fprintf (stderr, "Error\n");
		}
	}

	if (err != NULL) {
		g_fprintf (stderr, "Error: %s\n", err->message);
		g_error_free (err);
	}

	xfconf_shutdown ();

	return 0;
}
