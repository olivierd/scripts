/*
 * valac --pkg=glib-2.0 --pkg=gio-2.0 --pkg=libsoup-2.4 downloader.vala
 *
 * Download remote resource.
 *
 */

using GLib;
using Soup;

public class Downloader : Object {
 
	private string url;
	private URI parsed;
	private Session session;
	private Message msg;
	private Buffer buffer;

	public Downloader (string url) {
		this.url = url;

		this.session = new Session ();
	}

	private void get_resource (string filename, Buffer buffer) {
		string path = null;
		File file;
		BufferedOutputStream output_stream;
		DataOutputStream dos;

		try {
			path = Path.build_path (Path.DIR_SEPARATOR_S,
									Environment.get_home_dir (),
									filename);
			file = File.new_for_path (path);

			if (file.query_exists ()) {
				file.delete ();
			}

			output_stream = new BufferedOutputStream (file.create (FileCreateFlags.REPLACE_DESTINATION));
			output_stream.set_buffer_size (buffer.length);

			dos = new DataOutputStream (output_stream);
			dos.write_all (buffer.data, out buffer.length);

			dos.close ();
			
		} catch (Error e) {
			stderr.printf ("%s\n", e.message);
		}
	}

	private string get_real_url (string resource) {
		string new_url = null;

		/* Send a request */
		msg = new Message ("GET", resource);

		/* Signal */
		msg.got_headers.connect (() => {
				/* 302 */
				if (msg.status_code == Status.FOUND) {
					new_url = msg.response_headers.get_one ("Location");
					/* Finish processing request */
					session.cancel_message (msg, Status.CANCELLED);
				}
		});

		session.send_message (msg);

		if (new_url == null) {
			new_url = "%s".printf (resource);
		}

		return new_url;
	}

	private void request (URI destination) {
		string output = Path.get_basename (destination.get_path ());

		msg = new Message.from_uri ("GET", destination);

		/* Signal */
		msg.got_body.connect (() => {
				if (msg.status_code == Status.OK) {
					buffer = msg.response_body.flatten ();
					get_resource (output, buffer);
				}
		});

		session.send_message (msg);
	}

	public void run () {
		parsed = new URI (get_real_url (url));

		request (parsed);
		parsed.free ();
	}

	private static void usage (string program) {
		stderr.printf ("%s <url>\n", program);
	}

	public static int main (string[] args) {

		if (args.length == 2) {
			/* URL ? */
			if ("http" in args[1]) {
				var d = new Downloader (args[1]);
				d.run ();
			}
		}
		else {
			usage (args[0]);
		}

		return 0;
	}
}
