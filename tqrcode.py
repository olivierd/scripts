#!/usr/bin/env python

__description__ = '''
Small Python script which builds QR code image.

You must rewrite content of `data' variable defined in 'main'
function, before to run this program.
'''

import argparse
import os
import sys

try:
    import qrcode
except ImportError as e:
    print e
    sys.exit(1)


class TinyQrcode(object):

    def __init__(self, content, output=None):
        self.content = content
        if output is not None:
            self.output = self._build_filename(output)

        # Control the size of QR code (here 25x25 matrix)
        self.ver = 2
        self.error = qrcode.constants.ERROR_CORRECT_M

    def _build_filename(self, name):
        '''Return full path of output.'''

        root = os.path.expanduser('~')
        if os.path.exists(os.path.join(root, 'Images')):
            return os.path.join(root, 'Images', name)
        else:
            return os.path.join(root, name)

    def render(self):
        '''Create the Quick Response code (QR code).'''

        qr = qrcode.QRCode(
            error_correction=self.error,
            version=self.ver
        )

        qr.add_data(self.content)

        # Output
        img = qr.make_image()
        img.save(self.output)


def lookup_extension(filename, extension):
    tokens = filename.split('.')
    if len(tokens) == 2:
        if tokens[1] == extension:
            return filename
        else:
            return '%s.%s' % (tokens[0], extension)
    elif len(tokens) == 1:
        return '%s.%s' % (filename, extension)

def main(opts):
    data = 'http://avignu.com/'
    ext = 'png'
    
    name = lookup_extension(''.join(opts.filename), ext)

    qr_code = TinyQrcode(data, name)
    qr_code.render()
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs=1, help='Output')

    args = parser.parse_args()
    main(args)
