# Download stream from YouTube

`yt251.py` is a Python script in order to download multimedia file (video or audio). It is licensed under the [BSD Zero Clause License](https://spdx.org/licenses/0BSD.html) (it is a public domain equivalent license).

## Requirements

- [youtube_dl](https://pypi.org/project/youtube_dl/)
- [urllib3](https://pypi.org/project/urllib3/)
- [certifi](https://pypi.org/project/certifi/)

**Note:** Make sure you use the latest version of `youtube_dl`!

## Overview

    python3 yt251.py -h
    usage: yt251.py [-h] [-audio-formats | -video-formats] [-itag ITAG]
                    [-directory DIRECTORY] -url URL
    
    optional arguments:
        -h, --help            show this help message and exit
        -audio-formats        list available audio formats
        -video-formats        list available video formats
        -itag ITAG            prefered format of stream
        -directory DIRECTORY  directory to store the stream
        -url URL              link to YouTube video

Basic usage: `python3 yt251.py -url https://youtu.be/watch?v=AZE188erb`

By default it tries to download audio file (itag equal to 251). If it is not available nothing appends. You can try **-audio-formats** or **-video-formats** options to see other formats.

- `webm2mp3.sh` convert WebM → MP3 using [FFmpeg](https://www.ffmpeg.org/)
- `webm2mp3-gst.sh` convert WebM → MP3 using [GStreamer](https://gstreamer.freedesktop.org/)

***

`playlist.py` download a playlist, for more details run:

    python3 playlist.py -h


