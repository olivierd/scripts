#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# LicenseComment: For more details, https://spdx.org/licenses/0BSD.html
# FileCopyrightText: 2020, Olivier Duchateau <duchateau.olivier@gmail.com>
#

'''
Download YouTube stream (video or audio file).

For audio by default it is the 251 YouTube's format.

Be careful, YouTube supports the DASH (Dynamic Adaptive Streamer
over HTTP) technique. If you want video, you must download video
and audio files, then merge both files (with FFmpeg, GStreamer
pipeline, and so on). Or try the 18 format (it contains both
streams).
'''

import argparse
import io
import pathlib
import re
import sys
import unicodedata
import urllib.parse

try:
    import youtube_dl
    import certifi
    import urllib3
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


def get_value_from_dict(key, iterable):
    '''Return value of given key.'''
    res = None

    if isinstance(iterable, dict) and key in iterable:
        res = iterable.get(key)

    return res

def get_formats(iterable):
    '''Return list of each format (type of media).'''
    res = []

    formats = get_value_from_dict('formats', iterable)
    for item in iter(formats):
        # 'item' is dictionary
        if isinstance(item, dict) and 'format_id' in item:
            res.append(item.get('format_id'))

    return res

def get_resource_from_nth_format(key, iterable):
    '''Return information about given format.'''
    res = None

    formats = get_value_from_dict('formats', iterable)
    for item in iter(formats):
        if 'format_id' in item and item.get('format_id') == key:
            res = item
            break

    return res

def get_url(iterable):
    return get_value_from_dict('url', iterable)

def get_extension(iterable):
    return get_value_from_dict('ext', iterable)

def get_title(iterable):
    '''Return title of stream, always returns something.'''
    res = None

    res = get_value_from_dict('title', iterable)
    if res is None:
        res = get_value_from_dict('alt_title', iterable)

    if res is None:
        return 'Unknown'
    else:
        return res

def get_audio_codec(iterable):
    return get_value_from_dict('acodec', iterable)

def get_video_codec(iterable):
    return get_value_from_dict('vcodec', iterable)

def is_only_stream(iterable, kind):
    '''Check if it is an audio or video stream.'''
    res = False

    if kind == 'audio':
        value = get_audio_codec(iterable)
    elif kind == 'video':
        value = get_video_codec(iterable)

    if value is not None and value != 'none':
        res = True

    return res

def get_resource_info(iterable):
    '''Return tuple: (chunk size, HTTP headers,).'''
    chunk_size = None
    http_headers = {}

    data = get_value_from_dict('downloader_options', iterable)
    if data and 'http_chunk_size' in data:
        chunk_size = data.get('http_chunk_size')
        http_headers = get_value_from_dict('http_headers', iterable)

    return (chunk_size, http_headers,)

def get_stream(iterable):
    '''Retrieve stream.'''
    if isinstance(iterable, dict):
        # Build filename (we have Pathlib.Path object!)
        f = iterable.get('dirname') / iterable.get('name')
        full_name = f.with_suffix(iterable.get('extension'))

        #
        http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',
                                   ca_certs=certifi.where())

        if full_name.exists():
            print('File already exists')
            sys.exit(0)
        else:
            res = http.request('GET', iterable.get('url'),
                               headers=iterable.get('headers'),
                               preload_content=False)
            if res.status == 200:
                with io.BufferedWriter(io.FileIO(full_name, 'wb')) as o:
                    for chunk in res.stream(bytes(iterable.get('chunk'))):
                        o.write(chunk)
                res.release_conn()
            else:
                print('Wrong response from server ({0})'.format(res.status))
                sys.exit(0)
    else:
        print('Error')
        sys.exit(0)

def available_formats(iterable, kind='audio'):
    '''Display available formats, useful for debug.'''
    # List of YouTube's format
    formats = get_formats(iterable)
    
    for i in iter(formats):
        stream_info = get_resource_from_nth_format(i, iterable)
        if is_only_stream(stream_info, kind):
            if kind == 'audio':
                print('{0} → {1}'.format(i, get_audio_codec(stream_info)))
            elif kind == 'video':
                print('{0} → {1}'.format(i, get_video_codec(stream_info)))

def slugify(string, sep=b'-'):
    result = []
    p = re.compile(r'[\t !"#$%:&\'()*\-/<=>?@\[\\\]^_`{|},.]+')
    
    for c in p.split(string.lower()):
        c = unicodedata.normalize('NFKD', c).encode('ascii', 'ignore')
        if c:
            result.append(c)

    # It is bytes object!
    return sep.join(result)

def clean_query(query_str):
    '''Remove some unusual parameters in query.'''
    d = urllib.parse.parse_qs(query_str)
    
    if 'v' in d:
        return 'v={0}'.format(''.join(d.get('v')))
    else:
        return query_str

def check_host(resource):
    '''Check if YouTube hostname is valid.'''
    res = False
    # Pattern
    p = re.compile(r'youtu(\.be|be\.com)?')
    
    m = p.search(resource)
    if m is not None:
        res = True
    
    return res

def set_universal_url(resource):
    '''Check and transform if needed the given URL.'''
    result = None

    if resource.startswith('http') and check_host(resource):
        # Split the URL
        r = urllib.parse.urlparse(resource)
        # Avoid others resources from YouTube
        if r.path == '/watch':
            query = clean_query(r.query)

            # Rebuild the URL
            result = urllib.parse.urlunparse((r.scheme, r.netloc,
                                              r.path, r.params,
                                              query, r.fragment,))

    return result

def youtube_request(resource, display_audio, display_video,
                    prefered_format, dirname):
    data = {}
    
    with youtube_dl.YoutubeDL() as yt:
        data = yt.extract_info(resource, download=False)

    #print(data.keys())
    if display_audio:
        available_formats(data)
    elif display_video:
        available_formats(data, kind='video')
    else:
        list_formats = get_formats(data)
        if prefered_format in list_formats:
            stream_info = get_resource_from_nth_format(prefered_format, data)
            protocol = get_value_from_dict('protocol', stream_info)
            if protocol is not None and protocol == 'https':
                #print(stream_info)
                chunk_size, headers = get_resource_info(stream_info)

                get_stream({'url': get_url(stream_info),'headers': headers,
                            'chunk': chunk_size,
                            'name': slugify(get_title(data)).decode('utf-8'),
                            'extension': '.{0}'.format(get_extension(stream_info)),
                            'dirname': dirname})
            else:
                print('Stream composed of DASH segments, not yet supported')
                sys.exit(0)
        else:
            print('{0} is not available'.format(prefered_format))
            sys.exit(0)

def main(args):
    resource = set_universal_url(args.url)
    if resource is not None:
        youtube_request(resource, args.audio_formats,
                        args.video_formats, args.itag,
                        args.directory)
    else:
        print('The given URL is not valid')
        sys.exit(0)

def absolute_path(path):
    '''Returns an absolute pathlib.Path object.'''
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            p = path
        else:
            p = path.resolve()
    else:
        if path.startswith('~'):
            p = pathlib.Path(path).expanduser()
        else:
            p = pathlib.Path(path).resolve()

    if not p.exists():
        p.mkdir(parents=True)

    return p


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-audio-formats', action='store_true',
                       dest='audio_formats',
                       help='list available audio formats')
    group.add_argument('-video-formats', action='store_true',
                       dest='video_formats',
                       help='list available video formats')
    parser.add_argument('-itag', default='251',
                        help='prefered format of stream')
    parser.add_argument('-directory', default=pathlib.Path.home(),
                        help='directory to store the stream',
                        type=absolute_path)

    parser.add_argument('-url', required=True,
                        help='link to YouTube video')
    main(parser.parse_args())
