#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# LicenseComment: For more details, https://spdx.org/licenses/0BSD.html
# FileCopyrightText: 2020, Olivier Duchateau <duchateau.olivier@gmail.com>

'''
Retrieve stream from YouTube's playlist.
'''

import argparse
import io
import pathlib
import re
import sys
import threading
import unicodedata
try:
    import certifi
    import urllib3

    import youtube_dl
except ImportError:
    print('Additional modules are required')
    sys.exit(0)


def get_value_from_dict(iterable, key):
    '''Return value of given key.'''
    res = None

    if isinstance(iterable, dict) and key in iterable:
        res = iterable.get(key)

    return res

def get_url(iterable):
    return get_value_from_dict(iterable, 'url')

def get_extension(iterable):
    return get_value_from_dict(iterable, 'ext')

def get_title(iterable):
    '''Returns title.'''
    res = None

    res = get_value_from_dict(iterable, 'title')
    if res is None:
        res = get_value_from_dict(iterable, 'alt_title')

    return res

def get_user_agent(iterable):
    res = None

    headers = get_value_from_dict(iterable, 'http_headers')
    if 'User-Agent' in headers:
        res = headers.get('User-Agent')

    return res

def get_data_from_n_format(iterable, val):
    '''Returns information about given format.'''
    res = {}

    formats = get_value_from_dict(iterable, 'formats')
    for i in iter(formats):
        if 'format_id' in i and i.get('format_id') == val:
            res = i
            break

    return res

def get_formats(iterable):
    '''Return list of YouTube's format.'''
    res = []

    formats = get_value_from_dict(iterable, 'formats')
    for i in iter(formats):
        if isinstance(i, dict) and 'format_id' in i:
            res.append(i.get('format_id'))

    return res

def bytes_2_str(obj):
    res = None
    if isinstance(obj, bytes):
        res = obj.decode('utf-8')
    else:
        res = obj

    return res

def slugify(string, sep=b'-'):
    res = []
    p = re.compile(r'[\t !"#$%:&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

    for c in p.split(string.lower()):
        c = unicodedata.normalize('NFKD', c).encode('ascii', 'ignore')
        if c:
            res.append(c)

    # It is bytes object!
    return sep.join(res)

def get_video_id(content):
    '''Return bytes object'''
    res = None
    p = re.compile(b' data-video-id="(?P<video>.+?)" data-.+?')

    m = p.search(content)
    if m is not None:
        res = m.group('video')

    return res

def set_watch_url(video_id):
    return 'https://www.youtube.com/watch?v={0}'.format(video_id)

def find_videos(pool_obj, resource):
    '''From a playlist, find all videos.'''
    list_id = []

    r = pool_obj.request('GET', resource, preload_content=False)
    if r.status == 200:
        # r.data is bytes object!
        for chunk in r.stream(1024):
            i = get_video_id(chunk)
            #if i is not None and i not in list_id:
            #    list_id.append(i)
            if i is not None:
                list_id.append(i)
    else:
        print('Error: code {0}'.format(r.status))
    r.release_conn()

    return [set_watch_url(bytes_2_str(j)) for j in list_id]

def get_stream(pool_obj, iterable):
    # Build filename (we have pathlib.Path object)
    f = iterable.get('dirname') / iterable.get('name')
    full_name = f.with_suffix(iterable.get('extension'))

    if 'user_agent' in iterable:
        headers = {'User-Agent': iterable.get('user_agent')}
    else:
        headers = {}

    if not full_name.exists():
        r = pool_obj.request('GET', iterable.get('url'),
                             headers=headers,
                             preload_content=False)
        if r.status == 200:
            with io.BufferedWriter(io.FileIO(full_name, 'wb')) as o:
                for chunk in r.stream():
                    o.write(chunk)
            r.release_conn
            print('{0} saved ({1})'.format(full_name.name,
                                          full_name.stat().st_size))
        else:
            print('Error {0} code'.format(r.status))

def youtube_request(resource, dirname, prefered_format, pool_obj):
    res = {}

    with youtube_dl.YoutubeDL() as yt:
        res = yt.extract_info(resource, download=False)

    #print(get_formats(res))
    info = get_data_from_n_format(res, prefered_format)
    if info:
        #print(info)
        protocol = get_value_from_dict(info, 'protocol')
        if protocol == 'https':
            data = {'url': get_url(info),
                    'name': slugify(get_title(res)).decode('utf-8'),
                    'extension': '.{0}'.format(get_extension(info)),
                    'dirname': dirname}

            user_agent = get_user_agent(info)
            if user_agent is not None:
                data.update({'user_agent': user_agent})

            get_stream(pool_obj, data)
        else:
            print('Protocol not yet supported')
    else:
        print('{0} is not available'.format(prefered_format))

def playlist(resource, root_dir, prefered_format):
    videos = []
    http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',
                               ca_certs=certifi.where())

    videos = find_videos(http, resource)
    if videos:
        #print(videos)
        for v in videos:
            t = threading.Thread(target=youtube_request, args=(v, root_dir,
                                                               str(prefered_format),
                                                               http,))
            t.start()

def valid_host(resource):
    res = False
    # Pattern to look for
    p = re.compile(r'youtu(\.be|be\.com)?')

    if p.search(resource) is not None:
        res = True

    return res

def is_uri_valid(resource):
    res = False

    if resource.startswith('http'):
        r = urllib3.util.url.parse_url(resource)
        if valid_host(resource) and r.path == '/playlist':
            res = True

    return res

def main(args):
    if is_uri_valid(args.url):
        playlist(args.url, args.directory, args.yt_format)
    else:
        print('Not a valid URI')
        sys.exit(0)

def absolute_path(path):
    '''Returns an absolute pathlib.Path object.'''
    if isinstance(path, pathlib.Path):
        if path.is_absolute():
            p = path
        else:
            p = path.resolve()
    else:
        if path.startswith('~'):
            p = pathlib.Path(path).expanduser()
        else:
            p = pathlib.Path(path).resolve()

    if not p.exists():
        p.mkdir(parents=True)

    return p


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-directory', default=pathlib.Path.home(),
                        type=absolute_path,
                        help='directory to store stream')
    parser.add_argument('-format', default='251',
                        dest='yt_format',
                        help='prefered stream format')

    parser.add_argument('-url', required=True,
                        help='link to YouTube\'s playlist')
    main(parser.parse_args())
