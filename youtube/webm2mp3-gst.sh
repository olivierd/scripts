#!/bin/sh -eu

# SPDX-License-Identifier: 0BSD
# LicenseComment: For more details, https://spdx.org/licenses/0BSD.html
# FileCopyrightText: 2020, Olivier Duchateau <duchateau.olivier@gmail.com>
#
# Convert WebM to MP3 file using the GStreamer pipeline.
#

PRGNAME="${0##*/}"
CWD=$(pwd)

err()
{
    echo "$@" >&2
    exit 1
}

usage()
{
    more <<EOF >&2
usage: ${PRGNAME} option

options:
    -h  show this help
    -d  directory to scan
EOF
    exit 0
}

check_gst()
{
    if [ -x $(which ${1} 2>/dev/null) ]; then
        echo $(which ${1})
    else
        err "ERROR: ${1}(1) not found"
    fi
}

check_codec()
{
    codec=$(${1} | grep "${2}" | awk -F : '{printf("%s", $2);}')
    if [ -n ${codec} ]; then
        echo "Ok: ${3}"
    else
        err "ERROR: support of ${3} not available"
    fi
}

transform()
{
    # gst-launch-1.0
    launch_cmd=$(check_gst gst-launch-1.0)
    # gst-inspect-1.0
    inspect_cmd=$(check_gst gst-inspect-1.0)
    
    # Check codecs
    check_codec ${inspect_cmd} "skademux" "Matroska"
    check_codec ${inspect_cmd} "usdec" "Opus"
    check_codec ${inspect_cmd} "3enc" "LAME"
    
    if [ ! -d ${1} ]; then
        err "ERROR: no such file or directory"
    else
        cd ${1}
        
        for file in $(find . -type f -name '*.webm'); do
            ${launch_cmd} filesrc location=$(basename ${file}) ! matroskademux \
            ! opusdec ! audioconvert ! audioresample \
            ! lamemp3enc target=quality quality=0 \
            ! filesink location="$(basename ${file%.*}).mp3" ;
            
            rm $(basename ${file}) ;
        done
        
        cd ${CWD}
    fi
}

while getopts ":d:" opt; do
    case ${opt} in
        d ) transform ${OPTARG} ;;
        \?) usage ;;
    esac
done
