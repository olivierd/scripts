#!/bin/sh -eu

# SPDX-License-Identifier: 0BSD
# LicenseComment: For more details, https://spdx.org/licenses/0BSD.html
# FileCopyrightText: 2020, Olivier Duchateau <duchateau.olivier@gmail.com>
#
# Convert WebM to MP3 file using FFmpeg.
#

PRGNAME="${0##*/}"
CWD=$(pwd)

err()
{
    echo "$@" >&2
    exit 1
}

usage()
{
    more <<EOF >&2
usage: ${PRGNAME} option

options:
    -h  show this help
    -d  directory to scan
EOF
    exit 0
}

check_ffmpeg()
{
    if [ -x $(which ffmpeg 2>/dev/null) ]; then
        echo $(which ffmpeg)
    else
        err "ERROR: ffmpeg(1) not found"
    fi
}

transform()
{
    cmd=$(check_ffmpeg)
    
    if [ ! -d ${1} ]; then
        err "ERROR: no such file or directory"
    else
        cd ${1}
        
        for file in $(find . -type f -name '*.webm'); do
            ${cmd} -hide_banner -i $(basename ${file}) -codec:a libmp3lame \
            -q:a 2 "$(basename ${file%.*}).mp3" ;
            
            rm $(basename ${file}) ;
        done
        
        cd ${CWD}
    fi
}

while getopts ":d:" opt; do
    case ${opt} in
        d ) transform ${OPTARG} ;;
        \?) usage ;;
    esac
done
