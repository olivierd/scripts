/**
 * $CC -Wall `pkgconf --cflags --libs glib-2.0 gio-2.0 gdk-3.0 cairo \
 * libwebp` -lm -o webp-test webp-test.c
 */

#include <stdio.h>
#include <math.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>

#include <cairo.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <webp/decode.h>


#define THUMBNAIL_WIDTH 128
#define THUMBNAIL_HEIGHT 128


static GdkPixbuf *
scale_pixbuf (GdkPixbuf *source,
              gint       source_width,
              gint       source_height)
{
    gdouble w_ratio, h_ratio;
    gint    width = THUMBNAIL_WIDTH;
    gint    height= THUMBNAIL_HEIGHT;

    /* return the same pixbuf if no scaling is required */
    if (source_width <= width && source_height <= height)
        return source;

    /* determine which axis needs to be scaled down more */
    w_ratio = (gdouble) source_width / (gdouble) width;
    h_ratio = (gdouble) source_height / (gdouble) height;

    /* adjust the other axis */
    if (h_ratio > w_ratio)
        width = rint (source_width / h_ratio);
    else
        height = rint (source_height / w_ratio);

    return gdk_pixbuf_scale_simple (source,
                                    MAX (width, 1),
                                    MAX (height, 1),
                                    GDK_INTERP_BILINEAR);
}

static GdkPixbuf *
convert_webp (gchar *filename)
{
    WebPDecoderConfig config;
    /* uint8_t <=> unsigned char */
    uint8_t          *buffer = NULL;
    gsize             buffer_size;
    GError           *error = NULL;
    gint              width, height;
    gint              stride;
    cairo_surface_t  *surface;
    GdkPixbuf        *pixbuf = NULL;

    if (! WebPInitDecoderConfig (&config)) {
        g_fprintf (stderr, "Can't initialize WebPDecoder\n");
    }
    else {
        if (! g_file_get_contents (filename,
                                   (gchar **) &buffer,
                                   &buffer_size,
                                   &error)) {
            g_fprintf (stderr, "%s\n", error->message);
            g_error_free (error);
        }
        else {
            if (WebPGetFeatures (buffer, buffer_size,
                                 &config.input) != VP8_STATUS_OK) {
                g_fprintf (stderr, "Unable to retrieve features from the bitstream\n");
            }
            else {
                width = config.input.width;
                height = config.input.height;

                surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
                                                      width, height);
                cairo_surface_flush (surface);

                config.options.no_fancy_upsampling = 1;

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
                config.output.colorspace = MODE_BGRA;
#elif G_BYTE_ORDER == G_BIG_ENDIAN
                config.output.colorspace = MODE_ARGB;
#endif
                config.output.u.RGBA.rgba = (uint8_t *) cairo_image_surface_get_data (surface);
                stride = cairo_image_surface_get_stride (surface);
                config.output.u.RGBA.stride = stride;
                config.output.u.RGBA.size = stride * height;
                config.output.is_external_memory = 1;

                if (WebPDecode (buffer, buffer_size,
                                &config) == VP8_STATUS_OK) {
                    cairo_surface_mark_dirty (surface);
                    if (cairo_surface_status (surface) == CAIRO_STATUS_SUCCESS) {
                        pixbuf = gdk_pixbuf_get_from_surface (surface,
                                                              0, 0,
                                                              width,
                                                              height);
                    }
                }
                else {
                    g_fprintf (stdout, "Can't decode image\n");
                }

                WebPFreeDecBuffer (&config.output);
                cairo_surface_destroy (surface);
            }
            g_free (buffer);
        }
    }

    if (pixbuf != NULL) {
        return scale_pixbuf (pixbuf, width, height);
    }
    else {
        return pixbuf;
    }
}

static gboolean
is_webp (GFile *file)
{
    GFileInfo   *info;
    GError      *error = NULL;
    const gchar *mime;
    gboolean     res = FALSE;

    g_return_val_if_fail (G_IS_FILE (file), FALSE);

    info = g_file_query_info (file,
                              G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                              G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                              NULL, &error);
    if (info == NULL) {
        g_fprintf (stderr, "%s\n", error->message);
        g_error_free (error);
    }
    else {
        mime = g_file_info_get_content_type (info);
        if (g_strcmp0 (mime, "image/webp") == 0)
            res = TRUE;

        g_object_unref (info);
    }
    return res;
}

int
main (int argc, char *argv[])
{
    GFile     *file;
    gchar     *filename;
    GdkPixbuf *pixbuf;

    if (argc < 2) {
        g_fprintf (stderr, "Missing input FILE\n");
    }
    else {
        file = g_file_new_for_commandline_arg (argv[1]);
        if (is_webp (file)) {
            filename = g_file_get_path (file);

            pixbuf = convert_webp (filename);
            if (pixbuf != NULL) {
                gchar *new_filename;

                new_filename = g_strconcat (filename, ".png", NULL);
                
                if (gdk_pixbuf_save (pixbuf, new_filename,
                                     "png", NULL,
                                     "compression", "3", NULL))
                    g_fprintf (stdout, "Yes\n");

                g_free (new_filename);
                g_object_unref (pixbuf);
            }
            g_free (filename);
        }

        g_object_unref (file);
    }

    return 0;
}
