/**
* Example using GLib.Regex.
*
* valac --pkg glib-2.0 regex-test.vala
*/

int main (string[] argv) {
	GLib.Regex re;
	const string date = "2019-02-28T11:49:58+01:00";

	try {
		re = new GLib.Regex ("[+-][0-9]{2}:[0-9]{2}$");
		if (re.match (date)) {
			stdout.printf ("%s\n", re.replace (date, date.length, 0, "Z"));
		}
	} catch (GLib.RegexError e) {
		stderr.printf ("%s\n", e.message);
	}

	return 0;
}
