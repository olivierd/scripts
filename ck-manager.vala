/**
* valac --pkg=glib-2.0 --pkg=gio-2.0 ck-manager.vala
*
*/

struct SeatInfo {
	string name;
	GLib.ObjectPath path;
}

// Interface name
[DBus (name = "org.freedesktop.ConsoleKit.Manager")]
interface SystemInterface: GLib.Object {
	public abstract GLib.ObjectPath[] get_seats () throws GLib.Error;
	public abstract SeatInfo[] list_seats () throws GLib.Error;
}


static int
main (string[] argv) {
	GLib.MainLoop loop = new GLib.MainLoop ();
	SystemInterface system_iface = null;
	//GLib.ObjectPath[] seats;
	SeatInfo[] seats;

	try {
		system_iface = GLib.Bus.get_proxy_sync (GLib.BusType.SYSTEM,
                                                "org.freedesktop.ConsoleKit",
                                                "/org/freedesktop/ConsoleKit/Manager");

		if (system_iface != null) {
			/*system_iface.get_seats (out seats);
			foreach (GLib.ObjectPath seat in seats) {
				stdout.printf ("%s\n", seat.to_string ());
			}*/
			seats = system_iface.list_seats ();
			foreach (SeatInfo seat in seats) {
				stdout.printf ("%s %s\n", seat.path.to_string (),
                               seat.name);
			}
		}
		else {
			stderr.printf ("Error\n");
		}
	} catch (GLib.Error err) {
		stderr.printf ("Error: %s\n", err.message);
	}

	loop.run ();

	return 0;
}
