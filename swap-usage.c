/**
 * Get swap info.
 *
 * cc -Wall -I/usr/include -lc -lkvm -o swap-usage swap-usage.c
 */

#include <stdio.h>
#include <fcntl.h>
#include <kvm.h>
#include <unistd.h> /* getpagesize() */
#include <paths.h> /* _PATH_DEVNULL */
#include <sys/types.h>
#include <sys/sysctl.h> /* sysctlbyname() */


int
nb_swap_devices(void)
{
	int buf;
	size_t len = sizeof(int);

	if (sysctlbyname("vm.nswapdev", &buf, &len, NULL, 0) < 0) {
		return 0;
	}
	else {
		return buf;
	}
}

int
main(int argc, char *argv[])
{
	kvm_t *kd;
	struct kvm_swap kswap[16]; /* size taken from pstat/pstat.c */

	kd = kvm_open(NULL, _PATH_DEVNULL, NULL, O_RDONLY, "kvm_open");
	if (kd == NULL)
		kvm_close(kd);
	
	if (kvm_getswapinfo(kd, kswap, (sizeof(kswap) / sizeof(kswap[0])), SWIF_DEV_PREFIX) > 0) {
		int nb_swapd = nb_swap_devices();
		/* We display only one swap device */
		if (nb_swapd >= 1) {
			//printf("%s\n", kswap[0].ksw_devname);
			long swap_total = kswap[0].ksw_total * getpagesize();
			long swap_used = kswap[0].ksw_used * getpagesize();

			float swap_percent = (swap_used * 100) / (double) swap_total;
			printf("%.0f%%\n", swap_percent);
		}

		kvm_close(kd);
	}
	else {
		kvm_close(kd);
		perror("kvm_getswapinfo");
	}

	return 0;
}
