/**
* valac --pkg=glib-2.0 --pkg=gio-2.0 login1.vala
*
**/


static void
deconstructed_container (GLib.Variant variant) {
    GLib.VariantIter iter;
    GLib.Variant? val = null;
    string name;

    iter = variant.iterator ();
    val = iter.next_value ();
    if (val != null) {
        if (val.get_type ().is_array () || val.get_type ().is_tuple ()) {
            deconstructed_container (val);
        }
        else {
            stdout.printf ("%s\n", val.get_type ().to_string ());
        }
    }
}

static void
get_list_users (GLib.DBusConnection conn) {
    string bus_name = "org.freedesktop.login1";
    string path_name = "/org/freedesktop/login1";
    string iface_name = "org.freedesktop.login1.Manager";

    GLib.Variant result = null;
    GLib.VariantIter iter;

    try {
        result = conn.call_sync (bus_name, path_name, iface_name,
                                 "ListUsers", null,
                                 new GLib.VariantType ("(a(uso))"),
                                 GLib.DBusCallFlags.NONE, -1, null);
    } catch (GLib.Error err) {
        stdout.printf ("Error: %s\n", err.message);
    }

    if (result != null) {
        iter = result.iterator ();
        GLib.Variant? val = null;

        val = iter.next_value ();
        if (val != null) {
            deconstructed_container (val);
        }
    }
}

static int
main (string[] argv) {
    GLib.DBusConnection connection = null;

    try {
        connection = GLib.Bus.get_sync (GLib.BusType.SYSTEM, null);
    } catch (GLib.IOError err) {
        stderr.printf ("Error: %s\n", err.message);
    }

    if (connection != null) {
        //stdout.printf ("Ok\n");
        get_list_users (connection);
    }

    return 0;
}
