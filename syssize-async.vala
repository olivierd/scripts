/**
 * Display the size used by system (except for the /home directory).
 *
 * valac --pkg=glib-2.0 --pkg=gio-2.0 --pkg=gio-unix-2.0
 *
 **/

public class SystemSize : Object {
	private List<UnixMountEntry> entries;
	private List<string> list;

	/* Creation method */
	public SystemSize () {
		entries = GLib.UnixMountEntry.get ();
		list = new List<string> ();
	}

	private async void get_filesystem_space (string path,
											 out uint64 fs_size,
											 out uint64 fs_free) {
		GLib.File file;
		GLib.FileInfo info = null;

		file = GLib.File.new_for_path (path);
		try {
			info = yield file.query_filesystem_info_async ("filesystem::*",
														   0, null);
		} catch (GLib.Error e) {
			stderr.printf ("Error: %s\n", e.message);
		}

		if (info != null) {
			if (info.has_attribute (GLib.FileAttribute.FILESYSTEM_SIZE)) {
				fs_size = info.get_attribute_uint64 (GLib.FileAttribute.FILESYSTEM_SIZE);
			}
			if (info.has_attribute (GLib.FileAttribute.FILESYSTEM_FREE)) {
				fs_free = info.get_attribute_uint64 (GLib.FileAttribute.FILESYSTEM_FREE);
			}
		}
	}
	
	/* Get a list of mount points (even if system is splitted) */
	private void get_list_mount_points () {
		unowned string blk_device; /* e.g. /dev/... */
		unowned string mnt_point; /* e.g. /usr, etc. */

		foreach (unowned UnixMountEntry e in entries) {
			blk_device = e.get_device_path ();
			if ((blk_device != null) && (blk_device.has_prefix ("/dev/"))) {
				/* Get mount point */
				mnt_point = e.get_mount_path ();
				if (!mnt_point.has_prefix ("/home")) {
					list.append (mnt_point);
				}
			}
		}
	}

	private uint64 make_sum (List<uint64?> elems) {
		uint64 res = 0;

		elems.foreach ((e) => {
				res = res + e;
			});

		return res;
	}

	public async string get_filesystem_space_string () {
		uint64 fs_free = 0;
		uint64 fs_size = 0;
		List<uint64?> fs_free_list;
		List<uint64?> fs_size_list;
		string res;

		fs_free_list = new List<uint64?> ();
		fs_size_list = new List<uint64?> ();
		
		get_list_mount_points ();

		for (uint i = 0; i < list.length (); i++) {
			yield get_filesystem_space (list.nth_data (i),
										out fs_size, out fs_free);

			if ((fs_size != 0) && (fs_free != 0)) {
				fs_size_list.append (fs_size);
				fs_free_list.append (fs_free);
			}
		}

		res = "%s of %s free (%d%% used)".printf (GLib.format_size (make_sum (fs_free_list)),
												  GLib.format_size (make_sum (fs_size_list)),
												  (int)((make_sum (fs_size_list) - make_sum (fs_free_list)) * 100 / make_sum (fs_size_list)));

		return res;
	}

	public static int main(string[] args) {
		GLib.MainLoop loop;
		SystemSize size_str;

		loop = new GLib.MainLoop ();
		size_str = new SystemSize ();

		size_str.get_filesystem_space_string.begin ((obj, res) => {
				try {
					string result;

					result = size_str.get_filesystem_space_string.end(res);
					stdout.printf ("%s\n", result);
				} catch (GLib.ThreadError e) {
					stderr.printf ("Error: %s", e.message);
				}
				loop.quit ();
			});

		loop.run ();

		return 0;
	}
}
