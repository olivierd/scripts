/**
 * Simple example using Gtk.Toolbar
 *
 * valac --pkg=glib-2.0 --pkg=gtk+-3.0 --pkg=gdk-3.0 test-toolbar.vala
 */

public class TestWindow : Gtk.ApplicationWindow
{
	static Gtk.Grid grid;
	static Gtk.Box vbox;
	static Gtk.Calendar cal;
	static Gtk.SearchEntry search;
	static Gtk.Statusbar status_bar;

	public TestWindow (Gtk.Application app, DateTime dt)
	{
		GLib.Object (application: app);
		uint id;

		this.set_default_size (300, 350);
		this.window_position = Gtk.WindowPosition.NONE;
		this.title = "Toolbar example";

		grid = new Gtk.Grid ();
		this.add (grid);

		vbox = toolbar_init (dt);
		grid.attach (vbox, 0, 0, 1, 1);

		Gtk.Label label = new Gtk.Label ("%04d%02d%02d".printf (cal.year,
                                         (cal.month + 1), cal.day));
		grid.attach_next_to (label, vbox, Gtk.PositionType.BOTTOM, 3, 1);

		id = status_bar_init (label);
		status_bar.push (id, "%04d%02d%02d".printf (cal.year,
                         (cal.month + 1), cal.day));

		this.show_all ();

		/* Signals */
		cal.day_selected.connect (() => {
			label.set_label ("%04d%02d%02d".printf (cal.year,
                             (cal.month + 1), cal.day));

			status_bar.push (id, "%04d%02d%02d".printf (cal.year,
                             (cal.month + 1), cal.day));
		});

		search.activate.connect (() => {
			unowned string? res = null;

			res = check_entry ();
			if (res != null) {
				label.set_label ("%s".printf (res));
			}
		});
	}

	static unowned string? check_entry ()
	{
		Gtk.EntryBuffer buffer;
		Gtk.InfoBar bar;
		Gtk.Container container;
		unowned string? res = null;

		bar = new Gtk.InfoBar ();

		if (search.get_text () != null) {
			if (search.get_text ().has_prefix ("http")) {
				res = search.get_text ();
			}
			else {
				buffer = search.get_buffer ();
				buffer.delete_text (0, -1);

				/* Build InfoBar */
				bar.set_message_type (Gtk.MessageType.ERROR);
				bar.show_close_button = true;
				container = bar.get_content_area ();
				container.add (new Gtk.Label ("Not a valid URI scheme"));

				grid.attach_next_to (bar, vbox, Gtk.PositionType.TOP, 3, 1);
				bar.show_all ();

				/* Signal */
				bar.response.connect ((res) => {
					if (res == Gtk.ResponseType.CLOSE) {
						bar.set_visible (false);
					}
				});
			}
		}

		return res;
	}

	static Gtk.ToolButton add_toolbutton (string name, Gtk.Toolbar bar,
                                          Gtk.Align align)
	{
		Gtk.Image img;
		Gtk.ToolButton button;

		img = new Gtk.Image.from_icon_name (name, Gtk.IconSize.BUTTON);
		button = new Gtk.ToolButton (img, null);
		button.set_halign (align);

		bar.add (button);

		return button;
	}

	static void add_search_entry (Gtk.Toolbar bar)
	{
		Gtk.ToolItem item;

		item = new Gtk.ToolItem ();

		search = new Gtk.SearchEntry ();
		search.set_halign (Gtk.Align.FILL);

		item.add (search);
		bar.add (item);
	}

	static void calendar_popover (Gtk.ToolButton button,
                                  GLib.DateTime dt)
	{
		Gtk.Popover popover;

		popover = new Gtk.Popover (button);
		popover.position = Gtk.PositionType.BOTTOM;

		cal = new Gtk.Calendar ();
		cal.no_month_change = true;
		//cal.day = dt.get_day_of_month ();
		cal.month = (dt.get_month () -1);
		cal.year = dt.get_year ();

		cal.mark_day (dt.get_day_of_month ());

		popover.add (cal);

		/* Signals */
		button.clicked.connect (() => {
			popover.popup ();
			popover.show_all ();
		});
	}

	static Gtk.Box toolbar_init (GLib.DateTime dt)
	{
		Gtk.Box box;
		Gtk.Toolbar toolbar;
		Gtk.ToolButton cal_button;

		box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
		box.set_homogeneous (false);

		toolbar = new Gtk.Toolbar ();
		box.pack_start (toolbar, true, true, 0);

		/* Content of toolbar */
		cal_button = add_toolbutton ("x-office-calendar-symbolic",
                                     toolbar, Gtk.Align.START);
		add_search_entry (toolbar);

		calendar_popover (cal_button, dt);

		return box;
	}

	static uint status_bar_init (Gtk.Widget widget)
	{
		uint id;

		status_bar = new Gtk.Statusbar ();
		id = status_bar.get_context_id ("calendar-status");

		grid.attach_next_to (status_bar, widget,
                             Gtk.PositionType.BOTTOM, 2, 1);

		return id;
	}

}

public class TestApp : Gtk.Application
{
	/* Property */
	public GLib.DateTime dt { get; set; }

	const GLib.ActionEntry[] entries = {
		{ "quit", test_quit_application_cb }
	};

	void test_quit_application_cb (GLib.SimpleAction action,
                                   GLib.Variant? param)
	{
		this.quit ();
	}

	protected override void startup ()
	{
		base.startup ();

		add_action_entries (entries, this);

		GLib.Environment.set_application_name ("vala-toolbar");
		Gtk.Window.set_default_icon_name ("x-office-calendar");

		dt = new GLib.DateTime.now_utc ();
	}

	protected override void activate ()
	{
		new TestWindow (this, dt);
	}

	/* Constructor */
	public TestApp ()
	{
		GLib.Object (application_id: "org.bitbucket.olivierduchateau.test-toolbar",
                     flags: GLib.ApplicationFlags.FLAGS_NONE);
	}
}

public static int main (string[] argv)
{
	TestApp app;

	app = new TestApp ();
	return app.run (argv);
}
