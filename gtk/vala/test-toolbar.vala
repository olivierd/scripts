/**
 * Simple example using Gtk.Toolbar
 *
 * valac --pkg=glib-2.0 --pkg=gtk+-3.0 test-toolbar.vala
 */

public class TestWindow : Gtk.ApplicationWindow
{
	private GLib.DateTime date;
	private Gtk.Toolbar bar;
	private Gtk.Image img;
	private Gtk.ToolButton button;
	private Gtk.Popover popover;
	private Gtk.Box vbox;
	private Gtk.Calendar cal;

	public TestWindow (Gtk.Application app)
	{
		GLib.Object (application: app);

		this.set_default_size (300, 350);
		this.window_position = Gtk.WindowPosition.NONE;
		this.title = "Toolbar example";

		date = new GLib.DateTime.now_utc ();

		vbox = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
		this.add (vbox);

		bar = new Gtk.Toolbar ();
		vbox.pack_start (bar, false, false, 0);

		//Gtk.Label label = new Gtk.Label ("Hello!");

		/* Content of the toolbar */
		img = new Gtk.Image.from_icon_name ("x-office-calendar-symbolic",
                                            Gtk.IconSize.BUTTON);
		button = new Gtk.ToolButton (img, null);
		bar.add (button);

		popover = new Gtk.Popover (button);
		popover.position = Gtk.PositionType.BOTTOM;
		//popover.modal = false;
		//popover.add (label);

		cal = new Gtk.Calendar ();
		cal.no_month_change = true;
		cal.day = date.get_day_of_month ();
		cal.month = (date.get_month () -1);
		cal.year = date.get_year ();

		cal.mark_day (cal.day);

		popover.add (cal);

		this.show_all ();

		button.clicked.connect (() => {
			popover.popup ();
			popover.show_all ();

			stdout.printf ("%04d%02d%02d\n", cal.year, (cal.month + 1),
                           cal.day);
		});

		cal.day_selected.connect (() => {
			stdout.printf ("%04d%02d%02d\n", cal.year, (cal.month + 1),
                           cal.day);
		});
	}
}

public class TestApp : Gtk.Application
{
	const GLib.ActionEntry[] entries = {
		{ "quit", test_quit_application_cb }
	};

	void test_quit_application_cb (GLib.SimpleAction action,
                                   GLib.Variant? param)
	{
		this.quit ();
	}

	protected override void startup ()
	{
		base.startup ();

		this.add_action_entries (entries, this);

		GLib.Environment.set_application_name ("vala-toolbar");
		Gtk.Window.set_default_icon_name ("x-office-calendar");
	}

	protected override void activate ()
	{
		//TestWindow win;

		new TestWindow (this);
	}

	/* Constructor */
	public TestApp ()
	{
		GLib.Object (application_id: "org.bitbucket.olivierduchateau.test-toolbar",
                     flags: GLib.ApplicationFlags.FLAGS_NONE);
	}
}

public static int main (string[] argv)
{
	TestApp app;

	app = new TestApp ();
	return app.run (argv);
}
