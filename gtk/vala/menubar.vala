/**
 * Simple example showing menubar
 *
 * valac --pkg=glib-2.0 --pkg=gio-2.0 --pkg=gtk+-3.0 menubar.vala
 **/

public class Window : Gtk.ApplicationWindow {

	private const GLib.ActionEntry[] entries = {
		{ "about", about_window_cb }
	};

	private void
	about_window_cb (GLib.SimpleAction action, GLib.Variant? param)
	{
		string[] authors = {
			"Olivier Duchateau <duchateau.olivier@gmail.com>"
		};

		Gtk.show_about_dialog (this,
                               "program-name", "MenuBar",
                               "logo-icon-name", "application-x-executable",
                               "authors", authors,
                               null);
	}

	public Window (Gtk.Application app)
	{
		GLib.Object (application: app);

		this.set_default_size (150, 300);
		this.set_size_request (100, 300);
		this.window_position = Gtk.WindowPosition.NONE;
		this.title = "MenuBar example";
		this.show_menubar = true;

		this.add_action_entries (entries, this);

		this.show_all ();
	}
}

public class Application : Gtk.Application {

	private const GLib.ActionEntry[] entries = {
		{ "quit", quit_application_cb }
	};

	/* Constructor */
	public Application () {
		GLib.Object (application_id: "org.bitbucket.olivierduchateau.scripts.menubar",
                     flags: GLib.ApplicationFlags.FLAGS_NONE);
	}

	void
	quit_application_cb (GLib.SimpleAction action, GLib.Variant? param)
	{
		this.quit ();
	}

	protected override void
	activate ()
	{
		new Window (this);
	}

	protected override void
	startup ()
	{
		Gtk.Builder builder;

		base.startup ();

		this.add_action_entries (entries, this);

		builder = new Gtk.Builder ();
		try {
			builder.add_from_file ("menu.ui");
		} catch (GLib.Error e) {
			stderr.printf ("Error: %s\n", e.message);
		}

		this.menubar = builder.get_object ("menubar") as GLib.MenuModel;

		GLib.Environment.set_application_name ("menubar");
		Gtk.Window.set_default_icon_name ("application-x-executable");
	}
}

public static int
main (string[] argv)
{
	Application app;

	app = new Application ();
	return app.run (argv);
}
