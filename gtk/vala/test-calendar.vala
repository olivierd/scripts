/**
 * Simple example using Gtk.Calendar
 *
 * valac --pkg=glib-2.0 --pkg=gtk+-3.0 test-calendar.vala
 */


public class TestWindow: Gtk.ApplicationWindow
{
	private GLib.DateTime now;
	private Gtk.Calendar cal;

	public TestWindow (Gtk.Application app)
	{
		GLib.Object (application: app);

		this.set_default_size (300, 150);
		this.window_position = Gtk.WindowPosition.NONE;
		this.title = "Calendar example";

		now = new GLib.DateTime.now_utc ();

		cal = new Gtk.Calendar ();
		cal.no_month_change = true;
		this.add (cal);

		this.show_all ();
	}

	public void calendar_display ()
	{
		cal.day = now.get_day_of_month ();
		/* /!\ Here a number between 0 and 11 */
		cal.month = (now.get_month () -1);
		cal.year = now.get_year ();

		cal.mark_day (cal.day);

		/* Signal */
		cal.day_selected.connect (() => {
			stdout.printf ("%04d%02d%02d\n", cal.year, (cal.month + 1),
                           cal.day);
		});
	}
}


public class TestApp : Gtk.Application
{
	/* Property */
	public string country_code { get; set; }

	const GLib.ActionEntry[] entries = {
		{ "quit", test_quit_application_cb }
	};

	/* Constructor */
	public TestApp ()
	{
		GLib.Object (application_id: "org.bitbucket.olivierduchateau.test-calendar",
                     flags: GLib.ApplicationFlags.FLAGS_NONE);
	}

	void test_quit_application_cb (GLib.SimpleAction action,
                                          GLib.Variant? param)
	{
		this.quit ();
	}

	static string get_iso3166_2 ()
	{
		string locale = null;
		string code = null;
		string[] tokens;

		string[] env_prop = GLib.Environ.get ();
		locale = GLib.Environ.get_variable (env_prop, "LC_CTYPE");
		if (locale != null) {
			tokens = locale.split_set ("_");
			if (tokens.length == 2) {
				code = tokens[0];
			}
		}
		else {
			locale = GLib.Environ.get_variable (env_prop, "LANG");
			if (locale != null) {
				tokens = locale.split_set ("_");
				if (tokens.length == 2) {
					code = tokens[0];
				}
			}
		}

		/* Default value */
		if (code == null) {
			code = "fr";
		}

		return code;
	}

	protected override void startup ()
	{
		base.startup ();

		this.add_action_entries (entries, this);

		country_code = get_iso3166_2 ();

		GLib.Environment.set_application_name ("Calendar");
		Gtk.Window.set_default_icon_name ("x-office-calendar");
	}

	protected override void activate ()
	{
		TestWindow win;

		/*
		Gtk.ApplicationWindow win;
		Gtk.Label label;

		win = new Gtk.ApplicationWindow (this);
		win.title = "Calendar example";
		win.window_position = Gtk.WindowPosition.NONE;
		win.set_default_size (400, 400);

		label = new Gtk.Label ("%s".printf (country_code));
		win.add (label);
		win.show_all ();
		*/

		win = new TestWindow (this);
		win.calendar_display ();
	}
}

public static int main (string[] argv)
{
	TestApp app;

	app = new TestApp ();
	return app.run (argv);
}
