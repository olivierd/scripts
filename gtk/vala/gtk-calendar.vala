/**
 * Simple example using Gtk.Toolbar
 *
 * valac --pkg=glib-2.0 --pkg=gtk+-3.0 test-toolbar.vala
 */

public class TestWindow : Gtk.ApplicationWindow
{
	private Gtk.Toolbar bar;
	private Gtk.Image img;
	private Gtk.ToolButton button;
	private Gtk.Popover popover;
	private Gtk.Box vbox;
	private Gtk.Calendar cal;

	public TestWindow (Gtk.Application app, DateTime dt)
	{
		GLib.Object (application: app);

		this.set_default_size (300, 350);
		this.window_position = Gtk.WindowPosition.NONE;
		this.title = "Toolbar example";

		vbox = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
		this.add (vbox);

		bar = new Gtk.Toolbar ();
		vbox.pack_start (bar, false, false, 0);

		/* Content of the toolbar */
		img = new Gtk.Image.from_icon_name ("x-office-calendar-symbolic",
                                            Gtk.IconSize.BUTTON);
		button = new Gtk.ToolButton (img, null);
		bar.add (button);

		popover = new Gtk.Popover (button);
		popover.position = Gtk.PositionType.BOTTOM;

		cal = new Gtk.Calendar ();
		cal.no_month_change = true;
		//cal.day = date.get_day_of_month ();
		cal.month = (dt.get_month () -1);
		cal.year = dt.get_year ();

		cal.mark_day (dt.get_day_of_month ());

		popover.add (cal);

		this.show_all ();

		button.clicked.connect (() => {
			popover.popup ();
			popover.show_all ();
		});

		cal.day_selected.connect (() => {
			stdout.printf ("%04d%02d%02d\n", cal.year, (cal.month + 1),
                           cal.day);
		});
	}
}

public class TestApp : Gtk.Application
{
	/* Property */
	public GLib.DateTime dt { get; set; }

	const GLib.ActionEntry[] entries = {
		{ "quit", test_quit_application_cb }
	};

	void test_quit_application_cb (GLib.SimpleAction action,
                                   GLib.Variant? param)
	{
		this.quit ();
	}

	protected override void startup ()
	{
		base.startup ();

		add_action_entries (entries, this);

		GLib.Environment.set_application_name ("vala-toolbar");
		Gtk.Window.set_default_icon_name ("x-office-calendar");

		dt = new GLib.DateTime.now_utc ();
	}

	protected override void activate ()
	{
		new TestWindow (this, dt);
	}

	/* Constructor */
	public TestApp ()
	{
		GLib.Object (application_id: "org.bitbucket.olivierduchateau.test-toolbar",
                     flags: GLib.ApplicationFlags.FLAGS_NONE);
	}
}

public static int main (string[] argv)
{
	TestApp app;

	app = new TestApp ();
	return app.run (argv);
}
