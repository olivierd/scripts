#!/usr/bin/env python3

# Generate POTFILES.in from .po file
#

import argparse
import os
import re


def _save(path, seq):
    dir = os.path.dirname(path)
    pot_files = 'POTFILES.in'

    with open(os.path.join(dir, pot_files), 'w') as f:
        for i in seq:
            if seq[-1] == i:
                f.write('%s' % i)
            else:
                f.write('%s%s' % (i, os.linesep))
    

def _split_token(seq):
    list = []
    
    tokens = ''.join(seq).split(' ')
    for token in tokens:
        child = token.split(':')
        list.append(child[0])

    return list
        

def extract_files(resource):
    files = []
    # Pattern to search
    p = re.compile(r'^#: (?P<file>.+?)')

    with open(resource, 'r') as f:
        for line in f:
            l = line.replace(os.linesep, '')
            r = p.search(l)
            if r is not None:
                token = l.split(': ')
                files = list(set(files + _split_token(token[1:])))

    _save(resource, files)
                

def main(args):
    root_dir = os.path.abspath(''.join(args.path))
    po_file = 'template.po'

    path = os.path.join(root_dir, 'po', po_file)
    if os.path.exists(path):
        extract_files(path)
    else:
        print('No such file or directory')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('path', nargs=1,
                        help='Path to project directory')
    main(parser.parse_args())
