#!/usr/bin
#
# Create .po template in order to translate gettext messages.
#

## Code source
SOURCES=*.vala

## Don't change anything below
#

CWD=$(pwd)

if test -z "$XGETTEXT"; then
		if type xgettext >/dev/null 2>&1; then
				XGETTEXT=$(which xgettext)
		else
				echo "gettext package is required."
				exit 1
		fi
fi

# Go to project directory
if [ $# -eq 1 ]; then
		cd $1

		if test -n "$XGETTEXT"; then
				find . -type f \
						 \( -name "$SOURCES" -or -name "*.desktop.in" \) \
						| xargs $XGETTEXT --from-code=UTF-8 --keyword=_ \
										--keyword=N_ --keyword=C_:1c,2 \
										--keyword=NC_:1c,2 --keyword=g_dngettext:2,3 \
										--add-comments --output=po/template.po
		fi
fi

cd $CWD
